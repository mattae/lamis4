/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_sequence")
@Data
public class Sequence implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sequence_name")
    private String sequenceName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "current_value")
    private long currentValue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "increment_by")
    private int incrementBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "min_value")
    private long minValue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_value")
    private long maxValue;

    @Column(name = "cycle_flag")
    private Short cycleFlag;

    @Basic(optional = false)
    @NotNull
    @Column(name = "cache_size")
    private int cacheSize;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;
}
