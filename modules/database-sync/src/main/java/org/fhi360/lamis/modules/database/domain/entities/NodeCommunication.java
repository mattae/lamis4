/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_communication")
@Data
public class NodeCommunication implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeCommunicationPK nodeCommunicationPK;

    @Column(name = "lock_time")
    private ZonedDateTime lockTime;

    @Size(max = 255)
    @Column(name = "locking_server_id")
    private String lockingServerId;

    @Column(name = "last_lock_time")
    private ZonedDateTime lastLockTime;

    @Column(name = "last_lock_millis")
    private BigInteger lastLockMillis;

    @Column(name = "success_count")
    private BigInteger successCount;

    @Column(name = "fail_count")
    private BigInteger failCount;

    @Column(name = "skip_count")
    private BigInteger skipCount;

    @Column(name = "total_success_count")
    private BigInteger totalSuccessCount;

    @Column(name = "total_fail_count")
    private BigInteger totalFailCount;

    @Column(name = "total_success_millis")
    private BigInteger totalSuccessMillis;

    @Column(name = "total_fail_millis")
    private BigInteger totalFailMillis;

    @Column(name = "batch_to_send_count")
    private BigInteger batchToSendCount;

    @Column(name = "node_priority")
    private Integer nodePriority;
}
