/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author User10
 */
@Embeddable
@Data
public class IncomingBatchPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "batch_id")
    private long batchId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_id")
    private Node node;
}
