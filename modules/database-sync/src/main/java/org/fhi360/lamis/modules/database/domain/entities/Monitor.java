/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.MonitorType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_monitor")
@Data
public class Monitor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "monitor_id")
    private String monitorId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "node_group_id")
    private NodeGroup nodeGroup;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "external_id")
    private String externalId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "type")
    private MonitorType type;
    @Lob
    @Size(max = 16777215)
    @Column(name = "expression")
    private String expression;

    @Basic(optional = false)
    @NotNull
    @Column(name = "threshold")
    private long threshold;

    @Basic(optional = false)
    @NotNull
    @Column(name = "run_period")
    private int runPeriod;

    @Basic(optional = false)
    @NotNull
    @Column(name = "run_count")
    private int runCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "severity_level")
    private int severityLevel;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
