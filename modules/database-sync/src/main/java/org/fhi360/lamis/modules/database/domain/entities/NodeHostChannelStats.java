/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_host_channel_stats")
@Data
public class NodeHostChannelStats implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeHostChannelStatsPK nodeHostChannelStatsPK;

    @Column(name = "data_routed")
    private BigInteger dataRouted;

    @Column(name = "data_unrouted")
    private BigInteger dataUnrouted;

    @Column(name = "data_event_inserted")
    private BigInteger dataEventInserted;

    @Column(name = "data_extracted")
    private BigInteger dataExtracted;

    @Column(name = "data_bytes_extracted")
    private BigInteger dataBytesExtracted;

    @Column(name = "data_extracted_errors")
    private BigInteger dataExtractedErrors;

    @Column(name = "data_bytes_sent")
    private BigInteger dataBytesSent;

    @Column(name = "data_sent")
    private BigInteger dataSent;

    @Column(name = "data_sent_errors")
    private BigInteger dataSentErrors;

    @Column(name = "data_loaded")
    private BigInteger dataLoaded;

    @Column(name = "data_bytes_loaded")
    private BigInteger dataBytesLoaded;

    @Column(name = "data_loaded_errors")
    private BigInteger dataLoadedErrors;
}
