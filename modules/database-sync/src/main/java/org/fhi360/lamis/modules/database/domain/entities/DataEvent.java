/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_data_event")
@Data
public class DataEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DataEventPK dataEventPK;

    @Column(name = "create_time")
    private ZonedDateTime createTime;
}
