package org.fhi360.lamis.modules.database.service;

import org.jumpmind.security.SecurityServiceFactory;
import org.jumpmind.symmetric.service.IConfigurationService;
import org.jumpmind.symmetric.service.INodeService;
import org.jumpmind.symmetric.statistic.IStatisticManager;
import org.jumpmind.symmetric.transport.IConcurrentConnectionManager;
import org.jumpmind.symmetric.web.AuthenticationInterceptor;
import org.jumpmind.symmetric.web.IInterceptor;
import org.jumpmind.symmetric.web.NodeConcurrencyInterceptor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ServerSymmetricEngine extends ClientSymmetricEngine {
    public ServerSymmetricEngine(DataSource dataSource) {
        super(dataSource);
        setDeploymentType("server");
    }

    @Override
    protected String getConfigurationFile() {
        return  "server-000.properties";
    }

    @Override
    protected SecurityServiceFactory.SecurityServiceType getSecurityServiceType() {
        return SecurityServiceFactory.SecurityServiceType.SERVER;
    }

    @Bean
    public IInterceptor getAuthenticationInterceptor(INodeService nodeService) {
        return new AuthenticationInterceptor(nodeService);
    }

    @Bean
    public IInterceptor getNodeConcurrencyInterceptor(
            IConcurrentConnectionManager concurrentConnectionManager,
            IConfigurationService configurationService,
            IStatisticManager statisticManager) {
        return new NodeConcurrencyInterceptor(
                concurrentConnectionManager, configurationService, statisticManager);
    }
}
