/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class IncomingErrorPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "batch_id")
    private long batchId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "node_id")
    private Node node;

    @Basic(optional = false)
    @NotNull
    @Column(name = "failed_row_number")
    private long failedRowNumber;
}
