/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_host_job_stats")
@Data
public class NodeHostJobStats implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeHostJobStatsPK nodeHostJobStatsPK;

    @Column(name = "processed_count")
    private BigInteger processedCount;

    @Column(name = "target_node_id")
    private Node targetNode;

    @Column(name = "target_node_count")
    private Integer targetNodeCount;
}
