/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_host_stats")
@Data
public class NodeHostStats implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeHostStatsPK nodeHostStatsPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "restarted")
    private long restarted;

    @Column(name = "nodes_pulled")
    private BigInteger nodesPulled;

    @Column(name = "total_nodes_pull_time")
    private BigInteger totalNodesPullTime;

    @Column(name = "nodes_pushed")
    private BigInteger nodesPushed;

    @Column(name = "total_nodes_push_time")
    private BigInteger totalNodesPushTime;

    @Column(name = "nodes_rejected")
    private BigInteger nodesRejected;

    @Column(name = "nodes_registered")
    private BigInteger nodesRegistered;

    @Column(name = "nodes_loaded")
    private BigInteger nodesLoaded;

    @Column(name = "nodes_disabled")
    private BigInteger nodesDisabled;

    @Column(name = "purged_data_rows")
    private BigInteger purgedDataRows;

    @Column(name = "purged_data_event_rows")
    private BigInteger purgedDataEventRows;

    @Column(name = "purged_batch_outgoing_rows")
    private BigInteger purgedBatchOutgoingRows;

    @Column(name = "purged_batch_incoming_rows")
    private BigInteger purgedBatchIncomingRows;

    @Column(name = "triggers_created_count")
    private BigInteger triggersCreatedCount;

    @Column(name = "triggers_rebuilt_count")
    private BigInteger triggersRebuiltCount;

    @Column(name = "triggers_removed_count")
    private BigInteger triggersRemovedCount;
}
