/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.OutgoingBatchStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_outgoing_batch")
@Data
public class OutgoingBatch implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OutgoingBatchPK outgoingBatchPK;

    @JoinColumn(name = "channel_id")
    private Channel channel;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OutgoingBatchStatus status;

    @Column(name = "error_flag")
    private Short errorFlag;

    @Size(max = 10)
    @Column(name = "sql_state")
    private String sqlState;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sql_code")
    private int sqlCode;

    @Lob
    @Size(max = 16777215)
    @Column(name = "sql_message")
    private String sqlMessage;

    @Size(max = 255)
    @Column(name = "last_update_hostname")
    private String lastUpdateHostname;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 255)
    @Column(name = "summary")
    private String summary;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ignore_count")
    private int ignoreCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "byte_count")
    private long byteCount;

    @Column(name = "load_flag")
    private Short loadFlag;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_count")
    private int extractCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sent_count")
    private int sentCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_count")
    private int loadCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "reload_row_count")
    private int reloadRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "other_row_count")
    private int otherRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_row_count")
    private int dataRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_row_count")
    private int extractRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_row_count")
    private int loadRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_insert_row_count")
    private int dataInsertRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_update_row_count")
    private int dataUpdateRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_delete_row_count")
    private int dataDeleteRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_insert_row_count")
    private int extractInsertRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_update_row_count")
    private int extractUpdateRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_delete_row_count")
    private int extractDeleteRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_insert_row_count")
    private int loadInsertRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_update_row_count")
    private int loadUpdateRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_delete_row_count")
    private int loadDeleteRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "network_millis")
    private int networkMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "filter_millis")
    private int filterMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_millis")
    private int loadMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "router_millis")
    private int routerMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_millis")
    private int extractMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "transform_extract_millis")
    private int transformExtractMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "transform_load_millis")
    private int transformLoadMillis;

    @Column(name = "load_id")
    private BigInteger loadId;

    @Column(name = "common_flag")
    private Short commonFlag;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fallback_insert_count")
    private int fallbackInsertCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fallback_update_count")
    private int fallbackUpdateCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ignore_row_count")
    private int ignoreRowCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "missing_delete_count")
    private int missingDeleteCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "skip_count")
    private int skipCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "total_extract_millis")
    private int totalExtractMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "total_load_millis")
    private int totalLoadMillis;

    @Column(name = "extract_job_flag")
    private Short extractJobFlag;

    @Column(name = "extract_start_time")
    private ZonedDateTime extractStartTime;

    @Column(name = "transfer_start_time")
    private ZonedDateTime transferStartTime;

    @Column(name = "load_start_time")
    private ZonedDateTime loadStartTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "failed_data_id")
    private long failedDataId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "failed_line_number")
    private long failedLineNumber;

    @Size(max = 255)
    @Column(name = "create_by")
    private String createBy;
}
