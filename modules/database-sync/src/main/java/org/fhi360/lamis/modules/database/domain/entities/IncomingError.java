/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.EventType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_incoming_error")
@Data
public class IncomingError implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected IncomingErrorPK incomingErrorPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "failed_line_number")
    private long failedLineNumber;

    @Size(max = 255)
    @Column(name = "target_catalog_name")
    private String targetCatalogName;

    @Size(max = 255)
    @Column(name = "target_schema_name")
    private String targetSchemaName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "target_table_name")
    private String targetTableName;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    private EventType eventType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "binary_encoding")
    private String binaryEncoding;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "column_names")
    private String columnNames;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "pk_column_names")
    private String pkColumnNames;

    @Lob
    @Size(max = 16777215)
    @Column(name = "row_data")
    private String rowData;

    @Lob
    @Size(max = 16777215)
    @Column(name = "old_data")
    private String oldData;

    @Lob
    @Size(max = 16777215)
    @Column(name = "cur_data")
    private String curData;

    @Lob
    @Size(max = 16777215)
    @Column(name = "resolve_data")
    private String resolveData;

    @Column(name = "resolve_ignore")
    private Short resolveIgnore;

    @Size(max = 50)
    @Column(name = "conflict_id")
    private String conflictId;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
