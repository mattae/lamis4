/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.DataGapStatus;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_data_gap")
@Data
public class DataGap implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DataGapPK dataGapPK;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DataGapStatus status;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 255)
    @Column(name = "last_update_hostname")
    private String lastUpdateHostname;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
