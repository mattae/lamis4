/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.LoadFilterType;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_load_filter")
@Data
public class LoadFilter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "load_filter_id")
    private String loadFilterId;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "load_filter_type")
    private LoadFilterType loadFilterType;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "source_node_group_id")
    private Node sourceNodeGroup;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "target_node_group_id")
    private Node targetNodeGroup;

    @Size(max = 255)
    @Column(name = "target_catalog_name")
    private String targetCatalogName;

    @Size(max = 255)
    @Column(name = "target_schema_name")
    private String targetSchemaName;

    @Size(max = 255)
    @Column(name = "target_table_name")
    private String targetTableName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "filter_on_update")
    private short filterOnUpdate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "filter_on_insert")
    private short filterOnInsert;

    @Basic(optional = false)
    @NotNull
    @Column(name = "filter_on_delete")
    private short filterOnDelete;
    @Lob
    @Size(max = 16777215)
    @Column(name = "before_write_script")
    private String beforeWriteScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "after_write_script")
    private String afterWriteScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "batch_complete_script")
    private String batchCompleteScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "batch_commit_script")
    private String batchCommitScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "batch_rollback_script")
    private String batchRollbackScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "handle_error_script")
    private String handleErrorScript;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "load_filter_order")
    private int loadFilterOrder;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fail_on_error")
    private short failOnError;
}
