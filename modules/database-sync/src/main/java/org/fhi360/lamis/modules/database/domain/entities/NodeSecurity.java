/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_security")
@Data
public class NodeSecurity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_id")
    private Node node;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "node_password")
    private String nodePassword;

    @Column(name = "registration_enabled")
    private Short registrationEnabled;

    @Column(name = "registration_time")
    private ZonedDateTime registrationTime;

    @Column(name = "initial_load_enabled")
    private Short initialLoadEnabled;

    @Column(name = "initial_load_time")
    private ZonedDateTime initialLoadTime;

    @Column(name = "initial_load_id")
    private BigInteger initialLoadId;

    @Size(max = 255)
    @Column(name = "initial_load_create_by")
    private String initialLoadCreateBy;

    @Column(name = "rev_initial_load_enabled")
    private Short revInitialLoadEnabled;

    @Column(name = "rev_initial_load_time")
    private ZonedDateTime revInitialLoadTime;

    @Column(name = "rev_initial_load_id")
    private BigInteger revInitialLoadId;

    @Size(max = 255)
    @Column(name = "rev_initial_load_create_by")
    private String revInitialLoadCreateBy;

    @Size(max = 50)
    @Column(name = "created_at_node_id")
    private String createdAtNodeId;

    /*@JoinColumn(name = "node_id", referencedColumnName = "node_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Node node;*/
}
