/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_host")
@Data
public class NodeHost implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeHostPK nodeHostPK;

    @Size(max = 60)
    @Column(name = "instance_id")
    private String instanceId;

    @Size(max = 50)
    @Column(name = "ip_address")
    private String ipAddress;

    @Size(max = 50)
    @Column(name = "os_user")
    private String osUser;

    @Size(max = 50)
    @Column(name = "os_name")
    private String osName;

    @Size(max = 50)
    @Column(name = "os_arch")
    private String osArch;

    @Size(max = 50)
    @Column(name = "os_version")
    private String osVersion;

    @Column(name = "available_processors")
    private Integer availableProcessors;

    @Column(name = "free_memory_bytes")
    private BigInteger freeMemoryBytes;

    @Column(name = "total_memory_bytes")
    private BigInteger totalMemoryBytes;

    @Column(name = "max_memory_bytes")
    private BigInteger maxMemoryBytes;

    @Size(max = 50)
    @Column(name = "java_version")
    private String javaVersion;

    @Size(max = 255)
    @Column(name = "java_vendor")
    private String javaVendor;

    @Size(max = 255)
    @Column(name = "jdbc_version")
    private String jdbcVersion;

    @Size(max = 50)
    @Column(name = "symmetric_version")
    private String symmetricVersion;

    @Size(max = 6)
    @Column(name = "timezone_offset")
    private String timezoneOffset;

    @Column(name = "heartbeat_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime heartbeatTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_restart_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime lastRestartTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime createTime;
}
