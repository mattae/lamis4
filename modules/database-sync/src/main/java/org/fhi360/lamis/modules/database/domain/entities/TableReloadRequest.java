/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_table_reload_request")
@Data
public class TableReloadRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TableReloadRequestPK tableReloadRequestPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_table")
    private short createTable;

    @Basic(optional = false)
    @NotNull
    @Column(name = "delete_first")
    private short deleteFirst;

    @Lob
    @Size(max = 16777215)
    @Column(name = "reload_select")
    private String reloadSelect;

    @Lob
    @Size(max = 16777215)
    @Column(name = "before_custom_sql")
    private String beforeCustomSql;

    @Column(name = "reload_time")
    private ZonedDateTime reloadTime;

    @Column(name = "load_id")
    private BigInteger loadId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "processed")
    private short processed;

    @Size(max = 128)
    @JoinColumn(name = "channel_id")
    private Channel channel;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;
}
