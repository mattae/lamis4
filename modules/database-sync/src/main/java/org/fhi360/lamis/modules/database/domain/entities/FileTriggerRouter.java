/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.FileConflictStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_file_trigger_router")
@Data
public class FileTriggerRouter implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FileTriggerRouterPK fileTriggerRouterPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "initial_load_enabled")
    private short initialLoadEnabled;

    @Size(max = 255)
    @Column(name = "target_base_dir")
    private String targetBaseDir;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "conflict_strategy")
    private FileConflictStrategy conflictStrategy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

    @JoinColumn(name = "trigger_id", referencedColumnName = "trigger_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FileTrigger fileTrigger;

    @JoinColumn(name = "router_id", referencedColumnName = "router_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Router router;
}
