/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class NodeGroupLinkPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "source_node_group_id")
    private NodeGroup sourceNodeGroup;

    @Basic(optional = false)
    @NotNull
    @Column(name = "target_node_group_id")
    private NodeGroup targetNodeGroup;
}
