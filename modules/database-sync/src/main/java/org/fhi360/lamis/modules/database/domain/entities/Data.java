/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import org.fhi360.lamis.modules.database.domain.enumerations.EventType;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_data")
@lombok.Data
public class Data implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "data_id")
    private Long dataId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "table_name")
    private String tableName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "event_type")
    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @Lob
    @Size(max = 16777215)
    @Column(name = "row_data")
    private String rowData;

    @Lob
    @Size(max = 16777215)
    @Column(name = "pk_data")
    private String pkData;

    @Lob
    @Size(max = 16777215)
    @Column(name = "old_data")
    private String oldData;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_hist_id")
    private TriggerHist triggerHistory;

    @JoinColumn(name = "channel_id")
    private Channel channel;

    @Size(max = 255)
    @Column(name = "transaction_id")
    private String transactionId;

    @Size(max = 50)
    @JoinColumn(name = "source_node_id")
    private Node sourceNode;

    @Size(max = 50)
    @Column(name = "external_data")
    private String externalData;

    @Size(max = 255)
    @Column(name = "node_list")
    private String nodeList;

    @Column(name = "create_time")
    private ZonedDateTime createTime;
}
