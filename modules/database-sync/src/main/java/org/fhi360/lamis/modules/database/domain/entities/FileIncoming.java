/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.FileEventType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_file_incoming")
@Data
public class FileIncoming implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FileIncomingPK fileIncomingPK;

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @Column(name = "last_event_type")
    private FileEventType lastEventType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "node_id")
    private Node node;

    @Column(name = "file_modified_time")
    private Long fileModifiedTime;
}
