package org.fhi360.lamis.modules.database.web.rest;

import org.apache.commons.fileupload.FileUploadException;
import org.fhi360.lamis.modules.database.service.ServerSymmetricEngine;
import org.jumpmind.symmetric.web.FileSyncPushUriHandler;
import org.jumpmind.symmetric.web.IInterceptor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/sync")
public class FileSyncPushResource extends FileSyncPushUriHandler {
    public FileSyncPushResource(ServerSymmetricEngine engine, IInterceptor... interceptors) {
        super(engine, interceptors);
    }

    @GetMapping("/{serverId}/filesync/push")
    @PostMapping("/{serverId}/filesync/push")
    @PutMapping("/{serverId}/filesync/push")
    public void handleRequest(@PathVariable String serverId, HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException, FileUploadException {
        handle(req, res);
    }
}
