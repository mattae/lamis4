/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_extension")
@Data
public class Extension implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "extension_id")
    private String extensionId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "extension_type")
    private String extensionType;

    @Size(max = 255)
    @Column(name = "interface_name")
    private String interfaceName;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_group_id")
    private NodeGroup nodeGroupId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extension_order")
    private int extensionOrder;

    @Lob
    @Size(max = 16777215)
    @Column(name = "extension_text")
    private String extensionText;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

}
