/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_channel_ctl")
@Data
public class NodeChannelCtl implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeChannelCtlPK nodeChannelCtlPK;

    @Column(name = "suspend_enabled")
    private Short suspendEnabled;

    @Column(name = "ignore_enabled")
    private Short ignoreEnabled;

    @Column(name = "last_extract_time")
    private ZonedDateTime lastExtractTime;
}
