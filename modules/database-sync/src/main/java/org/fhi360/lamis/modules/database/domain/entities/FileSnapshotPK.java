/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class FileSnapshotPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "router_id")
    private Router router;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "relative_dir")
    private String relativeDir;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "file_name")
    private String fileName;
}
