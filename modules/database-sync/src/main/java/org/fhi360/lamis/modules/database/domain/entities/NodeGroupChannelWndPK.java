/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author User10
 */
@Embeddable
@Data
public class NodeGroupChannelWndPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_group_id")
    private NodeGroup nodeGroup;

    @Basic(optional = false)
    @NotNull
    @Column(name = "channel_id")
    private Channel channel;

    @Basic(optional = false)
    @NotNull
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime startTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime endTime;
}
