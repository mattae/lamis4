/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.FileEventType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_file_snapshot")
@Data
public class FileSnapshot implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FileSnapshotPK fileSnapshotPK;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "channel_id")
    private Channel channel;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "reload_channel_id")
    private Channel reloadChannel;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "last_event_type")
    private FileEventType lastEventType;

    @Column(name = "crc32_checksum")
    private BigInteger crc32Checksum;

    @Column(name = "file_size")
    private BigInteger fileSize;

    @Column(name = "file_modified_time")
    private BigInteger fileModifiedTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;
}
