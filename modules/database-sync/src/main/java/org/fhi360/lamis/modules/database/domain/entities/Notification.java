/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_notification")
@Data
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "notification_id")
    private String notificationId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "node_group_id")
    private NodeGroup nodeGroup;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "external_id")
    private String externalId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "severity_level")
    private int severityLevel;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "type")
    private String type;

    @Lob
    @Size(max = 16777215)
    @Column(name = "expression")
    private String expression;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
