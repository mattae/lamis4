/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.RegistrationRequestStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_registration_request")
@Data
public class RegistrationRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegistrationRequestPK registrationRequestPK;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RegistrationRequestStatus status;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "host_name")
    private String hostName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "attempt_count")
    private Integer attemptCount;

    @Size(max = 50)
    @Column(name = "registered_node_id")
    private String registeredNodeId;

    @Lob
    @Size(max = 16777215)
    @Column(name = "error_message")
    private String errorMessage;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
