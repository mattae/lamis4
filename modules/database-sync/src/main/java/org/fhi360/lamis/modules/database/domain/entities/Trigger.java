/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_trigger")
@Data
public class Trigger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "trigger_id")
    private String triggerId;

    @Size(max = 255)
    @Column(name = "source_catalog_name")
    private String sourceCatalogName;

    @Size(max = 255)
    @Column(name = "source_schema_name")
    private String sourceSchemaName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "source_table_name")
    private String sourceTableName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_update")
    private short syncOnUpdate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_insert")
    private short syncOnInsert;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_delete")
    private short syncOnDelete;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_incoming_batch")
    private short syncOnIncomingBatch;

    @Size(max = 255)
    @Column(name = "name_for_update_trigger")
    private String nameForUpdateTrigger;

    @Size(max = 255)
    @Column(name = "name_for_insert_trigger")
    private String nameForInsertTrigger;

    @Size(max = 255)
    @Column(name = "name_for_delete_trigger")
    private String nameForDeleteTrigger;

    @Lob
    @Size(max = 16777215)
    @Column(name = "sync_on_update_condition")
    private String syncOnUpdateCondition;

    @Lob
    @Size(max = 16777215)
    @Column(name = "sync_on_insert_condition")
    private String syncOnInsertCondition;

    @Lob
    @Size(max = 16777215)
    @Column(name = "sync_on_delete_condition")
    private String syncOnDeleteCondition;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_before_update_text")
    private String customBeforeUpdateText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_before_insert_text")
    private String customBeforeInsertText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_before_delete_text")
    private String customBeforeDeleteText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_on_update_text")
    private String customOnUpdateText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_on_insert_text")
    private String customOnInsertText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "custom_on_delete_text")
    private String customOnDeleteText;

    @Lob
    @Size(max = 16777215)
    @Column(name = "external_select")
    private String externalSelect;

    @Lob
    @Size(max = 16777215)
    @Column(name = "tx_id_expression")
    private String txIdExpression;

    @Lob
    @Size(max = 16777215)
    @Column(name = "channel_expression")
    private String channelExpression;

    @Lob
    @Size(max = 16777215)
    @Column(name = "excluded_column_names")
    private String excludedColumnNames;

    @Lob
    @Size(max = 16777215)
    @Column(name = "included_column_names")
    private String includedColumnNames;

    @Lob
    @Size(max = 16777215)
    @Column(name = "sync_key_names")
    private String syncKeyNames;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_stream_lobs")
    private short useStreamLobs;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_capture_lobs")
    private short useCaptureLobs;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_capture_old_data")
    private short useCaptureOldData;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_handle_key_updates")
    private short useHandleKeyUpdates;

    @Basic(optional = false)
    @NotNull
    @Column(name = "stream_row")
    private short streamRow;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trigger")
    private List<TriggerRouter> triggerRouters = new ArrayList<>();

    @JoinColumn(name = "reload_channel_id", referencedColumnName = "channel_id")
    @ManyToOne(optional = false)
    private Channel reloadChannel;

    @JoinColumn(name = "channel_id", referencedColumnName = "channel_id")
    @ManyToOne(optional = false)
    private Channel channel;
}
