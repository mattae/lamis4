package org.fhi360.lamis.modules.database.web.rest;

import org.jumpmind.symmetric.service.IParameterService;
import org.jumpmind.symmetric.service.IRegistrationService;
import org.jumpmind.symmetric.web.IInterceptor;
import org.jumpmind.symmetric.web.RegistrationUriHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/sync")
public class RegistrationResource extends RegistrationUriHandler {
    public RegistrationResource(IParameterService parameterService, IRegistrationService registrationService, IInterceptor... interceptors) {
        super(parameterService, registrationService, interceptors);
    }

    @GetMapping("/{serverId}/registration")
    @PostMapping("/{serverId}/registration")
    public void handleRequest(@PathVariable String serverId, HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        handle(req, res);
    }
}
