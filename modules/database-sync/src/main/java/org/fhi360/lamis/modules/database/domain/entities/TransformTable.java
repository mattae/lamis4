/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.ColumnPolicy;
import org.fhi360.lamis.modules.database.domain.enumerations.DeleteAction;
import org.fhi360.lamis.modules.database.domain.enumerations.TransformationPoint;
import org.fhi360.lamis.modules.database.domain.enumerations.UpdateAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_transform_table")
@Data
public class TransformTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TransformTablePK transformTablePK;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Enumerated(EnumType.STRING)
    @Column(name = "transform_point")
    private TransformationPoint transformPoint;

    @Size(max = 255)
    @Column(name = "source_catalog_name")
    private String sourceCatalogName;

    @Size(max = 255)
    @Column(name = "source_schema_name")
    private String sourceSchemaName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "source_table_name")
    private String sourceTableName;

    @Size(max = 255)
    @Column(name = "target_catalog_name")
    private String targetCatalogName;

    @Size(max = 255)
    @Column(name = "target_schema_name")
    private String targetSchemaName;

    @Size(max = 255)
    @Column(name = "target_table_name")
    private String targetTableName;

    @Column(name = "update_first")
    private Short updateFirst;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "update_action")
    @Enumerated(EnumType.STRING)
    private UpdateAction updateAction;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "delete_action")
    @Enumerated(EnumType.STRING)
    private DeleteAction deleteAction;

    @Basic(optional = false)
    @NotNull
    @Column(name = "transform_order")
    private int transformOrder;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "column_policy")
    @Enumerated(EnumType.STRING)
    private ColumnPolicy columnPolicy;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;
}
