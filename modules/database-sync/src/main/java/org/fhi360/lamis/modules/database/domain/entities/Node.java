/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node")
@Data
public class Node implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "node_id")
    private String nodeId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_group_id")
    private NodeGroup nodeGroup;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "external_id")
    private String externalId;

    @Column(name = "heartbeat_time")
    private ZonedDateTime heartbeatTime;

    @Size(max = 6)
    @Column(name = "timezone_offset")
    private String timezoneOffset;

    @Column(name = "sync_enabled")
    private Short syncEnabled;

    @Size(max = 255)
    @Column(name = "sync_url")
    private String syncUrl;

    @Size(max = 50)
    @Column(name = "schema_version")
    private String schemaVersion;

    @Size(max = 50)
    @Column(name = "symmetric_version")
    private String symmetricVersion;

    @Size(max = 50)
    @Column(name = "config_version")
    private String configVersion;

    @Size(max = 50)
    @Column(name = "database_type")
    private String databaseType;

    @Size(max = 50)
    @Column(name = "database_version")
    private String databaseVersion;

    @Column(name = "batch_to_send_count")
    private Integer batchToSendCount;

    @Column(name = "batch_in_error_count")
    private Integer batchInErrorCount;

    @Size(max = 50)
    @Column(name = "created_at_node_id")
    private String createdAtNodeId;

    @Size(max = 50)
    @Column(name = "deployment_type")
    private String deploymentType;

    @Size(max = 50)
    @Column(name = "deployment_sub_type")
    private String deploymentSubType;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "node")
    private NodeIdentity nodeIdentity;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "node")
    private NodeSecurity nodeSecurity;
}
