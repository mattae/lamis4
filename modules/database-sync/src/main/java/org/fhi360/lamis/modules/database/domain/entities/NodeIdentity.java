/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_identity")
@Data
public class NodeIdentity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "node_id")
    private Node nodeId;
    @JoinColumn(name = "node_id", referencedColumnName = "node_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Node node;
}
