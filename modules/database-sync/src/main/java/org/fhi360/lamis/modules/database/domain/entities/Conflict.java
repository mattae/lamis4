/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.DetectConflict;
import org.fhi360.lamis.modules.database.domain.enumerations.PingBack;
import org.fhi360.lamis.modules.database.domain.enumerations.ResolveConflict;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_conflict")
@Data
public class Conflict implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "conflict_id")
    private String conflictId;

    @JoinColumn(name = "target_channel_id")
    private Channel targetChannel;

    @Size(max = 255)
    @Column(name = "target_catalog_name")
    private String targetCatalogName;

    @Size(max = 255)
    @Column(name = "target_schema_name")
    private String targetSchemaName;

    @Size(max = 255)
    @Column(name = "target_table_name")
    private String targetTableName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "detect_type")
    @Enumerated(EnumType.STRING)
    private DetectConflict detectType = DetectConflict.USE_PK_DATA;

    @Lob
    @Size(max = 16777215)
    @Column(name = "detect_expression")
    private String detectExpression;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "resolve_type")
    private ResolveConflict resolveType;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ping_back")
    private PingBack pingBack;

    @Column(name = "resolve_changes_only")
    private Short resolveChangesOnly;

    @Column(name = "resolve_row_only")
    private Short resolveRowOnly;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @JoinColumns({
        @JoinColumn(name = "source_node_group_id", referencedColumnName = "source_node_group_id")
        , @JoinColumn(name = "target_node_group_id", referencedColumnName = "target_node_group_id")})
    @ManyToOne(optional = false)
    private NodeGroupLink nodeGroupLink;
}
