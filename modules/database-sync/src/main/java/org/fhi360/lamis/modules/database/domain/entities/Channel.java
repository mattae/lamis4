/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.DataEventAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_channel")
@Data
public class Channel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "channel_id")
    private String channelId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "processing_order")
    private Integer processingOrder;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_batch_size")
    private Integer maxBatchSize;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_batch_to_send")
    private Integer maxBatchToSend;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_data_to_route")
    private Integer maxDataToRoute;

    @Basic(optional = false)
    @NotNull
    @Column(name = "extract_period_millis")
    private Integer extractPeriodMillis;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_old_data_to_route")
    private short useOldDataToRoute;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_row_data_to_route")
    private short useRowDataToRoute;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_pk_data_to_route")
    private short usePkDataToRoute;

    @Basic(optional = false)
    @NotNull
    @Column(name = "reload_flag")
    private short reloadFlag;

    @Basic(optional = false)
    @NotNull
    @Column(name = "file_sync_flag")
    private short fileSyncFlag;

    @Basic(optional = false)
    @NotNull
    @Column(name = "contains_big_lob")
    private short containsBigLob;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "batch_algorithm")
    private String batchAlgorithm;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "data_loader_type")
    private String dataLoaderType;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "queue")
    private String queue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_network_kbps")
    private Double maxNetworkKbps;

    @Column(name = "data_event_action")
    private DataEventAction dataEventAction;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reloadChannelId")
    private List<Trigger> triggerList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "channelId")
    private List<Trigger> triggerList1 = new ArrayList<>();
}
