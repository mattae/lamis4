/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.ExtractionStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_extract_request")
@Data
public class ExtractRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "request_id")
    private Long requestId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "node_id")
    private Node node;

    @Size(max = 128)
    @Column(name = "queue")
    private String queue;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ExtractionStatus status;

    @Basic(optional = false)
    @NotNull
    @Column(name = "start_batch_id")
    private long startBatchId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "end_batch_id")
    private long endBatchId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "router_id")
    private Router router;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @Column(name = "create_time")
    private ZonedDateTime createTime;
}
