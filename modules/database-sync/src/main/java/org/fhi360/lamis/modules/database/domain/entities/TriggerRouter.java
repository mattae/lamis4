/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User10
 */
@Entity
@Table(name = "sym_trigger_router")
@Data
public class TriggerRouter implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TriggerRouterPK triggerRouterPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "initial_load_order")
    private int initialLoadOrder;

    @Lob
    @Size(max = 16777215)
    @Column(name = "initial_load_select")
    private String initialLoadSelect;

    @Lob
    @Size(max = 16777215)
    @Column(name = "initial_load_delete_stmt")
    private String initialLoadDeleteStmt;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ping_back_enabled")
    private short pingBackEnabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

    @JoinColumn(name = "router_id", referencedColumnName = "router_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Router router;

    @JoinColumn(name = "trigger_id", referencedColumnName = "trigger_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Trigger trigger;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "triggerRouter")
    @JsonIgnore
    private List<TriggerRouterGrouplet> triggerRouterGrouplets = new ArrayList<>();
}
