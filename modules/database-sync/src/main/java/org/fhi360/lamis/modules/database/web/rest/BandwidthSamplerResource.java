package org.fhi360.lamis.modules.database.web.rest;

import org.jumpmind.symmetric.service.IParameterService;
import org.jumpmind.symmetric.web.BandwidthSamplerUriHandler;
import org.jumpmind.symmetric.web.IInterceptor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/sync")
public class BandwidthSamplerResource extends BandwidthSamplerUriHandler {
    public BandwidthSamplerResource(IParameterService parameterService, IInterceptor[] interceptors) {
        super(parameterService, interceptors);
    }

    @GetMapping("/{serverId}/bandwidth")
    @PostMapping("/{serverId}/bandwidth")
    public void handleRequest(@PathVariable String serverId, HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        handle(req, res);
    }
}
