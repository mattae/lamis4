package org.fhi360.lamis.modules.database.web.rest;

import org.fhi360.lamis.modules.database.service.ServerSymmetricEngine;
import org.jumpmind.symmetric.web.FileSyncPullUriHandler;
import org.jumpmind.symmetric.web.IInterceptor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/sync")
public class FileSyncPullResource extends FileSyncPullUriHandler {
    public FileSyncPullResource(ServerSymmetricEngine engine, IInterceptor... interceptors) {
        super(engine, interceptors);
    }

    @GetMapping("/{serverId}/filesync/pull")
    @PostMapping("/{serverId}/filesync/pull")
    public void handleRequest(@PathVariable String serverId, HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        handle(req, res);
    }
}
