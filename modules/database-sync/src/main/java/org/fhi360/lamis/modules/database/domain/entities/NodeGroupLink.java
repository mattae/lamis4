/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.DataEventAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_group_link")
@Data
public class NodeGroupLink implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeGroupLinkPK nodeGroupLinkPK;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "data_event_action")
    private DataEventAction dataEventAction;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_config_enabled")
    private short syncConfigEnabled;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_reversible")
    private short isReversible;

    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nodeGroupLink")
    private List<Router> routerList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nodeGroupLink")
    private List<TransformTable> transformTables = new ArrayList<>();

    @JoinColumn(name = "target_node_group_id", referencedColumnName = "node_group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NodeGroup targetNodeGroup;

    @JoinColumn(name = "source_node_group_id", referencedColumnName = "node_group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NodeGroup sourceNodeGroup;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nodeGroupLink")
    private List<Conflict> conflicts = new ArrayList<>();
}
