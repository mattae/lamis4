package org.fhi360.lamis.modules.database.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileUploadException;
import org.jumpmind.symmetric.model.Node;
import org.jumpmind.symmetric.model.NodeSecurity;
import org.jumpmind.symmetric.service.IDataLoaderService;
import org.jumpmind.symmetric.service.INodeCommunicationService;
import org.jumpmind.symmetric.service.INodeService;
import org.jumpmind.symmetric.service.IParameterService;
import org.jumpmind.symmetric.statistic.IStatisticManager;
import org.jumpmind.symmetric.web.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

@RestController
@RequestMapping("/sync")
public class PushStatusResource extends PushStatusUriHandler {
    public PushStatusResource(IParameterService parameterService, INodeCommunicationService nodeCommunicationService, IInterceptor... interceptors) {
        super(parameterService, nodeCommunicationService, interceptors);
    }

    @GetMapping("/{serverId}/pushstatus")
    @PutMapping("/{serverId}/pushstatus")
    @PostMapping("/{serverId}/pushstatus")
    public void handleRequest(@PathVariable String serverId, HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException, FileUploadException {
        handle(req, res);
    }

}
