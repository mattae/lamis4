/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_trigger_hist")
@Data
public class TriggerHist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "trigger_hist_id")
    private Integer triggerHistId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "source_table_name")
    private String sourceTableName;

    @Size(max = 255)
    @Column(name = "source_catalog_name")
    private String sourceCatalogName;

    @Size(max = 255)
    @Column(name = "source_schema_name")
    private String sourceSchemaName;

    @Size(max = 255)
    @Column(name = "name_for_update_trigger")
    private String nameForUpdateTrigger;

    @Size(max = 255)
    @Column(name = "name_for_insert_trigger")
    private String nameForInsertTrigger;

    @Size(max = 255)
    @Column(name = "name_for_delete_trigger")
    private String nameForDeleteTrigger;

    @Basic(optional = false)
    @NotNull
    @Column(name = "table_hash")
    private long tableHash;

    @Basic(optional = false)
    @NotNull
    @Column(name = "trigger_row_hash")
    private long triggerRowHash;

    @Basic(optional = false)
    @NotNull
    @Column(name = "trigger_template_hash")
    private long triggerTemplateHash;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "column_names")
    private String columnNames;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "pk_column_names")
    private String pkColumnNames;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_trigger_build_reason")
    private Character lastTriggerBuildReason;

    @Lob
    @Size(max = 16777215)
    @Column(name = "error_message")
    private String errorMessage;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "inactive_time")
    private LocalDateTime inactiveTime;
}
