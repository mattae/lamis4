/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.MonitorType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_monitor_event")
@Data
public class MonitorEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MonitorEventPK monitorEventPK;

    @Size(max = 60)
    @Column(name = "host_name")
    private String hostName;

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private MonitorType type;

    @Basic(optional = false)
    @NotNull
    @Column(name = "threshold")
    private long threshold;

    @Basic(optional = false)
    @NotNull
    @Column(name = "event_value")
    private long eventValue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "event_count")
    private int eventCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "severity_level")
    private int severityLevel;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_resolved")
    private short isResolved;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_notified")
    private short isNotified;

    @Lob
    @Size(max = 16777215)
    @Column(name = "details")
    private String details;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;
}
