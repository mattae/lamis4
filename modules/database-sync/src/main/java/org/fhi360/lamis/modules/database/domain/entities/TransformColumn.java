/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.TransformType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_transform_column")
@Data
public class TransformColumn implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TransformColumnPK transformColumnPK;

    @Size(max = 128)
    @Column(name = "source_column_name")
    private String sourceColumnName;

    @Column(name = "pk")
    private Short pk;

    @Size(max = 50)
    @Column(name = "transform_type")
    @Enumerated(EnumType.STRING)
    private TransformType transformType;

    @Lob
    @Size(max = 16777215)
    @Column(name = "transform_expression")
    private String transformExpression;

    @Basic(optional = false)
    @NotNull
    @Column(name = "transform_order")
    private int transformOrder;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

}
