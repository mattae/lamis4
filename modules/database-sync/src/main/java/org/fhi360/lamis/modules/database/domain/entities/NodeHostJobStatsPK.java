/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class NodeHostJobStatsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "node_id")
    private Node node;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "host_name")
    private String hostName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "job_name")
    private String jobName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "start_time")
    private ZonedDateTime startTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "end_time")
    private ZonedDateTime endTime;
}
