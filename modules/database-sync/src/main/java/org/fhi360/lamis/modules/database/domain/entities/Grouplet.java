/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.GroupletLinkPolicy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_grouplet")
@Data
public class Grouplet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "grouplet_id")
    private String groupletId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "grouplet_link_policy")
    private GroupletLinkPolicy groupletLinkPolicy;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grouplet")
    private List<GroupletLink> groupletLinks = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grouplet")
    private List<TriggerRouterGrouplet> triggerRouterGrouplets = new ArrayList<>();
}
