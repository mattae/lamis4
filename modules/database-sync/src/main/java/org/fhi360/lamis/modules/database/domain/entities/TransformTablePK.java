/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class TransformTablePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "transform_id")
    private String transformId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "source_node_group_id")
    private NodeGroup sourceNodeGroup;

    @Basic(optional = false)
    @NotNull
    @Column(name = "target_node_group_id")
    private NodeGroup targetNodeGroup;
}
