/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_node_group_channel_wnd")
@Data
public class NodeGroupChannelWnd implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NodeGroupChannelWndPK nodeGroupChannelWndPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;
}
