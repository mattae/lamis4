/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_grouplet_link")
@Data
public class GroupletLink implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GroupletLinkPK groupletLinkPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime lastUpdateTime;

    @JoinColumn(name = "grouplet_id", referencedColumnName = "grouplet_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grouplet grouplet;
}
