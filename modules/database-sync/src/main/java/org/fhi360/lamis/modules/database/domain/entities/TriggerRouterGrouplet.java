/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_trigger_router_grouplet")
@Data
public class TriggerRouterGrouplet implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TriggerRouterGroupletPK triggerRouterGroupletPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @JoinColumn(name = "grouplet_id", referencedColumnName = "grouplet_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grouplet grouplet;

    @JoinColumns({
            @JoinColumn(name = "trigger_id", referencedColumnName = "trigger_id", insertable = false, updatable = false)
            , @JoinColumn(name = "router_id", referencedColumnName = "router_id", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private TriggerRouter triggerRouter;
}
