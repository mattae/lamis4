/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.database.domain.enumerations.RouterType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_router")
@Data
public class Router implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "router_id")
    private String routerId;

    @Size(max = 255)
    @Column(name = "target_catalog_name")
    private String targetCatalogName;

    @Size(max = 255)
    @Column(name = "target_schema_name")
    private String targetSchemaName;

    @Size(max = 255)
    @Column(name = "target_table_name")
    private String targetTableName;

    @Enumerated(EnumType.STRING)
    @Column(name = "router_type")
    private RouterType routerType;

    @Lob
    @Size(max = 16777215)
    @Column(name = "router_expression")
    private String routerExpression;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_update")
    private short syncOnUpdate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_insert")
    private short syncOnInsert;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_delete")
    private short syncOnDelete;

    @Basic(optional = false)
    @NotNull
    @Column(name = "use_source_catalog_schema")
    private short useSourceCatalogSchema;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "source_node_group_id")
    private NodeGroup sourceNodeGroup;

    @Basic(optional = false)
    @NotNull
    @Column(name = "target_node_group_id")
    private NodeGroup targetNodeGroup;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "router")
    private List<FileTriggerRouter> fileTriggerRouters = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "router")
    private List<TriggerRouter> triggerRouters = new ArrayList<>();
}
