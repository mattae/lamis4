/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class TableReloadRequestPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "target_node_id")
    private Node targetNode;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "source_node_id")
    private Node sourceNode;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "router_id")
    private Router router;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private LocalDateTime createTime;
}
