/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author User10
 */
@Embeddable
@Data
public class TriggerRouterGroupletPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "grouplet_id")
    private String groupletId;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    @ManyToOne
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @ManyToOne
    @JoinColumn(name = "router_id")
    private Router router;

    @Basic(optional = false)
    @NotNull
    @Column(name = "applies_when")
    private Character appliesWhen;
}
