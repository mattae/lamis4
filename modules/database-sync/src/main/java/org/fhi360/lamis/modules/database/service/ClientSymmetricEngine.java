package org.fhi360.lamis.modules.database.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jumpmind.db.platform.DatabaseNamesConstants;
import org.jumpmind.db.platform.IDatabasePlatform;
import org.jumpmind.db.platform.JdbcDatabasePlatformFactory;
import org.jumpmind.db.platform.generic.GenericJdbcDatabasePlatform;
import org.jumpmind.db.sql.LogSqlBuilder;
import org.jumpmind.db.sql.SqlTemplateSettings;
import org.jumpmind.properties.TypedProperties;
import org.jumpmind.security.ISecurityService;
import org.jumpmind.security.SecurityServiceFactory;
import org.jumpmind.symmetric.AbstractSymmetricEngine;
import org.jumpmind.symmetric.ISymmetricEngine;
import org.jumpmind.symmetric.ITypedPropertiesFactory;
import org.jumpmind.symmetric.common.Constants;
import org.jumpmind.symmetric.common.ParameterConstants;
import org.jumpmind.symmetric.db.ISymmetricDialect;
import org.jumpmind.symmetric.db.JdbcSymmetricDialectFactory;
import org.jumpmind.symmetric.io.stage.BatchStagingManager;
import org.jumpmind.symmetric.io.stage.IStagingManager;
import org.jumpmind.symmetric.job.IJobManager;
import org.jumpmind.symmetric.job.JobManager;
import org.jumpmind.symmetric.model.Node;
import org.jumpmind.symmetric.model.NodeSecurity;
import org.jumpmind.symmetric.service.*;
import org.jumpmind.symmetric.service.impl.ClientExtensionService;
import org.jumpmind.symmetric.service.impl.MonitorService;
import org.jumpmind.symmetric.statistic.IStatisticManager;
import org.jumpmind.symmetric.statistic.StatisticManager;
import org.jumpmind.symmetric.transport.IConcurrentConnectionManager;
import org.jumpmind.symmetric.transport.ITransportManager;
import org.jumpmind.symmetric.util.LogSummaryAppenderUtils;
import org.jumpmind.symmetric.util.SnapshotUtil;
import org.jumpmind.symmetric.util.SymmetricUtils;
import org.jumpmind.symmetric.util.TypedPropertiesFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static org.apache.commons.lang.StringUtils.isBlank;

@Component
@Slf4j
public class ClientSymmetricEngine extends AbstractSymmetricEngine {

    private static final String DEPLOYMENT_TYPE_CLIENT = "client";

    private static final String PROPERTIES_FACTORY_CLASS_NAME = "properties.factory.class.name";

    protected String PROPERTIES_FILE = "client.properties";

    protected Properties properties = System.getProperties();

    private DataSource dataSource;

    private IMonitorService monitorService;

    private IDatabasePlatform databasePlatform;

    private IDatabasePlatform targetPlatform;

    private ISymmetricDialect dialect;

    public ClientSymmetricEngine(DataSource dataSource) {
        super(false);
        setDeploymentType(DEPLOYMENT_TYPE_CLIENT);
        setDeploymentSubTypeByProperties(properties);
        this.dataSource = dataSource;
        this.init();
    }

    private void setDeploymentSubTypeByProperties(Properties properties) {
        if (properties != null) {
            String loadOnly = properties.getProperty(ParameterConstants.NODE_LOAD_ONLY);
            setDeploymentSubType(loadOnly != null && loadOnly.equals("true") ? Constants.DEPLOYMENT_SUB_TYPE_LOAD_ONLY : null);
        }
    }

    @Override
    protected void init() {
        try {
            LogSummaryAppenderUtils.registerLogSummaryAppender();

            if (getSecurityServiceType().equals(SecurityServiceFactory.SecurityServiceType.CLIENT)) {
                SymmetricUtils.logNotices();
            }
            super.init();

            this.monitorService = new MonitorService(parameterService, symmetricDialect, nodeService, extensionService,
                    clusterService, contextService);
            checkLoadOnly();
        } catch (RuntimeException ex) {
            destroy();
            throw ex;
        }
    }

    public IDatabasePlatform createDatabasePlatform(TypedProperties properties, DataSource dataSource) {
        boolean delimitedIdentifierMode = properties.is(
                ParameterConstants.DB_DELIMITED_IDENTIFIER_MODE, true);
        boolean caseSensitive = !properties.is(ParameterConstants.DB_METADATA_IGNORE_CASE, true);
        databasePlatform = JdbcDatabasePlatformFactory.createNewPlatformInstance(dataSource,
                createSqlTemplateSettings(properties), delimitedIdentifierMode, caseSensitive, false);
        return databasePlatform;
    }

    private static SqlTemplateSettings createSqlTemplateSettings(TypedProperties properties) {
        SqlTemplateSettings settings = new SqlTemplateSettings();
        settings.setFetchSize(properties.getInt(ParameterConstants.DB_FETCH_SIZE, 1000));
        settings.setQueryTimeout(properties.getInt(ParameterConstants.DB_QUERY_TIMEOUT_SECS, 300));
        settings.setBatchSize(properties.getInt(ParameterConstants.JDBC_EXECUTE_BATCH_SIZE, 100));
        settings.setBatchBulkLoaderSize(properties.getInt(ParameterConstants.JDBC_EXECUTE_BULK_BATCH_SIZE, 25));
        settings.setOverrideIsolationLevel(properties.getInt(ParameterConstants.JDBC_ISOLATION_LEVEL, -1));
        settings.setReadStringsAsBytes(properties.is(ParameterConstants.JDBC_READ_STRINGS_AS_BYTES, false));
        settings.setTreatBinaryAsLob(properties.is(ParameterConstants.TREAT_BINARY_AS_LOB_ENABLED, true));
        settings.setRightTrimCharValues(properties.is(ParameterConstants.RIGHT_TRIM_CHAR_VALUES, false));
        settings.setAllowUpdatesWithResults(properties.is(ParameterConstants.ALLOW_UPDATES_WITH_RESULTS, false));

        LogSqlBuilder logSqlBuilder = new LogSqlBuilder();
        logSqlBuilder.setLogSlowSqlThresholdMillis(properties.getInt(ParameterConstants.LOG_SLOW_SQL_THRESHOLD_MILLIS, 20000));
        logSqlBuilder.setLogSqlParametersInline(properties.is(ParameterConstants.LOG_SQL_PARAMETERS_INLINE, true));
        settings.setLogSqlBuilder(logSqlBuilder);

        if (settings.getOverrideIsolationLevel() >= 0) {
            log.info("Overriding isolation level to " + settings.getOverrideIsolationLevel());
        }
        return settings;
    }

    @Override
    public IExtensionService createExtensionService() {
        return new ClientExtensionService(this, null);
    }

    @Override
    public IJobManager createJobManager() {
        return new JobManager(this);
    }

    @Override
    public IStagingManager createStagingManager() {
        String directory = parameterService.getString(ParameterConstants.STAGING_DIR);
        if (isBlank(directory)) {
            directory = parameterService.getTempDirectory();
        }
        String stagingManagerClassName = parameterService.getString(ParameterConstants.STAGING_MANAGER_CLASS);
        if (stagingManagerClassName != null) {
            try {
                Constructor<?> cons = Class.forName(stagingManagerClassName).getConstructor(ISymmetricEngine.class, String.class);
                return (IStagingManager) cons.newInstance(this, directory);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return new BatchStagingManager(this, directory);
    }

    @Override
    public IStatisticManager createStatisticManager() {
        String statisticManagerClassName = parameterService.getString(ParameterConstants.STATISTIC_MANAGER_CLASS);
        if (statisticManagerClassName != null) {
            try {
                Constructor<?> cons = Class.forName(statisticManagerClassName).getConstructor(IParameterService.class,
                        INodeService.class, IConfigurationService.class, IStatisticService.class, IClusterService.class);
                return (IStatisticManager) cons.newInstance(parameterService, nodeService,
                        configurationService, statisticService, clusterService);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return new StatisticManager(parameterService, nodeService,
                configurationService, statisticService, clusterService);
    }

    @Override
    protected ISymmetricDialect createSymmetricDialect() {
        ISymmetricDialect dialect = new JdbcSymmetricDialectFactory(parameterService, platform).create();
        dialect.setTargetPlatform(targetPlatform);
        return dialect;
    }

    @Override
    public ITypedPropertiesFactory createTypedPropertiesFactory() {
        try {
            return createTypedPropertiesFactory(new ClassPathResource(getConfigurationFile()).getFile(), properties);
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }

    @Override
    protected IDatabasePlatform createDatabasePlatform(TypedProperties properties) {
        return createDatabasePlatform(properties, dataSource);
    }

    @Override
    protected SecurityServiceFactory.SecurityServiceType getSecurityServiceType() {
        return SecurityServiceFactory.SecurityServiceType.CLIENT;
    }

    private static ITypedPropertiesFactory createTypedPropertiesFactory(File propFile, Properties prop) {
        String propFactoryClassName = System.getProperties().getProperty(PROPERTIES_FACTORY_CLASS_NAME);
        ITypedPropertiesFactory factory;
        if (propFactoryClassName != null) {
            try {
                factory = (ITypedPropertiesFactory) Class.forName(propFactoryClassName).newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            factory = new TypedPropertiesFactory();
        }
        factory.init(propFile, prop);
        return factory;
    }

    public List<File> listSnapshots() {
        File snapshotsDir = SnapshotUtil.getSnapshotDirectory(this);
        List<File> files = new ArrayList<>(FileUtils.listFiles(snapshotsDir, new String[]{"zip"}, false));
        files.sort((o1, o2) -> -o1.compareTo(o2));
        return files;
    }

    private void checkLoadOnly() {
        TypedProperties properties = new TypedProperties();
        if (parameterService.is(ParameterConstants.NODE_LOAD_ONLY, false)) {
            String[] sqlTemplateProperties = new String[]{
                    ParameterConstants.DB_FETCH_SIZE,
                    ParameterConstants.DB_QUERY_TIMEOUT_SECS,
                    ParameterConstants.JDBC_EXECUTE_BATCH_SIZE,
                    ParameterConstants.JDBC_ISOLATION_LEVEL,
                    ParameterConstants.JDBC_READ_STRINGS_AS_BYTES,
                    ParameterConstants.TREAT_BINARY_AS_LOB_ENABLED,
                    ParameterConstants.LOG_SLOW_SQL_THRESHOLD_MILLIS,
                    ParameterConstants.LOG_SQL_PARAMETERS_INLINE
            };
            for (String prop : sqlTemplateProperties) {
                properties.put(prop, parameterService.getString(ParameterConstants.LOAD_ONLY_PROPERTY_PREFIX + prop));
            }

            String[] kafkaProperties = new String[]{
                    ParameterConstants.KAFKA_PRODUCER,
                    ParameterConstants.KAFKA_MESSAGE_BY,
                    ParameterConstants.KAFKA_TOPIC_BY,
                    ParameterConstants.KAFKA_FORMAT
            };

            for (String prop : kafkaProperties) {
                properties.put(prop, parameterService.getString(prop));
            }

            targetPlatform = createDatabasePlatform(properties, null);
            DataSource loadDataSource = targetPlatform.getDataSource();
            if (targetPlatform instanceof GenericJdbcDatabasePlatform) {
                if (targetPlatform.getName() == null || targetPlatform.getName().equals(DatabaseNamesConstants.GENERIC)) {
                    String name = null;
                    try {
                        String[] nameVersion = JdbcDatabasePlatformFactory.determineDatabaseNameVersionSubprotocol(loadDataSource);
                        name = (String.format("%s%s", nameVersion[0], nameVersion[1]).toLowerCase());
                    } catch (Exception e) {
                        log.info("Unable to determine database name and version, " + e.getMessage());
                    }
                    if (name == null) {
                        name = DatabaseNamesConstants.GENERIC;
                    }
                    ((GenericJdbcDatabasePlatform) targetPlatform).setName(name);
                }
                targetPlatform.getDatabaseInfo().setNotNullColumnsSupported(parameterService.is(ParameterConstants.LOAD_ONLY_PROPERTY_PREFIX + ParameterConstants.CREATE_TABLE_NOT_NULL_COLUMNS, true));
            }
            getSymmetricDialect().setTargetPlatform(targetPlatform);
        } else {
            getSymmetricDialect().setTargetPlatform(getSymmetricDialect().getPlatform());
        }
    }

    public File snapshot() {
        return SnapshotUtil.createSnapshot(this);
    }

    protected  String getConfigurationFile() {
        return PROPERTIES_FILE;
    }

    @Override
    public ISymmetricDialect getSymmetricDialect() {
        if (dialect == null) {
            dialect = createSymmetricDialect();
            dialect.setTargetPlatform(platform);
        }
        return dialect;
    }

    @Override
    public IMonitorService getMonitorService() {
        return monitorService;
    }

    @Bean
    public IMonitorService getMonitorServiceBean() {
        return monitorService;
    }

    @Bean
    public ISecurityService getSecurityServiceBean() {
        return securityService;
    }

    @Bean
    public ISymmetricDialect getSymmetricDialectBean() {
        return createSymmetricDialect();
    }

    @Bean
    public IParameterService getParameterServiceBean() {
        return parameterService;
    }

    @Bean
    public IMailService getMailServiceBean() {
        return mailService;
    }

    @Bean
    public INodeService getNodeServiceBean() {
        return nodeService;
    }

    @Bean
    public IConfigurationService getConfigurationServiceBean() {
        return configurationService;
    }

    @Bean
    public IBandwidthService getBandwidthServiceBean() {
        return bandwidthService;
    }

    @Bean
    public IStatisticService getStatisticServiceBean() {
        return statisticService;
    }

    @Bean
    public IStatisticManager getStatisticManagerBean() {
        return statisticManager;
    }

    @Bean
    public IConcurrentConnectionManager getConcurrentConnectionManagerBean() {
        return concurrentConnectionManager;
    }

    @Bean
    public ITransportManager getTransportManagerBean() {
        return transportManager;
    }

    @Bean
    public IClusterService getClusterServiceBean() {
        return clusterService;
    }

    @Bean
    public IPurgeService getPurgeServiceBean() {
        return purgeService;
    }

    @Bean
    public ITransformService getTransformServiceBean() {
        return transformService;
    }

    @Bean
    public ILoadFilterService getLoadFilterServiceBean() {
        return loadFilterService;
    }

    @Bean
    public ITriggerRouterService getTriggerRouterServiceBean() {
        return triggerRouterService;
    }

    @Bean
    public IOutgoingBatchService getOutgoingBatchServiceBean() {
        return outgoingBatchService;
    }

    @Bean
    public IDataService getDataServiceBean() {
        return dataService;
    }

    @Bean
    public IRouterService getRouterServiceBean() {
        return routerService;
    }

    @Bean
    public IRegistrationService getRegistrationServiceBean() {
        return registrationService;
    }

    @Bean
    public IJobManager getJobManagerBean() {
        return this.jobManager;
    }

    @Bean
    public IAcknowledgeService getAcknowledgeServiceBean() {
        return this.acknowledgeService;
    }

    @Bean
    public IDataExtractorService getDataExtractorServiceBean() {
        return this.dataExtractorService;
    }

    //@Bean
    public IDataExtractorService getFileSyncExtractorServiceBean() {
        return this.fileSyncExtractorService;
    }

    @Bean
    public IDataLoaderService getDataLoaderServiceBean() {
        return this.dataLoaderService;
    }

    @Bean
    public IIncomingBatchService getIncomingBatchServiceBean() {
        return this.incomingBatchService;
    }

    @Bean
    public IPullService getPullServiceBean() {
        return this.pullService;
    }

    @Bean
    public IPushService getPushServiceBean() {
        return this.pushService;
    }

    @Bean
    public IOfflinePullService getOfflinePullServiceBean() {
        return this.offlinePullService;
    }

    @Bean
    public IOfflinePushService getOfflinePushServiceBean() {
        return this.offlinePushService;
    }

    @Bean
    public IStagingManager getStagingManagerBean() {
        return stagingManager;
    }

    @Bean
    public ISequenceService getSequenceServiceBean() {
        return sequenceService;
    }

    @Bean
    public IExtensionService getIExtensionServiceBean() {
        return extensionService;
    }

    @Bean
    public IGroupletService getGroupletServiceBean() {
        return groupletService;
    }

    @Bean
    public INodeCommunicationService getNodeCommunicationServiceBean() {
        return nodeCommunicationService;
    }

    @Bean
    public IFileSyncService getFileSyncServiceBean() {
        return fileSyncService;
    }

    @Bean
    public IContextService getIContextServiceBean() {
        return contextService;
    }

    @Bean
    public IUpdateService getUpdateServiceBean() {
        return updateService;
    }

    @Bean
    public IDatabasePlatform getDatabasePlatformBean() {
        return databasePlatform;
    }

    protected void autoConfigRegistrationServer() {
        Node node = nodeService.findIdentity();

        if (node == null) {
            buildTablesFromDdlUtilXmlIfProvided();
            loadFromScriptIfProvided();
            parameterService.rereadParameters();
        }

        node = nodeService.findIdentity();

        if (node == null && parameterService.isRegistrationServer()
                && parameterService.is(ParameterConstants.AUTO_INSERT_REG_SVR_IF_NOT_FOUND, false)) {
            log.info("Inserting rows for node, security, identity and group for registration server");
            String nodeId = parameterService.getExternalId();
            node = new Node(parameterService, symmetricDialect);
            node.setNodeId(node.getExternalId());
            nodeService.save(node);
            nodeService.insertNodeIdentity(nodeId);
            node = nodeService.findIdentity();
            nodeService.insertNodeGroup(node.getNodeGroupId(), null);
            NodeSecurity nodeSecurity = nodeService.findOrCreateNodeSecurity(nodeId);
            nodeSecurity.setInitialLoadTime(new Date());
            nodeSecurity.setRegistrationTime(new Date());
            nodeSecurity.setInitialLoadEnabled(false);
            nodeSecurity.setRegistrationEnabled(false);
            nodeService.updateNodeSecurity(nodeSecurity);
        }
    }

    @PostConstruct
    public void startEngine() {
        start();
    }
}
