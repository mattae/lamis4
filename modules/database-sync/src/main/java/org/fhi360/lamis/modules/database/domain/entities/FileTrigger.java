/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.modules.database.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User10
 */
@Entity
@Table(name = "sym_file_trigger")
@Data
public class FileTrigger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "trigger_id")
    private Trigger trigger;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "channel_id")
    private Channel channel;

    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "reload_channel_id")
    private Channel reloadChannel;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "base_dir")
    private String baseDir;

    @Basic(optional = false)
    @NotNull
    @Column(name = "recurse")
    private short recurse;

    @Size(max = 255)
    @Column(name = "includes_files")
    private String includesFiles;

    @Size(max = 255)
    @Column(name = "excludes_files")
    private String excludesFiles;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_create")
    private short syncOnCreate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_modified")
    private short syncOnModified;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_delete")
    private short syncOnDelete;

    @Basic(optional = false)
    @NotNull
    @Column(name = "sync_on_ctl_file")
    private short syncOnCtlFile;

    @Basic(optional = false)
    @NotNull
    @Column(name = "delete_after_sync")
    private short deleteAfterSync;

    @Lob
    @Size(max = 16777215)
    @Column(name = "before_copy_script")
    private String beforeCopyScript;

    @Lob
    @Size(max = 16777215)
    @Column(name = "after_copy_script")
    private String afterCopyScript;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @Size(max = 50)
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime lastUpdateTime;

    @Lob
    @Size(max = 16777215)
    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fileTrigger")
    private List<FileTriggerRouter> fileTriggerRouters = new ArrayList<>();
}
