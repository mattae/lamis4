import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { XComponent } from './x.component';
import { YComponent } from './y.component';
import { LamisSharedModule, MatDateFormatModule } from '@lamis/web-core';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { MaterialModule } from 'app/material.module';
import { GridsterModule } from 'angular-gridster2';
import { CoreModule } from '@alfresco/adf-core';
import { CommonModule } from '@angular/common';
import { FormioModule } from 'angular-formio';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CellComponent } from './cell.component';

@NgModule({
    imports: [
        CommonModule,
        LamisSharedModule,
        RouterModule.forChild([HOME_ROUTE]),
        GridsterModule,
        MaterialModule,
        CoreModule,
        MatDateFormatModule,
        FormioModule,
        FlexLayoutModule
    ],
    declarations: [HomeComponent, XComponent, YComponent, CellComponent],
    entryComponents: [XComponent, YComponent],
    providers: []
})
export class LamisHomeModule {
}
