import { Component, OnDestroy, OnInit } from '@angular/core';
import { DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Router } from '@angular/router';

export interface Widget extends GridsterItem {
    component: string
}

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    options: GridsterConfig;
    dashboard: Array<Widget>;

    constructor(private rxStompService: RxStompService, private router: Router) {
    }

    ngOnInit() {
        this.options = {
            gridType: GridType.Fit,
            displayGrid: DisplayGrid.OnDragAndResize,
            pushItems: true,
            draggable: {
                enabled: true
            },
            resizable: {
                enabled: true
            },
            api: {},
            itemResizeCallback: (item, itemComponent) => {
                console.log('Item resize', itemComponent.gridster)
            },
            itemRemovedCallback: (item, itemComponent) => console.log('Gridster', itemComponent.gridster)
        };

        this.dashboard = [
            {cols: 2, rows: 1, y: 0, x: 0, component: 'x'},
            {cols: 2, rows: 2, y: 0, x: 2, component: 'x'},
            {cols: 1, rows: 1, y: 0, x: 4, component: 'x'},
            {cols: 3, rows: 2, y: 1, x: 4, component: 'y'},
            {cols: 1, rows: 1, y: 4, x: 5, component: 'x'},
            {cols: 1, rows: 1, y: 2, x: 1, component: 'x'},
            {cols: 2, rows: 2, y: 5, x: 5, component: 'y'},
            {cols: 2, rows: 2, y: 3, x: 2, component: 'x'},
            {cols: 2, rows: 1, y: 2, x: 2, component: 'y'},
            {cols: 1, rows: 1, y: 3, x: 4, component: 'y'},
            {cols: 1, rows: 1, y: 0, x: 6, component: 'x'}
        ];
    }

    changedOptions() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    removeItem($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        this.dashboard.splice(this.dashboard.indexOf(item), 1);
    }

    addItem() {
        this.dashboard.push({x: 0, y: 0, cols: 1, rows: 1, component: 'x'});
    }

    ngOnDestroy() {
    }

    form = {
        components: [
            {
                type: 'select',
                label: 'Make',
                key: 'make',
                placeholder: 'Select your make',
                dataSrc: 'values',
                validate: {
                    required: true
                },
                data: {
                    values: [
                        {
                            label: 'Chevy',
                            value: 'chevrolet'
                        },
                        {
                            value: 'honda',
                            label: 'Honda'
                        },
                        {
                            label: 'Ford',
                            value: 'ford'
                        },
                        {
                            label: 'Toyota',
                            value: 'toyota'
                        }
                    ]
                }
            },
            {
                type: 'select',
                label: 'Model',
                key: 'model',
                placeholder: 'Select your model',
                dataSrc: 'url',
                data: {
                    url: 'https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{{ row.make }}?format=json'
                },
                valueProperty: 'Model_Name',
                template: '<span>{{ item.Model_Name }}</span>',
                refreshOn: 'make',
                clearOnRefresh: true,
                selectValues: 'Results',
                validate: {
                    required: true
                }
            }
        ]
    };
}
