package org.fhi360.lamis.modules.datasource;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.fhi360.lamis.modules.base.tenant.util.TenantContextHolder;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class MariaDBDatasourceConfiguration {
    private int port = 3306;
    private String password = "password";
    private static DB db;
    private File keyFile;
    private File keyPasswordFile;
    private static final String KEY = "U2FsdGVkX1+PUTUO1EVRoEV+fsEXWerotpN+PfLWietoC+oCHyWwuhoPe8uXXB42Szq1LXG4KRiXV/c+vh+QzkxcNzV6IqA8PoHLzE2g7lwsFWmYDfOWX0WqStjk8RQz";
    private static final String KEY_PASSWORD = "696f54aae255e6256a8533fa5dba2e863570af30d3811d549a522724aae648305770da5b5a536e73c9b448f162a2fb3f640d27137495df06da06e9096c5cbe3ff57fe2b42830826e71912d94d9f339416476d9662f51718dc9c3c39ce054274fe024e99cefa7a9392cb7a601ed3d4ff9ecd4614a6f9826addc291ad27295f1fa";

    @PostConstruct
    public void init() throws ManagedProcessException, IOException {
        DBConfigurationBuilder configBuilder = DBConfigurationBuilder.newBuilder();
        keyFile = File.createTempFile(RandomStringUtils.randomAlphabetic(4),
                RandomStringUtils.randomAlphabetic(4));
        keyPasswordFile = File.createTempFile(RandomStringUtils.randomAlphabetic(4),
                RandomStringUtils.randomAlphabetic(4));
        byte[] bytes = Base64.getDecoder().decode(KEY);
        Files.write(keyFile.toPath(), bytes);
        try (FileWriter writer = new FileWriter(keyPasswordFile)){
            writer.write(KEY_PASSWORD);
        }
        configBuilder.setPort(3307);
        configBuilder.addArg("--plugin_load_add=file_key_management");
        configBuilder.addArg(String.format("--file-key-management-filename=%s",
                keyFile.getAbsolutePath()));
        configBuilder.addArg(String.format("--file_key_management_filekey=FILE:%s",
                keyPasswordFile.getAbsolutePath()));
        configBuilder.addArg("--file-key-management-encryption-algorithm=AES_CBC");
        configBuilder.addArg("--innodb-encrypt-tables");
        configBuilder.addArg("--innodb-encrypt-log");
        configBuilder.addArg("--innodb_encryption_threads=4");
        configBuilder.addArg("--skip-grant-tables");
        configBuilder.setDataDir("lamis/data"); // just an example
        configBuilder.setDeletingTemporaryBaseAndDataDirsOnShutdown(false);
        configBuilder.setSecurityDisabled(false);

        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "**");
        for (Resource resource : resources) {
            if (resource != null && resource.getFilename() != null &&
                    resource.getFilename().endsWith("file_key_management.dll")) {
                FileUtils.copyURLToFile(resource.getURL(),
                        new File(configBuilder.getBaseDir() + "/lib/plugin/file_key_management.dll"));
            }
            if (resource != null && resource.getFilename() != null &&
                    resource.getFilename().endsWith(".exe")) {
                FileUtils.copyURLToFile(resource.getURL(),
                        new File(configBuilder.getBaseDir() + "/bin/" + resource.getFilename()));
            }
        }
        db = DB.newEmbeddedDB(configBuilder.build());
        db.start();
        //password = UUID.randomUUID().toString();
        String updatePassword = "update mysql.user set password = password('%s') where user = 'root'";
        db.run(String.format(updatePassword, password),
                "root", "");
        db.createDB("masterdb", "root", password);
        db.createDB("lamis", "root", password);
        db.stop();
        configBuilder._getArgs().remove("--skip-grant-tables");
        db = DB.newEmbeddedDB(configBuilder.build());
        db.start();
        db.run(String.format("grant all on lamis.* to 'lamis'@'localhost' identified by '%s'", password), "root", password);
        db.run(String.format("grant all on masterdb.* to 'masterdb'@'localhost' identified by '%s'", password), "root", password);
        db.run("GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'mbackup'@'localhost' identified by 'backup'", "root", password);
        //db.run("GRANT PROXY ON ''@'' TO 'root'@'localhost' WITH GRANT OPTION", "root", password);
        //db.run("GRANT PROXY ON ''@'' TO 'root'@'127.0.0.1' WITH GRANT OPTION", "root", password);
        FileUtils.deleteQuietly(keyFile);
        FileUtils.deleteQuietly(keyPasswordFile);
        port = configBuilder.getPort();
    }

    public void backup() throws ManagedProcessException {
        db.run("", "root", "");
    }

    @Bean
    @ConditionalOnMissingBean(DataSource.class)
    public DataSource getDataSource() {
        RoutingDataSource dataSource = new RoutingDataSource();
        dataSource.setDefaultTargetDataSource(defaultDataSource());
        return dataSource;
    }

    private HikariDataSource defaultDataSource() {
        String url = String.format("jdbc:mysql://localhost:%s/lamis?tinyInt1isBit=false&serverTimezone=Africa/Lagos", port);
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url(url)
                .username("lamis")
                .password(password)
                .driverClassName("com.mysql.jdbc.Driver")
                .build();
    }

    class RoutingDataSource extends AbstractRoutingDataSource {
        public RoutingDataSource() {
            initDataSources();
        }

        @Override
        protected Object determineCurrentLookupKey() {
            return TenantContextHolder.getTenant();
        }

        private void initDataSources() {
            Map<Object, Object> dsMap = new HashMap<>();
            DataSource dataSource = DataSourceBuilder.create()
                    .type(HikariDataSource.class)
                    .url("jdbc:mysql://localhost:3306/tenant1?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false")
                    .username("lamis")
                    .password("lamis")
                    .driverClassName("com.mysql.jdbc.Driver")
                    .build();
            dsMap.put("tenant_1", dataSource);
            this.setTargetDataSources(dsMap);
        }
    }
}
