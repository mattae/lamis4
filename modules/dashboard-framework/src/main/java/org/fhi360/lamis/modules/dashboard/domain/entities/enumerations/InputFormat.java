package org.fhi360.lamis.modules.dashboard.domain.entities.enumerations;

public enum InputFormat {
    MULTI_SERIES, SINGLE_SERIES, TREE_MAP, MAP
}
