package org.fhi360.lamis.modules.dashboard.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.base.domain.entities.BaseEntity;
import org.fhi360.lamis.modules.base.domain.entities.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "dashboard")
@Data
public class Dashboard extends BaseEntity implements Serializable {
    @NotNull
    private String title;

    @NotNull
    private Integer position = 1;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "dashboard", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Widget> widgets = new HashSet<>();
}
