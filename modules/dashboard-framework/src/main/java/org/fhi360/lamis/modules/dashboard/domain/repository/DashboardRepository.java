package org.fhi360.lamis.modules.dashboard.domain.repository;

import org.fhi360.lamis.modules.base.domain.entities.User;
import org.fhi360.lamis.modules.dashboard.domain.entities.Dashboard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DashboardRepository extends JpaRepository<Dashboard, String> {
    @Query("select d from Dashboard d where d.user = ?1 or d.user is null")
    List<Dashboard> findByUser(User user);
}
