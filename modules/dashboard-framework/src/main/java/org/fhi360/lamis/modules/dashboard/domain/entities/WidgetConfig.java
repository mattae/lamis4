package org.fhi360.lamis.modules.dashboard.domain.entities;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import org.fhi360.lamis.modules.base.domain.entities.BaseEntity;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "dashboard_widget_config")
@Data
public class WidgetConfig extends BaseEntity implements Serializable {
    @OneToOne
    private Widget widget;

    @ManyToOne
    @NotNull
    private DataSource datasource;

    @Type(type = "json-node")
    @javax.persistence.Column(columnDefinition = "json")
    private JsonNode properties;

    private String formKey;
}
