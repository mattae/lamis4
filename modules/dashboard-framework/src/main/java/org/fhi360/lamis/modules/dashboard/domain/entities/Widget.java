package org.fhi360.lamis.modules.dashboard.domain.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@DiscriminatorValue("WIDGET")
@Data
@EqualsAndHashCode(of = "id")
public class Widget extends WidgetDefinition implements Serializable {
    @ManyToOne
    @NotNull
    private Dashboard dashboard;
}
