package org.fhi360.lamis.modules.dashboard.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.base.domain.entities.Authority;
import org.fhi360.lamis.modules.base.domain.entities.BaseEntity;
import org.fhi360.lamis.modules.dashboard.domain.entities.enumerations.InputFormat;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Column;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "dashboard_datasource")
public class DataSource extends BaseEntity implements Serializable {
    @NotNull
    private String name;

    @Lob
    private String description;

    @URL
    @NotNull
    private String url;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "input_format")
    @NotNull
    private InputFormat format;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "datasource_authorities",
            joinColumns = @JoinColumn(name = "datasource_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Authority> authorities = new ArrayList<>();
}
