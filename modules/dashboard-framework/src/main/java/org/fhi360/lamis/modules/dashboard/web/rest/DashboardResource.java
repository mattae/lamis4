package org.fhi360.lamis.modules.dashboard.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.Authority;
import org.fhi360.lamis.modules.base.domain.entities.User;
import org.fhi360.lamis.modules.base.domain.repositories.UserRepository;
import org.fhi360.lamis.modules.base.web.util.HeaderUtil;
import org.fhi360.lamis.modules.dashboard.domain.entities.Dashboard;
import org.fhi360.lamis.modules.dashboard.domain.entities.DataSource;
import org.fhi360.lamis.modules.dashboard.domain.entities.WidgetDefinition;
import org.fhi360.lamis.modules.dashboard.domain.entities.enumerations.InputFormat;
import org.fhi360.lamis.modules.dashboard.domain.repository.DashboardRepository;
import org.fhi360.lamis.modules.dashboard.domain.repository.DataSourceRepository;
import org.fhi360.lamis.modules.dashboard.domain.repository.WidgetDefinitionRepository;
import org.fhi360.lamis.modules.security.config.security.SecurityUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@Slf4j
public class DashboardResource {
    private final DashboardRepository dashboardRepository;
    private final UserRepository userRepository;
    private final DataSourceRepository dataSourceRepository;
    private final WidgetDefinitionRepository widgetDefinitionRepository;

    public DashboardResource(DashboardRepository dashboardRepository,
                             UserRepository userRepository,
                             DataSourceRepository dataSourceRepository,
                             WidgetDefinitionRepository widgetDefinitionRepository) {
        this.dashboardRepository = dashboardRepository;
        this.userRepository = userRepository;
        this.dataSourceRepository = dataSourceRepository;
        this.widgetDefinitionRepository = widgetDefinitionRepository;
    }

    @GetMapping("/dashboards")
    @Cacheable("dashboards")
    public ResponseEntity<List<Dashboard>> getDashboards() {
        List<Dashboard> dashboards = new ArrayList<>();
        User user = SecurityUtils.getCurrentUserLogin()
                .map(username -> userRepository.findOneByLogin(username).orElse(null)).orElse(null);

        if (user != null) {
            dashboards = dashboardRepository.findByUser(user);
        }
        return new ResponseEntity<>(dashboards, HttpStatus.OK);
    }

    @GetMapping("/dashboard/{id}")
    @Cacheable("dashboard")
    public ResponseEntity<Dashboard> getDashboard(@PathVariable String id) {
        LOG.debug("Get dashboard with id {}", id);

        return ResponseUtil.wrapOrNotFound(dashboardRepository.findById(id));
    }

    @PostMapping("/dashboards")
    public ResponseEntity<Dashboard> saveDashboard(@RequestBody Dashboard dashboard) throws URISyntaxException {
        LOG.debug("REST request to save Dashboard : {}", dashboard);

        dashboard = dashboardRepository.save(dashboard);
        return ResponseEntity.created(new URI("/api/dashboards/" + dashboard.getId()))
                .headers(HeaderUtil
                        .createAlert("A dashboard is created with identifier " + dashboard.getId(), dashboard.getId()))
                .body(dashboard);
    }

    @PutMapping("/dashboards")
    public ResponseEntity<Dashboard> updateDashboard(@RequestBody Dashboard dashboard) throws URISyntaxException {
        LOG.debug("REST request to update Dashboard : {}", dashboard);
        dashboard = dashboardRepository.save(dashboard);
        return ResponseEntity.created(new URI("/api/dashboards/" + dashboard.getId()))
                .headers(HeaderUtil
                        .createAlert("A dashboard is updated with identifier " + dashboard.getId(), dashboard.getId()))
                .body(dashboard);
    }

    @DeleteMapping("/dashboards/{id}")
    public ResponseEntity<Void> deleteDashboard(@PathVariable String id) {
        LOG.debug("Delete dashboard with id {}", id);

        dashboardRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Dashboard deleted", id)).build();
    }

    @GetMapping("/dashboards/datasource/{input-format}")
    public List<DataSource> dataSources(@PathVariable("input-format")InputFormat format) {
        String username = SecurityUtils.getCurrentUserLogin().orElse(null);
        LOG.debug("Get all dataSources consumable by current user: {}", username);
        if (username == null) {
            return new ArrayList<>();
        }
        List<DataSource> dataSources = dataSourceRepository.findByFormat(format);

        return dataSources.stream()
                .filter(dataSource -> {
                    for (Authority authority: dataSource.getAuthorities()) {
                        if (SecurityUtils.isCurrentUserInRole(authority.getId())) {
                            return true;
                        }
                    }
                    return false;
                }).collect(Collectors.toList());
    }

    @GetMapping("/dashboards/widget/widget-definitions")
    public List<WidgetDefinition> widgetDefinitions() {
        return widgetDefinitionRepository.findAll();
    }
}
