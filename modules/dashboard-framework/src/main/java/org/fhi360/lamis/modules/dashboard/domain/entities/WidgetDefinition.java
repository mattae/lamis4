package org.fhi360.lamis.modules.dashboard.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.base.domain.entities.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("null")
@Table(name = "dashboard_widget")
@Data
public class WidgetDefinition extends BaseEntity {
    @NotNull
    private String componentType;

    @NotNull
    private String description;

    @NotNull
    private String name;

    private String icon;

    @NotNull
    @Column(name = "x_pos")
    private Integer x = 0;

    @NotNull
    @Column(name = "y_pos")
    private Integer y = 0;

    @NotNull
    private Integer cols;

    @NotNull
    private Integer rows;

    @OneToMany(mappedBy = "widget", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Tag> tags = new HashSet<>();

    @OneToOne(mappedBy = "widget", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private WidgetConfig config;
}
