package org.fhi360.lamis.modules.dashboard.domain.repository;

import org.fhi360.lamis.modules.dashboard.domain.entities.WidgetDefinition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WidgetDefinitionRepository extends JpaRepository<WidgetDefinition, String> {
}
