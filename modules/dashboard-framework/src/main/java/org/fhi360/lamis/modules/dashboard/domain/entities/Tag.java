package org.fhi360.lamis.modules.dashboard.domain.entities;

import lombok.Data;
import org.fhi360.lamis.modules.base.domain.entities.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "dashboard_widget_tag")
@Data
public class Tag extends BaseEntity implements Serializable {
    @NotNull
    private String name;

    @NotNull
    private String facet;

    @ManyToOne
    @NotNull
    private Widget widget;
}
