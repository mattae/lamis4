package org.fhi360.lamis.modules.dashboard.config;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import org.fhi360.lamis.modules.dashboard.domain.DashboardDomain;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DashboardDomain.class)
public class DomainConfiguration {
}
