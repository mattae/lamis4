package org.fhi360.lamis.modules.dashboard.domain.repository;


import org.fhi360.lamis.modules.dashboard.domain.entities.DataSource;
import org.fhi360.lamis.modules.dashboard.domain.entities.enumerations.InputFormat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DataSourceRepository extends JpaRepository<DataSource, String> {
    List<DataSource> findByFormat(InputFormat format);
}
