import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { JsonFormModule } from '@lamis/web-core';
import { ConfigFormComponent } from './config-form.component';

@NgModule({
    imports: [
        CommonModule,
        MatTabsModule,
        MatSelectModule,
        JsonFormModule
    ],
    declarations: [
        ConfigFormComponent
    ],
    exports: [
        ConfigFormComponent
    ]
})
export class ConfigModule {

}