import { Component, EventEmitter, Output } from '@angular/core';
import { DataSource } from '../model/dashboard.model';
import { PropertyUpdateEvent } from '../widgets/_common/widget';

@Component({
    selector: 'config-form',
    templateUrl: './config-form.component.html'
})
export class ConfigFormComponent {
    formValid = false;
    config: any;
    template: string;
    datasource: DataSource;
    @Output()
    updatePropertiesEvent: EventEmitter<PropertyUpdateEvent> = new EventEmitter<PropertyUpdateEvent>();

    save() {
        const props = {
            dataSource: this.datasource,
            properties: this.config
        };
        this.updatePropertiesEvent.emit(props)
    }

    onData(data: any) {

    }
}