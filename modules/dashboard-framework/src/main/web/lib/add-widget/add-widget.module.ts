import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AddWidgetComponent } from './add-widget.component';
import { AddWidgetService } from '../services/add-widget.service';
import { DataListModule } from '../datalist/data-list.module';
import { MatButtonModule, MatCardModule, MatChipsModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
	imports: [
		CommonModule,
		DataListModule,
		FlexLayoutModule,
		MatButtonModule,
		MatCardModule,
		MatChipsModule,
		MatDialogModule,
		PerfectScrollbarModule
	],
	declarations: [
		AddWidgetComponent
	],
	providers: [
		AddWidgetService
	],
	exports: [
		AddWidgetComponent
	]
})
export class AddWidgetModule {
}

