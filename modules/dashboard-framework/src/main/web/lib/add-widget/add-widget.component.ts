import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';

import { animate, style, transition, trigger } from '@angular/animations';

import { AddWidgetService } from '../services/add-widget.service';
import { Facet } from '../facet/facet-model';
import { FacetTagProcessor } from '../facet/facet-tag-processor';
import { Widget } from '../model/dashboard.model';
import { MatDialog } from '@angular/material';

/**
 */
@Component({
    selector: 'add-widget-modal',
    templateUrl: './add-widget.component.html',
    styleUrls: ['./styles.css'],
    animations: [
        trigger(
            'showHideAnimation',
            [
                transition(':enter', [   // :enter is alias to 'void => *'
                    style({opacity: 0}),
                    animate(750, style({opacity: 1}))
                ]),
                transition(':leave', [   // :leave is alias to '* => void'
                    animate(750, style({opacity: 0}))
                ])
            ])
    ]

})
export class AddWidgetComponent implements AfterViewInit {

    @Output() addWidgetEvent: EventEmitter<any> = new EventEmitter();

    widgetObjectList: Widget[] = [];
    gadgetObjectTitleList: string[] = [];
    placeHolderText = 'Begin typing widget name';
    layoutColumnOneWidth = '33.33';
    layoutColumnTwoWidth = '66.66';
    listHeader = 'Widgets';
    facetTags: Array<Facet>;

    typeAheadIsInMenu = false;

    constructor(private _addWidgetService: AddWidgetService, private dialog: MatDialog) {
        this.getObjectList();
    }

    actionHandler(actionItem) {
        this.addWidgetEvent.emit(actionItem);
    }

    ngAfterViewInit() {
    }

    getObjectList() {
        this._addWidgetService.getWidgetLibrary().subscribe(data => {
            this.widgetObjectList.length = 0;
            const me = this;
            data.forEach(function (item) {
                me.widgetObjectList.push(item);
                me.gadgetObjectTitleList.push(item.name);
            });
            const facetTagProcess = new FacetTagProcessor(this.widgetObjectList);
            this.facetTags = facetTagProcess.getFacetTags();
        });
    }
}
