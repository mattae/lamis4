import { DonutWidgetComponent } from '../widgets/donut/donut-widget.component';
import { BubbleWidgetComponent } from '../widgets/bubble/bubble-widget.component';
import { BarChartWidgetComponent } from '../widgets/barchart/bar-chart-widget.component';
import { PieChartWidgetComponent } from '../widgets/piechart/pie-chart-widget.component';
import { Injectable } from '@angular/core';
import { DynamicComponentMapper, DynamicComponentResolveFunction, DynamicComponentResolver } from '@alfresco/adf-core';

@Injectable({
    providedIn: 'root'
})
export class WidgetFactoryService extends DynamicComponentMapper {
    protected types: { [key: string]: DynamicComponentResolveFunction } = {
        'DonutWidgetComponent': DynamicComponentResolver.fromType(DonutWidgetComponent),
        'PieChartWidgetComponent': DynamicComponentResolver.fromType(PieChartWidgetComponent),
        'BarChartWidgetComponent': DynamicComponentResolver.fromType(BarChartWidgetComponent),
        'BubbleWidgetComponent': DynamicComponentResolver.fromType(BubbleWidgetComponent)
    };
}
