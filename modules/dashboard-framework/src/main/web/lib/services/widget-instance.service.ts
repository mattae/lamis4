import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Dashboard } from '../model/dashboard.model';


@Injectable()
export class WidgetInstanceService {

    private concreteWidgetInstances: any[] = [];
    private model: Dashboard;
    private subject: Subject<string> = new Subject<string>();
    private subscribers: Array<Subject<string>> = [];

    constructor() {
    }

    addInstance(widget: any) {
        let widgetFound = false;

        for (let x = 0; x < this.concreteWidgetInstances.length; x++) {
            if (widget.instance.instanceId === this.concreteWidgetInstances[x]['instance']['instanceId']) {
                widgetFound = true;
            }
        }

        if (widgetFound === false) {
            this.concreteWidgetInstances.push(widget);
        }
    }

    enableConfigureMode() {
        this.concreteWidgetInstances.forEach((widget) => {
            widget.instance.toggleConfigMode();
        });
    }

    removeInstance(id: number) {

        console.log('REMOVING WIDGET');
        // remove instance representation from model
        this.model.rows.forEach((row) => {
            row.columns.forEach((column) => {
                if (column.widgets) {
                    for (let i = column.widgets.length - 1; i >= 0; i--) {
                        if (column.widgets[i].instanceId === id) {
                            column.widgets.splice(i, 1);
                            break;
                        }
                    }
                }
            });
        });

        // removes concrete instance from service
        for (let x = this.concreteWidgetInstances.length - 1; x >= 0; x--) {
            if (this.concreteWidgetInstances[x].instance.instanceId === id) {
                const _widget = this.concreteWidgetInstances.splice(x, 1);
                _widget[0].destroy();
                break;
            }
        }
        // raise an event indicating a widget was removed
        this.subject.next('widget id: ' + id);
    }

    getInstanceCount() {
        return this.concreteWidgetInstances.length;
    }

    /*
     this allows this service to update the board when a delete operation occurs
     */
    setCurrentModel(model: Dashboard) {
        this.model = model;
    }

    /*
     raise an event that the grid.component is listening for when a widget is removed.
     */
    listenForInstanceRemovedEventsFromWidgets(): Observable<string> {
        return this.subject.asObservable();
    }

    addSubscriber(subscriber: any) {
        this.subscribers.push(subscriber);
    }

    unSubscribeAll() {
        this.subscribers.forEach(subscription => {
            subscription.unsubscribe();
        });

        this.subscribers.length = 0;
        this.clearAllInstances();

    }

    clearAllInstances() {
        this.concreteWidgetInstances.length = 0;
    }
}
