import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { defaultBoard } from './default-board';
import { sampleBoardCollectionProd } from './configuration-sample-boards-prod.model';
import { sampleBoardCollection } from './configuration-sample-boards.model';
import { Dashboard, DataSource, Widget, WidgetConfig } from '../model/dashboard.model';
import { SERVER_API_URL_CONFIG, ServerApiUrlConfig } from '@lamis/web-core';


@Injectable()
export class ConfigurationService {
    currentModel: Dashboard;
    defaultBoard: any;
    private readonly resourceUrl: string;

    constructor(private _http: HttpClient, @Inject(SERVER_API_URL_CONFIG) apiUrlConfig: ServerApiUrlConfig) {
        this.resourceUrl = apiUrlConfig.SERVER_API_URL + 'api/dashboards';
        Object.assign(this, {defaultBoard});
        Object.assign(this, {sampleBoardCollection});
        Object.assign(this, {sampleBoardCollectionProd});
    }

    public getBoards(): Observable<Dashboard[]> {
        return this._http.get<Dashboard[]>(this.resourceUrl);
    }

    public saveBoard(board: Dashboard): Observable<Dashboard> {
        console.log('Board to save', board);
        this.currentModel = board;
        return this._http.post<Dashboard>(this.resourceUrl, board, {observe: 'body'});
    }

    public deleteBoard(id: string) {
        return this._http.delete(this.resourceUrl + '/' + id);
    }

    public getDefaultBoard() {
        return new Observable(observer => {
            observer.next(this.defaultBoard);
            return () => {
            };
        });
    }

    /*
     when a widget instance's property page is updated and saved, the change gets communicated to all
     widgets. The widget instance id that caused the change will update their current instance. todo - this might be able to be
     improved. For now the utility of this approach allows the configuration service to capture the property page change in a way
     that allows us to update the persisted board model.
     */
    notifyWidgetOnPropertyChange(properties: any, dataSource: DataSource, instanceId: number) {
        this.savePropertyPageConfigurationToStore(properties, dataSource, instanceId);
    }

    setCurrentModel(_currentModel: any) {
        this.currentModel = _currentModel;
    }

    savePropertyPageConfigurationToStore(properties: any, dataSource: DataSource, instanceId: number) {
        this.currentModel.rows.forEach(row => {
            row.columns.forEach(column => {
                if (column.widgets) {
                    column.widgets.forEach(widget => {
                        this.updateProperties(properties, widget, dataSource, instanceId);
                    });
                }
            });
        });

        this.saveBoard(this.currentModel).subscribe(result => {
                this.currentModel = result;
                console.debug('The following configuration model was saved!');

            },
            error => console.error('Error' + error),
            () => console.debug('Saving configuration to store!')
        );
    }

    updateProperties(properties: any, widget: Widget, dataSource: DataSource, instanceId: number) {
        if (widget.instanceId === instanceId) {
            widget.config.dataSource = dataSource;
            widget.config.pages.forEach( (propertyPage) => {
                for (let x = 0; x < propertyPage.properties.length; x++) {
                    for (const prop in properties) {
                        if (properties.hasOwnProperty(prop)) {
                            if (prop === propertyPage.properties[x].name) {
                                propertyPage.properties[x].value = properties[prop];
                            }
                        }
                    }
                }
            });
        }
    }
}
