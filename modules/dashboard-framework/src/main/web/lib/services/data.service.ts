import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FilterService } from './filter.service';

@Injectable()
export class DataService {
	private filter: any;

	constructor(private http: HttpClient, private filterService: FilterService) {
		filterService.$filter.subscribe((value) => this.filter = value)
	}

	getData(url: string) {
		const params = new HttpParams()
			.set("to", this.filter['to'])
			.set('from', this.filter['from'])
			.set('level', this.filter['level'])
			.set('value', this.filter['value']);

		return this.http.get(`${url}`, {observe: 'body', params: params});
	}
}
