import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL_CONFIG, ServerApiUrlConfig } from '@lamis/web-core';
import { Widget } from '../model/dashboard.model';

@Injectable()
export class AddWidgetService {
    private readonly resourceUrl: string;

    constructor(private _http: HttpClient, @Inject(SERVER_API_URL_CONFIG) apiUrlConfig: ServerApiUrlConfig) {
        this.resourceUrl = apiUrlConfig.SERVER_API_URL + 'api/dashboards';
    }

    getWidgetLibrary(): Observable<Widget[]> {
        return this._http.get<Widget[]>('/widget/widget-definitions');
    }

}
