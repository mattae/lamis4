import { GridsterConfig, GridsterItem } from 'angular-gridster2';

export enum InputFormat {
    MULTI_SERIES, SINGLE_SERIES, BUBBLE, GRAPH, COMBO_CHART, CALENDAR_DATA, STATUS_DATA,
    MULTI_SERIES_NEGATIVE, SINGLE, TREE_MAP, MAP
}

export enum DisplayGrid {
    Always = 'always',
    OnDragAndResize = 'onDrag&Resize',
    None = 'none'
}

export enum CompactType {
    None = 'none',
    CompactUp = 'compactUp',
    CompactLeft = 'compactLeft',
    CompactUpAndLeft = 'compactUp&Left',
    CompactLeftAndUp = 'compactLeft&Up',
    CompactRight = 'compactRight',
    CompactUpAndRight = 'compactUp&Right',
    CompactRightAndUp = 'compactRight&Up',
}

export enum GridType {
    Fit = 'fit',
    ScrollVertical = 'scrollVertical',
    ScrollHorizontal = 'scrollHorizontal',
    Fixed = 'fixed',
    VerticalFixed = 'verticalFixed',
    HorizontalFixed = 'horizontalFixed'
}

// https://github.com/tiberiuzuld/angular-gridster2/blob/v7.2.0/projects/angular-gridster2/src/lib/gridsterConfig.interface.ts

// tslint:disable-next-line:no-empty-interface
export interface DashboardConfig extends GridsterConfig {
}

export type Property = { [key: string]: any }

export interface DataSource {
    url: string;
    name: string;
    description: string;
    format: InputFormat;
    id?: string;
}

export interface Dashboard {
    id?: string;
    position?: number;
    title?: string;
    widgets: Widget[];
}

export interface Widget extends GridsterItem {
    id?: string;
    componentType?: string;
    description?: string;
    name?: string;
    icon?: string;
    tags?: Tag[];
    config?: WidgetConfig;
}

export interface Tag {
    id?: string;
    facet: string;
    name: string;
}

export interface WidgetConfig {
    id?: string;
    format?: InputFormat;
    properties?: Property;
    formKey?: string;
    layoutKey?: string;
    dataSource?: DataSource;
}
