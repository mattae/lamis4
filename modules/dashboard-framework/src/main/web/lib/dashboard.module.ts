import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddWidgetModule } from './add-widget/add-widget.module';
import { BoardModule } from './board/board.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { DataListModule } from './datalist/data-list.module';
import { DynamicFormModule } from './dynamic-form/dynamic-form-module';
import { FacetModule } from './facet/facet.module';
import { GridModule } from './grid/grid.module';
import { LayoutModule } from './layout/layout.module';
import { MenuModule } from './menu/menu.module';
import { TypeAheadInputModule } from './typeahead-input/typeahead-input.module';
import { WidgetModule } from './widgets/widget.module';
import { DataService } from './services/data.service';
import { WidgetInstanceService } from './services/widget-instance.service';
import { ConfigurationService } from './services/configuration.service';
import { OptionsService } from './configuration/tab-options/service';
import { AddWidgetService } from './services/add-widget.service';
import { FilterService } from './services/filter.service';

@NgModule({
    imports: [
        CommonModule,
        AddWidgetModule,
        BoardModule,
        ConfigurationModule,
        DataListModule,
        DynamicFormModule,
        FacetModule,
        GridModule,
        LayoutModule,
        MenuModule,
        TypeAheadInputModule,
        WidgetModule
    ],
    providers: [
        DataService,
        FilterService,
        WidgetInstanceService,
        ConfigurationService,
        OptionsService,
        AddWidgetService
    ]
})
export class DashboardModule {
    
}