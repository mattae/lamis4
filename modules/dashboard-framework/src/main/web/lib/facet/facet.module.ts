import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FacetComponent } from './facet-component';
import { FilterListComponent } from './filter-list-component';
import { FilterTagComponent } from './filter-tag-component';
import { CapitalizeFirstPipe } from './capitalize-first-character-pipe';
import { MatCheckboxModule, MatChipsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCheckboxModule,
        MatChipsModule
    ],
    declarations: [
        FacetComponent,
        FilterListComponent,
        FilterTagComponent,
        CapitalizeFirstPipe
    ],
    providers: [
    ],
    exports: [
        FacetComponent,
        FilterListComponent,
        FilterTagComponent,
        CapitalizeFirstPipe
    ]
})
export class FacetModule {
}
