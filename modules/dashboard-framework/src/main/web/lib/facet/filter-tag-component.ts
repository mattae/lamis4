import { Component, EventEmitter, Output } from '@angular/core';

import { animate, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'filter-tag',
    template: `
        <mat-chip-list [@showHideAnimation] *ngFor='let tag of filterList'>
            <mat-chip
                    mat-sm-chip>
                {{tag}}
            </mat-chip>
        </mat-chip-list>
    `,
    styleUrls: ['../widgets/_common/widget-header.css'],
    animations: [
        trigger(
            'showHideAnimation',
            [
                transition(':enter', [   // :enter is alias to 'void => *'
                    style({opacity: 0}),
                    animate(750, style({opacity: 1}))
                ]),
                transition(':leave', [   // :leave is alias to '* => void'
                    animate(750, style({opacity: 0}))
                ])
            ])
    ]
})
export class FilterTagComponent {
    @Output() updateFilterListEvent = new EventEmitter<any>();

    filterList: Array<string> = [];

    constructor() {
    }

    updateFilterList(filter) {
        filter = filter.toLocaleLowerCase();
        const index: number = this.filterList.indexOf(filter);
        if (index !== -1) {
            this.filterList.splice(index, 1);
        } else {
            this.filterList.push(filter);
        }
        this.updateFilterListEvent.emit(this.filterList);
    }
}
