import { WidgetInstanceService } from '../../services/widget-instance.service';
import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { WidgetConfig } from '../../model/dashboard.model';
import { IWidget, PropertyUpdateEvent } from './widget';
import { DataService } from '../../services/data.service';
import { interval } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import set from 'lodash/set';
import get from 'lodash/set';


export abstract class WidgetBase implements IWidget, OnDestroy, OnInit, AfterViewInit {
    title: string;
    instanceId: number;
    config: WidgetConfig;
    widgetTags: Array<any>;
    data: any[] = [];
    subscription: any;
    protected POLL_INTERVAL = 15000;

    /**
     * Used to determine when to show the controls that appear in the widgets
     * heading area. This is set by the mouseover/mouseout events.
     * @type {boolean}
     */
    showControls = false;

    /**
     * determines whether to show the widgets property page
     * @type {boolean}
     */
    inConfig = false;

    /**
     * Determines if a widget is running or not
     * @type {boolean}
     */
    inRun = false;

    /**
     * When a widget is manually put into run mode this property will be used to
     * display a spinning icon and will be enabled between initiating an operation (run or stop)
     * to the operation is enabled
     * @type {boolean}
     */
    actionInitiated = false;

    /**
     * Gadgets that are of type realtime have a run/stop set of controls.
     * Those widgets should set this property to true. This property's visibility
     * will also be controlled by whether the widget's configuration form is valid.
     * @type {boolean}
     */
    showOperationControls = false;
    /**
     * This property is used to simply allow the widget to not show any run/stop controls.
     * This is needed because the showOperationControls does something similar but not exactly the same.
     * The showOperationControls property allows the widgetBase to determine if the run control, if the widget
     * uses it, to be displayed when the widget has a valid configuration.
     *
     * Default: true - Gadgets without a need for run/stop control should override this value.
     * @type {boolean}
     */
    widgetHasOperationControls = true;

    /**
     * Most widgets need configuration so widgets that don't can override this property
     * @type {boolean}
     */
    showConfigurationControl = true;

    // internally controls dynamic form properties
    errorExists = false;
    globalOptions: any;

    protected constructor(protected _widgetInstanceService: WidgetInstanceService,
                          protected _dataService: DataService) {
    }

    public ngOnInit() {
        this.toggleConfigMode();
    }

    public ngAfterViewInit() {
        this.preRun();
    }

    public toggleConfigMode() {
        this.inConfig = !this.inConfig;
    }

    protected updateData() {
        this.data = [];
        this.subscription = interval(this.POLL_INTERVAL).pipe(
            startWith(0), switchMap(() => {
                if (this.inRun) {
                    return this.getData()
                }
            }))
            .subscribe(data => {
                    Object.assign(this, data);
                },
                error => console.log(error)
            );

    }

    public updateProperties(event: PropertyUpdateEvent): void {
        this.config.dataSource = event.dataSource;
        event.properties.forEach((prop) => {
            const key = Object.keys(prop)[0];
            set(this.config.properties, key, prop[key]);
        });
    }

    public abstract run(): void

    public abstract stop(): void

    public abstract preRun(): void

    public initializeRunState(forceRunState: boolean) {
        this.errorExists = false;
        this.actionInitiated = true;
        this.inConfig = false;
        if (forceRunState) {
            this.setInRunState();
        }
    }

    public setInRunState() {
        this.inRun = true;
        this.actionInitiated = false;
    }

    public setStopState(longRunningStopAction: boolean) {
        /**
         *  If the widget indicates longRunningStopAction then the widget has to set this value to
         *  false once the operation is complete
         */
        this.actionInitiated = longRunningStopAction;
        this.inRun = false;
    }

    public remove() {
        this._widgetInstanceService.removeInstance(this.instanceId);
    }

    public showWidgetControls(enable: boolean) {
        this.showControls = enable;
    }

    /**
     * called from cell.component after the widget is created during runtime
     * instanceId, config, title and endpoint are common to all widgets. Once the widgets are configured
     * we give them an opportunity to perform an action during the preRun() method. For example,
     * the statistic widget uses preRun() to make a single call to the endpoint to update its display.
     * */
    public configureWidget(instanceId: number, config: WidgetConfig, tags: Array<any>) {
        this.instanceId = instanceId;
        this.config = config;
        this.widgetTags = tags.slice();
        this.setTitle(this.getPropFromPropertyPages('title'));

        /**
         *  Todo - remove this preRun call and refactor remaining code. PreRun was called twice and had an impact on the barchart api calls.
         *  API calls continued after route changes which is undesirable. See ngAfterViewInit where it is also called from.
         */
        // this.preRun();

    }

    public ngOnDestroy() {

    }

    protected setTitle(title: string) {
        this.title = title;
    }

    protected getPropFromPropertyPages(prop: string) {
        return get(this.config.properties, prop);
    }

    protected getEndpoint(): string {
        this.config.properties.name = '';
        return this.config.dataSource.url;
    }

    public drillDown(data) {
        this.inRun = false;

        this.getData().subscribe((data) => {
            Object.assign(this, data);
        })
    }

    protected getData() {
        return this._dataService.getData(this.getEndpoint())
    }
}
