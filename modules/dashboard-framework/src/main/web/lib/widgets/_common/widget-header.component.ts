import { Component, EventEmitter, Input, Output } from '@angular/core';


@Component({
    selector: 'widget-header',
    templateUrl: './widget-header.html',
    styleUrls: ['./widget-header.css']
})
export class WidgetHeaderComponent {
    @Input() title: string;
    @Input() showControls: boolean;
    @Input() inRun: boolean;
    @Input() inConfig: boolean;
    @Input() actionInitiated: boolean;
    @Input() showOperationControls: boolean;
    @Input() showConfigurationControl: boolean;
    @Input() widgetHasOperationControls: boolean;
    @Input() globalOptions: any;
    @Output() removeEvent: EventEmitter<any> = new EventEmitter();
    @Output() toggleConfigModeEvent: EventEmitter<any> = new EventEmitter();
    @Output() runEvent: EventEmitter<any> = new EventEmitter();
    @Output() stopEvent: EventEmitter<any> = new EventEmitter();
    @Output() helpEvent: EventEmitter<any> = new EventEmitter();


    remove() {
        this.removeEvent.emit();
    }

    toggleConfigMode() {
        this.toggleConfigModeEvent.emit();
    }

    run() {
        this.runEvent.emit();
    }

    stop() {
        this.stopEvent.emit();
    }

    help() {
        this.helpEvent.emit();
    }
}
