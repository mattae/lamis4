import { DataSource } from '../../model/dashboard.model';

export type PropertyUpdateEvent = { properties: { [key: string]: any }, dataSource: DataSource };

export interface IWidget {

    run();

    stop();

    toggleConfigMode();

    updateProperties(event: PropertyUpdateEvent);

    remove();

    showWidgetControls(enable: boolean);

    configureWidget(instanceId: number, config: any, tags: Array<any>);

}
