import { Series } from './series.model';

export abstract class Bar {

    public name: string;
    public series: Array<Series>;


    protected constructor(name: string, series: Array<Series>) {
        this.name = name;
        this.series = series;
    }

}
