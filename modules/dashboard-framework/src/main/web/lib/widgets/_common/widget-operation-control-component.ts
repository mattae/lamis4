import { Component, EventEmitter, Input, Output } from '@angular/core';


@Component({
	selector: 'widget-operation-control',
	template: `
        <button class="right floated"
                mat-icon-button
                *ngIf="!inRun && !actionInitiated && showOperationControls && gadgetHasOperationControls"
                (click)="run()">
	        <mat-icon color="primary">info</mat-icon>
        </button>

        <button class="right floated"
                mat-icon-button
                *ngIf="!inRun && actionInitiated && showOperationControls && gadgetHasOperationControls">
            <mat-icon>cached</mat-icon>
        </button>

        <button class="right floated"
                mat-icon-button
                *ngIf="inRun && !actionInitiated && showOperationControls && gadgetHasOperationControls"
                (click)="stop()">
	        <mat-icon>stop</mat-icon>
        </button>
	`,
})
export class WidgetOperationComponent {
	@Output() runEvent: EventEmitter<any> = new EventEmitter();
	@Output() stopEvent: EventEmitter<any> = new EventEmitter();

	@Input() inRun: boolean;
	@Input() actionInitiated: boolean;
	@Input() inConfig: boolean;
	@Input() showOperationControls: boolean;
	@Input() gadgetHasOperationControls: boolean;


	run() {
		this.runEvent.emit();
	}

	stop() {
		this.stopEvent.emit();
	}
}
