import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetHeaderComponent } from './widget-header.component';
import { WidgetOperationComponent } from './widget-operation-control-component';
import { HelpModalComponent } from './help-modal-component';
import { VisDrillDownComponent } from './vis-drill-down-component';
import { DndModule } from 'ng2-dnd';
import { MatButtonModule, MatIconModule, MatProgressBarModule, MatToolbarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
    imports: [
        CommonModule,
        DndModule.forRoot(),
        MatProgressBarModule,
        MatButtonModule,
        MatIconModule,
        FlexLayoutModule,
        MatToolbarModule
    ],
    declarations: [
        WidgetHeaderComponent,
        WidgetOperationComponent,
        HelpModalComponent,
        VisDrillDownComponent
    ],
    exports: [
        WidgetHeaderComponent,
        WidgetOperationComponent,
        HelpModalComponent,
        VisDrillDownComponent
    ]
})
export class WidgetSharedModule {
}
