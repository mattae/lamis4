import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticGadgetComponent } from './statistic/statistic-gadget.component';
import { TrendGadgetComponent } from './trend/trend-gadget.component';
import { WidgetSharedModule } from './_common/widget-shared.module';
import { DndModule } from 'ng2-dnd';
import { DynamicFormModule } from '../dynamic-form/dynamic-form-module';
import {
	MatButtonModule,
	MatCheckboxModule,
	MatExpansionModule,
	MatIconModule,
	MatInputModule,
	MatOptionModule,
	MatProgressBarModule,
	MatSelectModule,
	MatCardModule
} from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DataListModule } from '../datalist/data-list.module';
import { TypeAheadInputModule } from '../typeahead-input/typeahead-input.module';
import { FormsModule } from '@angular/forms';
import { FacetModule } from '../facet/facet.module';
import { TrendLineWidgetComponent } from './trend-line/trend-line-widget.component';
import { BubbleWidgetComponent } from './bubble/bubble-widget.component';
import { PieChartWidgetComponent } from './piechart/pie-chart-widget.component';
import { BarChartWidgetComponent } from './barchart/bar-chart-widget.component';
import { DrillDownComponent } from './donut/drill-down-component';
import { DonutWidgetComponent } from './donut/donut-widget.component';


@NgModule({
	imports: [
		CommonModule,
		WidgetSharedModule,
		DndModule.forRoot(),
		DynamicFormModule,
		NgxChartsModule,
		MatButtonModule,
		MatCardModule,
		MatIconModule,
		MatCheckboxModule,
		MatInputModule,
		MatProgressBarModule,
		MatExpansionModule,
		MatOptionModule,
		MatSelectModule,
		FormsModule,
		FacetModule,
		TypeAheadInputModule,
		DataListModule
	],
	declarations: [
		StatisticGadgetComponent,
		TrendGadgetComponent,
		TrendLineWidgetComponent,
		StatisticGadgetComponent,
		DonutWidgetComponent,
		DrillDownComponent,
		BubbleWidgetComponent,
		BarChartWidgetComponent,
		PieChartWidgetComponent
	],

	providers: [
	],

	exports: [
		StatisticGadgetComponent,
		TrendGadgetComponent,
		TrendLineWidgetComponent,
		StatisticGadgetComponent,
		DonutWidgetComponent,
		BubbleWidgetComponent,
		BarChartWidgetComponent,
		PieChartWidgetComponent
	]
})
export class WidgetModule {
}

