import { ChangeDetectorRef, Component } from '@angular/core';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { RuntimeService } from '../../services/runtime.service';
import { WidgetPropertyService } from '../_common/widget-property.service';
import { WidgetBase } from '../_common/widget-base';
import { TrendLineService } from './service';
import { OptionsService } from '../../configuration/tab-options/service';

declare var d3: any;

@Component({
    selector: 'dynamic-component',
    templateUrl: './trend-line.html',
    styleUrls: ['../_common/widget-header.css']
})

export class TrendLineWidgetComponent extends WidgetBase {


    topic: any;

    // chart options
    showXAxis = true;
    showYAxis = true;
    gradient = true;
    showLegend = true;
    showXAxisLabel = true;
    showYAxisLabel = true;
    yAxisLabel = 'IOPS';
    xAxisLabel = 'Time';
    autoScale = true;
    view: any[];
    colorScheme: any = {
        domain: ['#2185D0', '#0AFF16']
    };

    d3 = d3;
    multi: any[] = [];

    collectors: Array<string> = [];

    eventTimerSubscription: any;

    constructor(protected _trendLineService: TrendLineService,
                protected _runtimeService: RuntimeService,
                protected _gadgetInstanceService: WidgetInstanceService,
                protected _propertyService: WidgetPropertyService,
                protected _changeDetectionRef: ChangeDetectorRef,
                protected _optionsService: OptionsService) {
        super(_runtimeService,
            _gadgetInstanceService,
            _propertyService,
            _changeDetectionRef,
            _optionsService);


    }


    public preRun(): void {

        this.setHelpTopic();
        /**
         * todo - get collectors from property page data
         * @type {[string,string]}
         */
        this.collectors = ['read', 'write'];

        for (let y = 0; y < this.collectors.length; y++) {

            this.multi[y] = {
                'name': this.collectors[y],
                'series': TrendLineService.seedData()
            };
        }
    }

    public run() {
        this.initializeRunState(true);
        this.updateData();
    }

    public stop() {
        this.setStopState(false);
        this._trendLineService.stop(this.eventTimerSubscription);
    }

    public updateData() {
        this.eventTimerSubscription = this._trendLineService.get(this.collectors).subscribe(data => {
                for (let x = 0; x < this.collectors.length; x++) {
                    this.multi[x].series.shift();
                    this.multi[x].series.push(data[x]);
                }
                this.multi = [...this.multi];
            },
            error => this.handleError(error));
    }


    public updateProperties(updatedProperties: any) {
        super.updateProperties(updatedProperties);

        this.title = updatedProperties.title;
        this.showXAxis = updatedProperties.chart_properties;
        this.showYAxis = updatedProperties.chart_properties;
        this.gradient = updatedProperties.chart_properties;
        this.showLegend = updatedProperties.chart_properties;
        this.showXAxisLabel = updatedProperties.chart_properties;
        this.showYAxisLabel = updatedProperties.chart_properties;
        this.showOperationControls = true;
    }

    private setHelpTopic() {
        this._trendLineService.getHelpTopic().subscribe(data => {
            this.topic = data;
        });
    }
}
