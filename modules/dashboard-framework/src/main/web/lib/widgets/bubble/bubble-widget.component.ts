import { Component } from '@angular/core';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { WidgetBase } from '../_common/widget-base';
import { Router } from '@angular/router';
import { OptionsService } from '../../configuration/tab-options/service';
import { DataService } from '../../services/data.service';
import { InputFormat } from '../../model/dashboard.model';

@Component({
    selector: 'dynamic-component',
    templateUrl: './bubble-widget.html',
    styleUrls: ['../_common/widget-header.css']
})

export class BubbleWidgetComponent extends WidgetBase<InputFormat.BUBBLE> {

    // chart options
    showXAxis = true;
    showYAxis = true;
    gradient = true;
    showLegend = true;
    showXAxisLabel = true;
    showYAxisLabel = true;
    yAxisLabel = 'Available';
    xAxisLabel = 'Total';
    view: any[];
    data: any[] = [];
    colorScheme: any = {
        domain: ['#00ff00', '#4800ff', '#4894FF', '#AF0854']
    };


    constructor(protected _widgetInstanceService: WidgetInstanceService,
                private _route: Router,
                protected _dataService: DataService) {
        super(_widgetInstanceService, _dataService);

    }

    public preRun(): void {

        // this.run();
    }

    public run() {
        this.data = [];
        this.initializeRunState(true);
        this.updateData();
    }

    public stop() {
        this.setStopState(false);
    }

    public updateData() {

        this.getData().subscribe(data => {

                Object.assign(this, {data});

                console.log(data);

            },
            error => console.log(error));
    }

    public drillDown(data) {
        this._route.navigate(['detail'], {});
    }

    public updateProperties(updatedProperties: any) {

        /**
         * todo
         *  A similar operation exists on the procmman-config-service
         *  whenever the property page form is saved, the in memory board model
         *  is updated as well as the gadget instance properties
         *  which is what the code below does. This can be eliminated with code added to the
         *  config service or the property page service.
         *
         * **/
        super.updateProperties(updatedProperties);
        /*
        this.title = updatedPropsObject.title;
        this.showXAxis = updatedPropsObject.chart_properties;
        this.showYAxis = updatedPropsObject.chart_properties;
        this.gradient = updatedPropsObject.chart_properties;
        this.showLegend = updatedPropsObject.chart_properties;
        this.showXAxisLabel = updatedPropsObject.chart_properties;
        this.showYAxisLabel = updatedPropsObject.chart_properties;
        */

        this.showOperationControls = true;

    }
}
