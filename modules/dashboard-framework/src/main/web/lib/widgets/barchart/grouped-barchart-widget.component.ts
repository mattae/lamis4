import { Component } from '@angular/core';
import { InputFormat } from '../../model/dashboard.model';
import { BarChartWidgetComponent } from './bar-chart-widget.component';

@Component({
    selector: 'group-barchart-widget',
    templateUrl: './grouped-barchart-widget.component.html'
})
export class GroupedBarchartWidgetComponent extends BarChartWidgetComponent<InputFormat.MULTI_SERIES> {

}