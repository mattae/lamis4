import { Component } from '@angular/core';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { WidgetBase } from '../_common/widget-base';
import { DataService } from '../../services/data.service';

@Component({
    selector: 'dynamic-component',
    templateUrl: './barchart.html',
    styleUrls: ['../_common/widget-header.css']
})

export class BarChartWidgetComponent extends WidgetBase {

    // chart options
    results: object[];
    scheme: object;
    schemeType: string;
    customColors: object;
    animations: boolean = true;
    legend: boolean = false;
    legendTitle: string = 'Legend';
    legendPosition: string = 'right';
    xAxis: boolean = false;
    yAxis: boolean = false;
    showGridLines: boolean = true;
    roundDomains: boolean = false;
    showXAxisLabel: boolean = false;
    showYAxisLabel: boolean = false;
    xAxisLabel: string;
    yAxisLabel: string;
    trimXAxisTicks: boolean = true;
    trimYAxisTicks: boolean = true;
    maxXAxisTickLength: number = 16;
    maxYAxisTickLength: number = 16;
    xAxisTickFormatting: any;
    yAxisTickFormatting: any;
    xAxisTicks: any[];
    yAxisTicks: any[];
    showDataLabel: boolean = false;
    gradient: boolean = false;
    activeEntries: object[] = [];
    barPadding: number = 8;
    tooltipDisabled: boolean = false;
    xScaleMax: number;
    xScaleMin: number;
    showXAxis: boolean;
    showYAxis: boolean;
    showLegend: boolean;
    chartType: string;
    view: any[];
    colorScheme: any = {
        domain: ['#0AFF16', '#B2303B'] //todo - control color from property page
    };
    //////////////////
    state: string;

    RUN_STATE = 'run';
    STOP_STATE = 'stop';


    constructor(protected _widgetInstanceService: WidgetInstanceService,
                protected _dataService: DataService
    ) {
        super(_widgetInstanceService, _dataService);
    }

    public preRun() {

        /**
         * the base class initializes the common property widgets. PreRun gives
         * us a chance to initialize any of the gadgets unique properties.
         */
        this.initializeTheRemainderOfTheProperties();

        if (this.getPropFromPropertyPages('state') == this.RUN_STATE) {
            this.run();
        }
    }

    initializeTheRemainderOfTheProperties() {
        this.gradient = this.getPropFromPropertyPages('gradient');
        this.showXAxis = this.getPropFromPropertyPages('showXAxis');
        this.showYAxis = this.getPropFromPropertyPages('showYAxis');
        this.showLegend = this.getPropFromPropertyPages('showLegend');
        this.showXAxisLabel = this.getPropFromPropertyPages('showXAxisLabel');
        this.showYAxisLabel = this.getPropFromPropertyPages('showYAxisLabel');
        this.barChartType = this.getPropFromPropertyPages('barChartType');
        this.showDataLabel = this.getPropFromPropertyPages('showDataLabel');
        this.yAxisLabel = this.getPropFromPropertyPages('yAxisLabel');
        this.xAxisLabel = this.getPropFromPropertyPages('xAxisLabel');
    }


    public run() {
        this.clearChartData();
        this.initializeRunState(true);
        this.updateData();
    }

    clearChartData() {
        this.data = [];
    }

    public stop() {
        this.stopWithoutStateSave();
    }

    /**
     * When the gadget is destroyed (see ngOnDestroy) there is no need to
     * save the state. We just want to stop any API calls.
     */
    public stopWithoutStateSave() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        const data = [];
        Object.assign(this.data, {data});
        this.setStopState(false);
    }

    public ngOnDestroy() {
        this.stopWithoutStateSave();
    }

    toggleChartProperties() {
        if (this.globalOptions.displayWidgetOptionsInSideBar == false) {
            this.toggleConfigMode();
            return;
        }
    }
}
