import { Component } from '@angular/core';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { WidgetBase } from '../_common/widget-base';
import { DataService } from '../../services/data.service';

@Component({
    selector: 'dynamic-component',
    templateUrl: './pie-chart.html',
    styleUrls: ['../_common/widget-header.css']
})

export class PieChartWidgetComponent extends WidgetBase {
    // chart options
    labels: boolean;
    legend: boolean;
    legendPosition: string;
    explodeSlices: boolean;
    doughnut: boolean;
    arcWidth: number;
    gradient: boolean;
    tooltipDisabled: boolean;
    labelFormatting: any;
    trimLabels: boolean;
    maxLabelLength: number;
    tooltipText: any;
    translation: string;
    outerRadius: number;
    innerRadius: number;
    data: any;
    domain: any;
    dims: any;
    margin: number[];
    legendOptions: any;
    showDonut: boolean;
    showLegend: boolean;
    showLabels: boolean;
    legendTitle = 'Title';

    view: any[];
    colorScheme: any = {
        domain: ['#0d5481', '#0AFF16', '#4894FF', '#F54B7D'] //todo - control color from property page
    };
    //////////////////
    subscription: any;
    state: string;

    POLL_INTERVAL = 15000;


    constructor(protected _gadgetInstanceService: WidgetInstanceService,
                protected _dataService: DataService
    ) {
        super(_gadgetInstanceService, _dataService);

    }

    public preRun() {
        /**
         * the base class initializes the common property gadgets. Prerun gives
         * us a chance to initialize any of the gadgets unique properties.
         */
        this.initializeTheRemainderOfTheProperties();
        this.run();
    }

    initializeTheRemainderOfTheProperties() {
        this.explodeSlices = this.getPropFromPropertyPages('explodeSlices');
        this.showDonut = this.getPropFromPropertyPages('showDonut');
        this.gradient = this.getPropFromPropertyPages('gradient');
        this.showLegend = this.getPropFromPropertyPages('showLegend');
        this.showLabels = this.getPropFromPropertyPages('showLabels');

    }


    public run() {
        this.clearChartData();
        this.initializeRunState(true);
        this.updateData();
    }

    clearChartData() {
        this.data = [];
    }


    public stop() {
        this.stopWithoutStateSave();
    }

    /**
     * When the gadget is destroyed (see ngOnDestroy) there is no need to
     * save the state. We just want to stop any API calls.
     */
    public stopWithoutStateSave() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        const data = [];
        Object.assign(this, {data});
        this.setStopState(false);

    }

    public updateProperties(updatedProperties: any) {
        /**
         * update the tools internal state
         */
        this.setInternalProperties(updatedProperties);
    }

    public ngOnDestroy() {
        this.stopWithoutStateSave();
    }

    toggleChartProperties() {
        if (this.globalOptions.displayWidgetOptionsInSideBar == false) {
            this.toggleConfigMode();
            return;
        }

    }

    private setInternalProperties(updatedPropsObject: any) {
        this.state = updatedPropsObject.state;

        if (updatedPropsObject.title != undefined) {
            this.title = updatedPropsObject.title;
            this.explodeSlices = updatedPropsObject.explodeSlices;
            this.showDonut = updatedPropsObject.showDonut;
            this.gradient = updatedPropsObject.gradient;
            this.showLegend = updatedPropsObject.showLegend;
            this.showLabels = updatedPropsObject.showLabels;
            this.showOperationControls = true;
        }
    }
}
