import { ChangeDetectorRef, Component } from '@angular/core';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { RuntimeService } from '../../services/runtime.service';
import { WidgetPropertyService } from '../_common/widget-property.service';
import { WidgetBase } from '../_common/widget-base';
import { TrendService } from './service';
import { OptionsService } from '../../configuration/tab-options/service';

declare var d3: any;

@Component({
    selector: 'dynamic-component',
    templateUrl: './trend.html',
    styleUrls: ['../_common/widget-header.css']
})

export class TrendGadgetComponent extends WidgetBase {

    // chart options
    showXAxis = true;
    showYAxis = true;
    gradient = true;
    showLegend = true;
    showXAxisLabel = true;
    showYAxisLabel = true;
    yAxisLabel = 'Trend Data';
    xAxisLabel = 'Trend Time Line';
    view: any[];
    data: any[] = [];
    colorScheme: any = {
        domain: ['#7B7E81', '#0AFF16', '#FAFF16']
    };

    d3 = d3;

    constructor(protected _trendService: TrendService,
                protected _runtimeService: RuntimeService,
                protected _gadgetInstanceService: WidgetInstanceService,
                protected _propertyService: WidgetPropertyService,
                protected _changeDetectionRef: ChangeDetectorRef,
                protected _optionsService: OptionsService) {
        super(_runtimeService,
            _gadgetInstanceService,
            _propertyService,
            _changeDetectionRef,
            _optionsService);

    }

    public preRun(): void {

        this.run();
    }

    public run() {
        this.data = [];
        this.initializeRunState(true);
        this.updateData(null);
    }

    public stop() {
        this.setStopState(false);
    }

    public updateData(data: any[]) {

        this._trendService.get().subscribe(res => {
                this.data = res['data'];
            },
            error => this.handleError(error));
    }

    public updateProperties(updatedProperties: any) {

        /**
         * todo
         *  A similar operation exists on the procmman-config-service
         *  whenever the property page form is saved, the in memory board model
         *  is updated as well as the gadget instance properties
         *  which is what the code below does. This can be eliminated with code added to the
         *  config service or the property page service.
         *
         * **/

        super.updateProperties(updatedProperties);

        this.title = updatedProperties.title;
        this.showXAxis = updatedProperties.chart_properties;
        this.showYAxis = updatedProperties.chart_properties;
        this.gradient = updatedProperties.chart_properties;
        this.showLegend = updatedProperties.chart_properties;
        this.showXAxisLabel = updatedProperties.chart_properties;
        this.showYAxisLabel = updatedProperties.chart_properties;

        this.showOperationControls = true;


    }

}
