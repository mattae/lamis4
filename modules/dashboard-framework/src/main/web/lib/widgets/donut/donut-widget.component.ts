import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';

import { animate, state, style, transition, trigger } from '@angular/animations';

import { RuntimeService } from '../../services/runtime.service';
import { WidgetInstanceService } from '../../services/widget-instance.service';
import { WidgetPropertyService } from '../_common/widget-property.service';
import { WidgetBase } from '../_common/widget-base';
import { DonutService } from './service';
import { OptionsService } from '../../configuration/tab-options/service';


@Component({
    selector: 'dynamic-component',
    moduleId: module.id,
    templateUrl: './view.html',
    styleUrls: ['../_common/widget-header.css'],
    animations: [

        trigger('accordion', [
            state('in', style({
                height: '*'
            })),
            state('out', style({
                opacity: '0',
                height: '0px'
            })),
            transition('in => out', animate('700ms ease-in-out')),
            transition('out => in', animate('300ms ease-in-out'))
        ]),
        trigger('accordion2', [
            state('in', style({
                height: '*'
            })),
            state('out', style({
                opacity: '0',
                height: '0px'
            })),
            transition('in => out', animate('300ms ease-in-out')),
            transition('out => in', animate('800ms ease-in-out'))
        ])
    ]
})
export class DonutWidgetComponent extends WidgetBase implements OnDestroy {

    topic: any;
    data = {};
    colorScheme = {
        domain: ['#0cd057', '#3f8eff', '#9a0101']
    };
    detailMenuOpen: string;
    donutServiceSubscription: any;

    // gadget properties
    autoCompliance: boolean;
    complianceFrequency: number;

    constructor(protected _runtimeService: RuntimeService,
                protected _gadgetInstanceService: WidgetInstanceService,
                protected _propertyService: WidgetPropertyService,
                protected _changeDetectionRef: ChangeDetectorRef,
                protected _donutService: DonutService,
                protected _optionsService: OptionsService) {
        super(_runtimeService,
            _gadgetInstanceService,
            _propertyService,
            _changeDetectionRef,
            _optionsService);
    }

    public preRun(): void {
        this.setTopic();
        this.setProperties();
        this.run();
    }

    public run() {
        this.initializeRunState(true);
        this.updateData(null);
    }

    public stop() {
        if (this.donutServiceSubscription) {
            this.donutServiceSubscription.unsubscribe();
        }
        this.setStopState(false);
    }


    public updateData(data: any[]) {
        this.donutServiceSubscription = this._donutService.poll().subscribe(donutData => {
            const me = this;
            this._donutService.get().subscribe(_data => {
                    me.data = _data;
                },
                error => this.handleError(error));
        });
    }


    public updateProperties(updatedPropsObject: any) {
        super.updateProperties(updatedPropsObject);
        this.title = updatedPropsObject.title;
        this.autoCompliance = updatedPropsObject.auto;
        this.complianceFrequency = updatedPropsObject.frequency;
        this.showOperationControls = true;
    }

    setTopic() {
        this._donutService.getHelpTopic().subscribe(data => {
            this.topic = data;
        });
    }

    public setProperties() {
        this.title = this.getPropFromPropertyPages('title');
        this.detailMenuOpen = 'out';
    }


    toggleAccordion(): void {
        this.detailMenuOpen = this.detailMenuOpen === 'out' ? 'in' : 'out';
    }

    public ngOnDestroy() {
        this.stop();
    }
}
