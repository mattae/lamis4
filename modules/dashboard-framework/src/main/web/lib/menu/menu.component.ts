import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationService } from '../services/configuration.service';
import { MenuEventService } from './menu-service';
import { BoardLayoutManagerComponent } from '../layout/layout-component';
import { FilterComponent } from './filter.component';
import { AddWidgetComponent } from '../add-widget/add-widget.component';
import { TdDialogService } from '@covalent/core';
import { ConfigurationComponent } from '../configuration/configuration-component';

@Component({
    selector: 'menu-component',
    templateUrl: './menu-component.html',
    styleUrls: ['./styles.css'],

})
export class MenuComponent implements OnInit {

    host = window.location.host;
    dashboardList: any[] = [];
    selectedBoard = '';

    @ViewChild(BoardLayoutManagerComponent) layoutSideBar: BoardLayoutManagerComponent;
    @ViewChild(FilterComponent) filterComponent: FilterComponent;

    layoutId = '0';

    constructor(private _configurationService: ConfigurationService,
                private _dialogService: TdDialogService,
                private _menuEventService: MenuEventService) {

        this._menuEventService.unSubscribeAll();
        this.setupEventListeners();
    }

    setupEventListeners() {
        const gridEventSubscription = this._menuEventService.listenForGridEvents().subscribe((event: IEvent) => {
            const edata = event['data'];
            if (event['name'] === 'boardUpdateEvent') {
                this.updateDashboardMenu(edata);
            }
        });

        this._menuEventService.addSubscriber(gridEventSubscription);

    }

    ngOnInit() {
        this.updateDashboardMenu('');
    }

    emitBoardChangeLayoutEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'boardChangeLayoutEvent', data: event});
    }

    emitFilterChangeEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'filterEvent', data: event});
    }

    emitBoardSelectEvent(event) {
        this.boardSelect(event);
        this._menuEventService.raiseMenuEvent({name: 'boardSelectEvent', data: event});
    }

    emitBoardCreateEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'boardCreateEvent', data: event});
        this.updateDashboardMenu(event);
    }

    emitBoardEditEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'boardEditEvent', data: event});
    }

    emitBoardDeleteEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'boardDeleteEvent', data: event});
        this.updateDashboardMenu('');
    }

    emitBoardAddWidgetEvent(event) {
        this._menuEventService.raiseMenuEvent({name: 'boardAddGadgetEvent', data: event});
    }

    updateDashboardMenu(selectedBoard: string) {
        this._configurationService.getBoards().subscribe(data => {
            const me = this;
            if (data && data instanceof Array && data.length) {
                this.dashboardList.length = 0;
                // sort boards
                data.sort((a: any, b: any) => a.instanceId - b.instanceId);

                data.forEach(board => {
                    me.dashboardList.push(board.title);
                });

                if (selectedBoard === '') {
                    this.boardSelect(this.dashboardList[0]);
                } else {
                    this.boardSelect(selectedBoard);
                }
            }
        });
    }

    boardSelect(selectedBoard: string) {
        this.selectedBoard = selectedBoard;
    }

    toggleFilterComponent(event) {
        this.filterComponent.toggleFilter()
    }

    toggleLayoutSideBar() {
        this.layoutSideBar.showLayouts();
        this.layoutId = this._configurationService.currentModel.id;
    }

    showWidgetModal() {
        const dialog = this._dialogService.open(AddWidgetComponent);
        dialog.componentInstance.addWidgetEvent.subscribe((event) => {
            this.emitBoardAddWidgetEvent(event);
        })
    }

    showConfigurationModal() {
        const dialog = this._dialogService.open(ConfigurationComponent, {
            data: {
                dashboardList: this.dashboardList
            }
        });
        const instance = dialog.componentInstance;
        instance.dashboardCreateEvent.subscribe((event) => {
            this.emitBoardCreateEvent(event);
        });
        instance.dashboardDeleteEvent.subscribe((event) => {
            this.emitBoardDeleteEvent(event);
        });
        instance.dashboardEditEvent.subscribe((event) => {
            this.emitBoardEditEvent(event);
        });
    }
}
