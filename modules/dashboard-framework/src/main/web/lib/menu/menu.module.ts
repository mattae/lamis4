import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
	MatButtonModule,
	MatCardModule,
	MatChipsModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatSelectModule,
	MatTabsModule,
	MatToolbarModule,
	MatTooltipModule
} from '@angular/material';
import { DndModule } from 'ng2-dnd';
import { ConfigurationModule } from '../configuration/configuration.module';
import { AddWidgetModule } from '../add-widget/add-widget.module';
import { ConfigurationService } from '../services/configuration.service';
import { RuntimeService } from '../services/runtime.service';
import { TypeAheadInputModule } from '../typeahead-input/typeahead-input.module';
import { MenuComponent } from './menu.component';
import { MenuEventService } from './menu-service';
import { WidgetModule } from '../widgets/widget.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FilterComponent } from './filter.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { LamisCoreModule } from '@lamis/web-core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AddWidgetModule,
        LamisCoreModule,
        ConfigurationModule,
        TypeAheadInputModule,
        MatIconModule,
        WidgetModule,
        DndModule.forRoot(),
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatInputModule,
        MatListModule,
        MatChipsModule,
        MatCardModule,
        PerfectScrollbarModule,
        MatTooltipModule,
        MatTabsModule,
        MatSelectModule,
        MatSelectModule
    ],
	providers: [
		RuntimeService,
		ConfigurationService,
		MenuEventService
	],
	declarations: [
		MenuComponent,
		FilterComponent
	],
	exports: [
		MenuComponent,
		FilterComponent
	]
})
export class MenuModule {
}
