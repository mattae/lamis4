import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FilterService } from '../services/filter.service';
import * as moment_ from 'moment';
import { Facility, FacilityService, LGA, LgaService, State, StateService } from '@lamis/web-core';

const moment = moment_;

@Component({
    selector: 'dashboard-filter',
    templateUrl: './filter.component.html'
})
export class FilterComponent implements OnInit {
    @Output() filterChange: EventEmitter<any> = new EventEmitter();
    filter: { 'from'?: moment_.Moment, 'to'?: moment_.Moment, 'level'?: string, 'value'?: number } = {};
    showFilter = false;
    selected: { start: moment_.Moment, end: moment_.Moment };
    ranges: any = {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 14 Days': [moment().subtract(13, 'days'), moment()],
        'Last 28 Days': [moment().subtract(27, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    };
    states: State[];
    lastState: State;
    lgas: LGA[];
    lastLga: LGA;
    facilities: Facility[];

    constructor(private filterService: FilterService,
                private stateService: StateService,
                private lgaService: LgaService,
                private facilityService: FacilityService) {
    }

    public toggleFilter() {
        this.showFilter = !this.showFilter;
    }

    dateChange() {
        this.filter.from = this.selected.start;
        this.filter.to = this.selected.end;
        this.filterService.$filter.next(this.filter);
    }

    stateChange(event) {
        if (!!event) {
            this.filter.level = 'state';
            this.filter.value = event.id;
            this.lastState = event;
            this.filterService.$filter.next(this.filter);
            this.lgaService.findByState(event.id).subscribe((res) => this.lgas = res.body);
        }
        else {
            this.filter.level = 'global';
            this.filterService.$filter.next(this.filter);
        }
    }

    lgaChange(event) {
        if (!!event) {
            this.filter.level = 'institution';
            this.filter.value = event.id;
            this.lastLga = event;
            this.filterService.$filter.next(this.filter);
            this.facilityService.findByLga(event.id).subscribe((res) => this.facilities = res.body);
        }
        else {
            this.filter.level = 'state';
            this.filter.value = this.lastState.id;
            this.filterService.$filter.next(this.filter);
        }
    }

    facilityChange(event) {
        if (!!event) {
            this.filter.level = 'facilities';
            this.filter.value = event.id;
            this.filterService.$filter.next(this.filter);
        }
        else {
            this.filter.level = 'institution';
            this.filter.value = this.lastLga.id;
            this.filterService.$filter.next(this.filter);
        }
    }

    ngOnInit(): void {
        this.selected = {start: moment().subtract(27, 'days'), end: moment()};
        this.filter.from = this.selected.start;
        this.filter.to = this.selected.end;
        this.filter.level = 'global';
        this.filterService.$filter.next(this.filter);
        this.stateService.getStates().subscribe((res) => this.states = res.body);
    }
}
