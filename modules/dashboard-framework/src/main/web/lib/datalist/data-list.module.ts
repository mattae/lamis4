import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FacetModule } from '../facet/facet.module';
import { HttpClientModule } from '@angular/common/http';
import { TypeAheadInputModule } from '../typeahead-input/typeahead-input.module';
import { DataListComponent } from './data-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
    imports: [
        CommonModule,
        FacetModule,
        TypeAheadInputModule,
        PerfectScrollbarModule,
        FlexLayoutModule
    ],
    declarations: [
        DataListComponent
    ],
    providers: [],
    exports: [
        DataListComponent
    ]
})
export class DataListModule {
}
