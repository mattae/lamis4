import { AfterViewInit, Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { tabsModel } from './tabs.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

/**
 *
 */
@Component({
    selector: 'configuration-modal',
    templateUrl: './configuration-modal.html',
    styleUrls: ['./styles.css'],
    animations: [

        trigger('contentSwitch', [
            state('inactive', style({
                opacity: 0
            })),
            state('active', style({
                opacity: 1
            })),
            transition('inactive => active', animate('750ms ease-in')),
            transition('active => inactive', animate('750ms ease-out'))
        ])
    ]


})
export class ConfigurationComponent implements AfterViewInit {

    @Output() dashboardCreateEvent: EventEmitter<any> = new EventEmitter();
    @Output() dashboardEditEvent: EventEmitter<any> = new EventEmitter();
    @Output() dashboardDeleteEvent: EventEmitter<any> = new EventEmitter();
    dashboardList: any [];

    newDashboardItem = '';
    currentTab: string;
    tabsModel: any[];

    constructor(private dialog: MatDialog,@Inject(MAT_DIALOG_DATA) private data: any ) {
        Object.assign(this, {tabsModel});
        this.setCurrentTab(0);
        this.dashboardList = data.dashboardList;
    }

    createBoard(name: string) {
        if (name !== '') {
            this.dashboardCreateEvent.emit(name);
            this.newDashboardItem = '';
        }
        console.log('Creating new board event from configuration component: ' + name);
    }

    editBoard(name: string) {
        this.dashboardEditEvent.emit(name);
    }

    deleteBoard(name: string) {
        this.dashboardDeleteEvent.emit(name);
    }

    ngAfterViewInit() {
    }

    setCurrentTab(tab_index) {
        this.currentTab = this.tabsModel[tab_index].displayName;
    }
}
