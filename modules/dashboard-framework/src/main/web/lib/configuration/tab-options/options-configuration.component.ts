import { Component } from '@angular/core';
import { OptionsService } from './service';

@Component({
    selector: 'options-config',
    templateUrl: './options-config.html',
    styleUrls: ['./styles.css']
})
export class OptionsConfigurationComponent {
    enableHover: boolean;
    displayWidgetOptionsInSideBar: boolean;

    constructor(private _optionsService: OptionsService) {
        this.enableHover = this._optionsService.getBoardOptions()['enableHover'];
        this.displayWidgetOptionsInSideBar = this._optionsService.getBoardOptions()['displayGadgetOptionsInSideBar'];
    }

    onHooverOptionChange(value) {
        this._optionsService.setBoardOptions(
            {
                'enableHover': value['checked'],
                'displayGadgetOptionsInSideBar': this.displayWidgetOptionsInSideBar
            });
        this._toastService.sendMessage('The board configuration has changed!', null);
    }

    onDisplayWidgetOptionsInSideBarChange(value) {
        this._optionsService.setBoardOptions({
            'enableHover': this.enableHover,
            'displayGadgetOptionsInSideBar': value['checked']
        });
        this._toastService.sendMessage('The board configuration has changed!', null);
    }
}
