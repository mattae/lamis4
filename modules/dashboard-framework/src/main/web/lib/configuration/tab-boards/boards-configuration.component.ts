import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 */
@Component({
    selector: 'boards-config',
    templateUrl: './boards-configuration.component.html',
    styleUrls: ['./styles.css']

})
export class BoardsConfigurationComponent {

    @Output() dashboardCreateEvent: EventEmitter<any> = new EventEmitter();
    @Output() dashboardEditEvent: EventEmitter<any> = new EventEmitter();
    @Output() dashboardDeleteEvent: EventEmitter<any> = new EventEmitter();
    @Input() dashboardList: any [];


    newDashboardItem = '';

    constructor() {
    }


    createBoard(name: string) {
        if (name !== '') {
            this.dashboardCreateEvent.emit(name);
            this.newDashboardItem = '';
        }
    }

    editBoard(name: string) {
        this.dashboardEditEvent.emit(name);
    }

    deleteBoard(name: string) {
        this.dashboardDeleteEvent.emit(name);
    }
}
