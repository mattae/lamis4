import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardsConfigurationComponent } from './tab-boards/boards-configuration.component';
import { DndModule } from 'ng2-dnd';
import {
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDialogModule,
	MatIconModule,
	MatInputModule,
	MatOptionModule,
	MatSelectModule,
	MatSlideToggleModule,
	MatTabsModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptionsConfigurationComponent } from './tab-options/options-configuration.component';
import { ConfigurationComponent } from './configuration-component';
import { RouterModule } from "@angular/router";

@NgModule({
    imports: [
        CommonModule,
        DndModule.forRoot(),
        MatButtonModule,
        MatIconModule,
        MatCheckboxModule,
        MatInputModule,
        MatSelectModule,
        MatOptionModule,
        FormsModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        MatChipsModule,
        MatCardModule,
        MatDialogModule,
        MatTabsModule,
        RouterModule
    ],
	declarations: [
		BoardsConfigurationComponent,
		OptionsConfigurationComponent,
		ConfigurationComponent
	],
	providers: [
	],
	exports: [
		BoardsConfigurationComponent,
		OptionsConfigurationComponent,
		ConfigurationComponent
	]
})
export class ConfigurationModule {
}
