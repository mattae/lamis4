import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'typeahead-input',
    templateUrl: './typehead-input.html',
    styleUrls: ['./styles.css'],
})
export class TypeAheadInputComponent {

    @Input() searchList: string[];
    @Input() placeHolderText;
    @Input() typeAheadIsInMenu: boolean;
    @Output() selectionEvent = new EventEmitter<string>();
    @Output() ArtificialIntelligenceEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    requestCounter = 0;
    maxAttempts = 5;

    public query = '';
    public filteredList = [];
    public elementRef;

    constructor(myElement: ElementRef) {
        this.elementRef = myElement;
    }

    filter() {
        let _this = this;
        if (this.query !== '') {
            this.filteredList = this.searchList.filter(function (el) {
                return el.toLowerCase().indexOf(_this.query.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredList = [];
        }
        this.selectionEvent.emit(this.query);

        this.requestCounter++;
        if (this.requestCounter === this.maxAttempts) {
            this.requestCounter = 0;
        }
    }

    select(item) {
        this.query = item;
        this.filteredList = [];
        this.selectionEvent.emit(item);
    }
}
