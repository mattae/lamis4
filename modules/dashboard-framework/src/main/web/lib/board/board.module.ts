import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '../grid/grid.module';
import { BoardComponent } from './board.component';
import { TrendGadgetComponent } from '../widgets/trend/trend-gadget.component';
import { StatisticGadgetComponent } from '../widgets/statistic/statistic-gadget.component';
import { DonutWidgetComponent } from '../widgets/donut/donut-widget.component';
import { BubbleWidgetComponent } from '../widgets/bubble/bubble-widget.component';
import { BarChartWidgetComponent } from '../widgets/barchart/bar-chart-widget.component';
import { PieChartWidgetComponent } from '../widgets/piechart/pie-chart-widget.component';
import { TrendLineWidgetComponent } from '../widgets/trend-line/trend-line-widget.component';

@NgModule({
    imports: [
        CommonModule,
        GridModule.withComponents([
            StatisticGadgetComponent,
            TrendGadgetComponent,
            TrendLineWidgetComponent,
            DonutWidgetComponent,
            BubbleWidgetComponent,
            BarChartWidgetComponent,
            PieChartWidgetComponent

        ]),
    ],
    providers: [],
    declarations: [
        BoardComponent
    ]
})
export class BoardModule {
}
