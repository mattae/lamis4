import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { GridComponent } from './grid.component';
import { CellComponent } from './cell.component';
import { AddWidgetService } from '../services/add-widget.service';
import { DndModule } from 'ng2-dnd';
import { MatCardModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GridsterModule } from 'angular-gridster2';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCardModule,
        FlexLayoutModule,
        DndModule.forRoot(),
        GridsterModule
    ],
	declarations: [
		GridComponent,
		CellComponent
	],
	exports: [
		GridComponent
	],
	providers: [
		AddWidgetService
	]
})
export class GridModule {

}
