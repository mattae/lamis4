import { Component, ComponentFactoryResolver, Input, OnInit, ViewContainerRef } from '@angular/core';
import { WidgetInstanceService } from '../services/widget-instance.service';
import { WidgetFactoryService } from '../services/widget-factory-service';
import { WidgetConfig } from '../model/dashboard.model';

/*
 this class handles the dynamic creation of components
 */

@Component({
    selector: 'grid-cell',
    template: ''
})
export class CellComponent implements OnInit {
    @Input() widgetType: string;
    @Input() widgetConfig: WidgetConfig;
    @Input() widgetTags: Array<any>;


    constructor(private viewContainerRef: ViewContainerRef,
                private widgetFactoryService: WidgetFactoryService,
                private cfr: ComponentFactoryResolver,
                private widgetInstanceService: WidgetInstanceService) {
    }

    ngOnInit() {
        /*
         create component instance dynamically
         */
        const component: any = this.widgetFactoryService.resolveComponentType({type: this.widgetType});
        let compFactory: any = {};
        let widgetRef: any = {};

        if (component) {
            compFactory = this.cfr.resolveComponentFactory(component);
            widgetRef = this.viewContainerRef.createComponent(compFactory);
            /*
             we need to pass the input parameters (instance id and config) back into the newly created component.
             */
            widgetRef.instance.configureWidget(this.widgetInstanceId, this.widgetConfig, this.widgetTags);

            /*
             add concrete component to service for tracking
             */
            this.widgetInstanceService.addInstance(widgetRef);
        }
    }
}

