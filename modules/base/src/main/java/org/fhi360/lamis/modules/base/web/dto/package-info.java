/**
 * Data Access Objects used by WebSocket services.
 */
package org.fhi360.lamis.modules.base.web.dto;
