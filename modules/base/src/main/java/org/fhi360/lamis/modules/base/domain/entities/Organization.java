package org.fhi360.lamis.modules.base.domain.entities;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
@Table(name = "lamis_organization")
public class Organization extends SettableIdAuditableEntity<Organization> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
