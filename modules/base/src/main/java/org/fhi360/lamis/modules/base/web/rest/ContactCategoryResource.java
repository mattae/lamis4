package org.fhi360.lamis.modules.base.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.ContactCategory;
import org.fhi360.lamis.modules.base.domain.repositories.ContactCategoryRepository;
import org.fhi360.lamis.modules.base.web.errors.BadRequestAlertException;
import org.fhi360.lamis.modules.base.web.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ContactCategory.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class ContactCategoryResource {

    private static final String ENTITY_NAME = "entityCategory";

    private final ContactCategoryRepository contactCategoryRepository;

    public ContactCategoryResource(ContactCategoryRepository contactCategoryRepository) {
        this.contactCategoryRepository = contactCategoryRepository;
    }

    /**
     * POST  /entity-categories : Create a new entityCategory.
     *
     * @param contactCategory the entityCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entityCategory, or with status 400 (Bad Request) if the entityCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/entity-categories")
    public ResponseEntity<ContactCategory> createContactCategory(@RequestBody ContactCategory contactCategory) throws URISyntaxException {
        LOG.debug("REST request to save ContactCategory : {}", contactCategory);
        if (contactCategory.getId() != null) {
            throw new BadRequestAlertException("A new entityCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactCategory result = contactCategoryRepository.save(contactCategory);
        return ResponseEntity.created(new URI("/api/entity-categories/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /contact-categories : Updates an existing contactCategory.
     *
     * @param contactCategory the contactCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contactCategory,
     * or with status 400 (Bad Request) if the contactCategory is not valid,
     * or with status 500 (Internal Server Error) if the contactCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contact-categories")
    public ResponseEntity<ContactCategory> updateContactCategory(@RequestBody ContactCategory contactCategory) throws URISyntaxException {
        LOG.debug("REST request to update ContactCategory : {}", contactCategory);
        if (contactCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactCategory result = contactCategoryRepository.save(contactCategory);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contactCategory.getId().toString()))
                .body(result);
    }

    /**
     * GET  /contact-categories : get all the contact categories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contact categories in body
     */
    @GetMapping("/contact-categories")
    public List<ContactCategory> getAllContactCategories() {
        LOG.debug("REST request to get all Contact Categories");
        return contactCategoryRepository.findAll();
    }

    /**
     * GET  /contact-categories/:id : get the "id" contactCategory.
     *
     * @param id the id of the contactCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactCategory, or with status 404 (Not Found)
     */
    @GetMapping("/contact-categories/{id:\\d+}")
    public ResponseEntity<ContactCategory> getContactCategory(@PathVariable Long id) {
        LOG.debug("REST request to get ContactCategory : {}", id);
        Optional<ContactCategory> contactCategory = contactCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(contactCategory);
    }

    /**
     * GET  /contact-categories/:name : get the "name" contactCategory.
     *
     * @param name the name of the contactCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactCategory, or with status 404 (Not Found)
     */
    @GetMapping("/contact-categories/{name:[^\\d].*}")
    public ResponseEntity<ContactCategory> getContactCategory(@PathVariable String name) {
        LOG.debug("REST request to get ContactCategory with name : {}", name);
        Optional<ContactCategory> contactCategory = contactCategoryRepository.findByName(name);
        return ResponseUtil.wrapOrNotFound(contactCategory);
    }

    /**
     * DELETE  /contact-categories/:id : delete the "id" contactCategory.
     *
     * @param id the id of the contactCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contact-categories/{id}")
    public ResponseEntity<Void> deleteContactCategory(@PathVariable Long id) {
        LOG.debug("REST request to delete ContactCategory : {}", id);
        contactCategoryRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

