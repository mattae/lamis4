package org.fhi360.lamis.modules.base.domain.enumeration;

public enum MaritalStatus {
    SINGLE, MARRIED, SEPARATED, DIVORCED, WIDOWED
}
