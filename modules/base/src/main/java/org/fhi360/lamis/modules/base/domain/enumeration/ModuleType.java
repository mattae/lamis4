package org.fhi360.lamis.modules.base.domain.enumeration;

public enum ModuleType {
    BPM, WEB
}
