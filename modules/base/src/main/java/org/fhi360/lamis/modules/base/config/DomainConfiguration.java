package org.fhi360.lamis.modules.base.config;

import org.fhi360.lamis.modules.base.domain.BaseDomain;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses = BaseDomain.class)
public class DomainConfiguration {
}
