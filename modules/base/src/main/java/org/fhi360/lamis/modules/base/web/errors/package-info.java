/**
 * Specific errors used with Zalando's "problem-spring-modules" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package org.fhi360.lamis.modules.base.web.errors;
