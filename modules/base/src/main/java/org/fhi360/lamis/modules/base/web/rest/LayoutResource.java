package org.fhi360.lamis.modules.base.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.fhi360.lamis.modules.base.domain.entities.LayoutConfig;
import org.fhi360.lamis.modules.base.domain.repositories.LayoutConfigRepository;
import org.fhi360.lamis.modules.base.domain.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class LayoutResource {
    private final LayoutConfigRepository layoutConfigRepository;
    private final UserRepository userRepository;

    public LayoutResource(LayoutConfigRepository layoutConfigRepository,
                          UserRepository userRepository) {
        this.layoutConfigRepository = layoutConfigRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/layout-config/{username}")
    public LayoutConfig getLayoutConfig(@PathVariable String username) {
        return userRepository.findOneByLogin(username).map(user -> {
            Optional<LayoutConfig> layoutConfig = layoutConfigRepository.findByUser(user);
            return layoutConfig.orElse(new LayoutConfig());
        }).orElse(new LayoutConfig());
    }

    @PutMapping("/layout-config/{username}")
    public LayoutConfig updateLayoutConfig(@RequestBody LayoutConfig layoutConfig, @PathVariable String username) {
        LOG.info("Layout Config: {}", layoutConfig);
        final LayoutConfig[] config = {layoutConfig};
        userRepository.findOneByLogin(username).ifPresent(user -> {
            if (!StringUtils.equals(username, "anonymoususer")) {
                layoutConfig.setUser(user);
                config[0] = layoutConfigRepository.save(layoutConfig);
            }
        });
        return config[0];
    }
}
