package org.fhi360.lamis.modules.base.module;

public interface ModuleLifecycle {
    default void preInstall() {}

    default void preUninstall() {}
}
