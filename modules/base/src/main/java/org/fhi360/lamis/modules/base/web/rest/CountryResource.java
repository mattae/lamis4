package org.fhi360.lamis.modules.base.web.rest;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.Country;
import org.fhi360.lamis.modules.base.domain.repositories.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Nationality.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class CountryResource {

    private static final String ENTITY_NAME = "nationality";

    private final CountryRepository countryRepository;

    public CountryResource(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    /**
     * GET  /nationalities : get all the nationalities.
     *
     * @return the ResponseEntity with maritalStatus 200 (OK) and the list of nationalities in body
     */
    @GetMapping("/countries")
    @Timed
    public List<Country> getAllCountries() {
        LOG.debug("REST request to get all Nationalities");
        return countryRepository.findAll();
    }
}
