package org.fhi360.lamis.modules.base.domain.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Embeddable
@Data
public class Address {
    @NotNull
    private String street;

    private String street2;

    @NotNull
    private String city;

    private String zip;

    @ManyToOne(fetch = FetchType.LAZY)
    private LGA lga;

    @ManyToOne(fetch = FetchType.LAZY)
    private State state;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;
}
