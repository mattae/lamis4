package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.Module;
import org.fhi360.lamis.modules.base.domain.entities.ViewTemplate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TemplateRepository extends JpaRepository<ViewTemplate, String> {
    @Query("select t from ViewTemplate t join t.module m where t.name = ?1 and m.active = true order by t.priority desc")
    List<ViewTemplate> findByName(String name, Pageable pageable);

    List<ViewTemplate> findByNameAndModule(String name, Module module);

    List<ViewTemplate> findByModule_ActiveTrue();
}
