package org.fhi360.lamis.modules.base.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.LGA;
import org.fhi360.lamis.modules.base.domain.entities.State;
import org.fhi360.lamis.modules.base.domain.repositories.LGARepository;
import org.fhi360.lamis.modules.base.domain.repositories.StateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LGA.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class LGAResource {

    private final LGARepository lGARepository;

    private final StateRepository stateRepository;

    public LGAResource(LGARepository lGARepository, StateRepository stateRepository) {
        this.lGARepository = lGARepository;
        this.stateRepository = stateRepository;
    }

    /**
     * GET  /lgas/state/stateId : get all the lGAS for state with id.
     *
     * @return the ResponseEntity with maritalStatus 200 (OK) and the list of lGAS in body
     */
    @GetMapping("/lgas/state/{stateId}")
    @Timed
    public List<LGA> getAllLGAS(@PathVariable Long stateId) {
        LOG.debug("REST request to get all LGAS for state: {}", stateId);

        List<LGA> lgas = new ArrayList<>();
        if (stateId != null) {
            Optional<State> state = stateRepository.findById(stateId);
            state.ifPresent(state1 -> lgas.addAll(lGARepository.findByStateOrderByName(state1)));

        }
        return lgas;
    }

    /**
     * GET  /lgas/:id : get the "id" lGA.
     *
     * @param id the id of the lGA to retrieve
     * @return the ResponseEntity with maritalStatus 200 (OK) and with body the lGA, or with maritalStatus 404 (Not Found)
     */
    @GetMapping("/lgas/{id}")
    @Timed
    public ResponseEntity<LGA> getLGA(@PathVariable Long id) {
        LOG.debug("REST request to get LGA : {}", id);
        Optional<LGA> lGA = lGARepository.findById(id);
        return ResponseUtil.wrapOrNotFound(lGA);
    }
}
