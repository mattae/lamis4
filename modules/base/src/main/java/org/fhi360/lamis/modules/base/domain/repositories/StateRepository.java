package org.fhi360.lamis.modules.base.domain.repositories;

import com.foreach.across.core.annotations.Exposed;
import org.fhi360.lamis.modules.base.domain.entities.State;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repositories for the State entity.
 */
@SuppressWarnings("unused")
@Exposed
public interface StateRepository extends JpaRepository<State, Long> {

}
