package org.fhi360.lamis.modules.base.domain.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ContactCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    private Boolean active = true;

    @ManyToOne(fetch = FetchType.LAZY)
    private ContactCategory parent;

    @OneToMany(mappedBy = "parent")
    private Set<ContactCategory> children = new HashSet<>();
}
