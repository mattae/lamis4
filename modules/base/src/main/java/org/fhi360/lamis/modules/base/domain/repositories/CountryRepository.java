package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}
