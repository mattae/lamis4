package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.LayoutConfig;
import org.fhi360.lamis.modules.base.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LayoutConfigRepository extends JpaRepository<LayoutConfig, String> {
    Optional<LayoutConfig> findByUser(User user);
}
