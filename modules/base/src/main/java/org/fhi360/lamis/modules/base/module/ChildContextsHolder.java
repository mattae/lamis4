package org.fhi360.lamis.modules.base.module;

import com.foreach.across.core.AcrossContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class ChildContextsHolder {
    private Map<String, AcrossContext> contexts = new HashMap<>();

    public Map<String, AcrossContext> getContexts() {
        return contexts;
    }
}
