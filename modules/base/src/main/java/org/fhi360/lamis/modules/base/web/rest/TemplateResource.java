package org.fhi360.lamis.modules.base.web.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.ViewTemplate;
import org.fhi360.lamis.modules.base.domain.repositories.ModuleRepository;
import org.fhi360.lamis.modules.base.domain.repositories.TemplateRepository;
import org.fhi360.lamis.modules.base.web.errors.BadRequestAlertException;
import org.fhi360.lamis.modules.base.web.util.HeaderUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@Slf4j
public class TemplateResource {
    private static final String ENTITY_NAME = "template";

    private final TemplateRepository templateRepository;
    private final ModuleRepository moduleRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public TemplateResource(TemplateRepository templateRepository, ModuleRepository moduleRepository) {
        this.templateRepository = templateRepository;
        this.moduleRepository = moduleRepository;
    }

    /**
     * POST  /templates : Create a new template.
     *
     * @param template the template to create
     * @return the ResponseEntity with status 201 (Created) and with body the new template, or with status 400 (Bad Request) if the template has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/templates")
    public ResponseEntity<ViewTemplate> createAppointment(@RequestBody ViewTemplate template) throws URISyntaxException {
        LOG.debug("REST request to save Template : {}", template);
        if (template.getId() != null) {
            throw new BadRequestAlertException("A new template cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ViewTemplate result = templateRepository.save(template);
        return ResponseEntity.created(new URI("/api/templates/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /templates : Updates an existing template.
     *
     * @param template the template to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated template,
     * or with status 400 (Bad Request) if the template is not valid,
     * or with status 500 (Internal Server Error) if the template couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/templates")
    public ResponseEntity<ViewTemplate> updateAppointment(@RequestBody ViewTemplate template) throws URISyntaxException {
        LOG.debug("REST request to update Template : {}", template);
        if (template.getId() == null) {
            throw new BadRequestAlertException("Invalid name", ENTITY_NAME, "id null");
        }
        ViewTemplate result = templateRepository.save(template);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, template.getId()))
                .body(result);
    }

    /**
     * GET  /templates : get all the Templates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of templates in body
     */
    @GetMapping("/templates")
    public List<ViewTemplate> getTemplates() {
        return templateRepository.findByModule_ActiveTrue()
                .stream()
                .sorted(Comparator.comparing(ViewTemplate::getName)
                        .thenComparing(ViewTemplate::getPriority).reversed())
                .collect(Collectors.toList());
    }

    /**
     * GET  /templates/:name : get the "name" Template.
     *
     * @param name the name of the template to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the template, or with status 404 (Not Found)
     */
    @GetMapping("/templates/{name}")
    public ResponseEntity<ViewTemplate> getTemplate(@PathVariable String name) {
        LOG.debug("REST request to get Template : {}", name);
        List<ViewTemplate> templates = templateRepository.findByName(name, PageRequest.of(0, 1));
        return ResponseUtil.wrapOrNotFound(templates.stream().findFirst());
    }

    public void loadTemplateJson(String moduleName, String templateName, String path, boolean update){
        moduleRepository.findByName(moduleName).ifPresent(module -> {
            List<ViewTemplate> templates = templateRepository.findByNameAndModule(templateName, module);
            if (templates.isEmpty()){
                try {
                    JsonNode template = objectMapper.readTree(new ClassPathResource(path).getInputStream());
                    ViewTemplate viewTemplate = new ViewTemplate();
                    viewTemplate.setName(templateName);
                    viewTemplate.setModule(module);
                    viewTemplate.setTemplate(template);
                    templateRepository.save(viewTemplate);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (update){
                templates.forEach(viewTemplate -> {
                    try {
                        JsonNode template = objectMapper.readTree(new ClassPathResource(path).getInputStream());
                        viewTemplate.setTemplate(template);
                        templateRepository.save(viewTemplate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
    }
}
