package org.fhi360.lamis.modules.base.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Table(name = "view_template")
@Entity
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = "name")
public class ViewTemplate extends BaseEntity implements Serializable {
    @NotNull
    private String name;

    @NotNull
    @Type(type = "json-node")
    @Column(columnDefinition = "json")
    private JsonNode template;

    @JsonIgnore
    private Integer priority = 1;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private Module module;
}
