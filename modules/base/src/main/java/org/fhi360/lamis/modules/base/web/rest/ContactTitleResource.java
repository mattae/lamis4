package org.fhi360.lamis.modules.base.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.ContactTitle;
import org.fhi360.lamis.modules.base.domain.repositories.ContactTitleRepository;
import org.fhi360.lamis.modules.base.web.errors.BadRequestAlertException;
import org.fhi360.lamis.modules.base.web.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EntityTitle.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class ContactTitleResource {

    private static final String ENTITY_NAME = "contactTitle";

    private final ContactTitleRepository contactTitleRepository;

    public ContactTitleResource(ContactTitleRepository contactTitleRepository) {
        this.contactTitleRepository = contactTitleRepository;
    }

    /**
     * POST  /contact-titles : Create a new contactTitle.
     *
     * @param contactTitle the contactTitle to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contactTitle, or with status 400 (Bad Request) if the contactTitle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contact-titles")
    public ResponseEntity<ContactTitle> createContactTitle(@RequestBody ContactTitle contactTitle) throws URISyntaxException {
        LOG.debug("REST request to save ContactTitle : {}", contactTitle);
        if (contactTitle.getId() != null) {
            throw new BadRequestAlertException("A new contactTitle cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactTitle result = contactTitleRepository.save(contactTitle);
        return ResponseEntity.created(new URI("/api/contact-titles/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /contact-titles : Updates an existing contactTitle.
     *
     * @param contactTitle the contactTitle to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contactTitle,
     * or with status 400 (Bad Request) if the contactTitle is not valid,
     * or with status 500 (Internal Server Error) if the contactTitle couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contact-titles")
    public ResponseEntity<ContactTitle> updateContactTitle(@RequestBody ContactTitle contactTitle) throws URISyntaxException {
        LOG.debug("REST request to update ContactTitle : {}", contactTitle);
        if (contactTitle.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactTitle result = contactTitleRepository.save(contactTitle);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contactTitle.getId().toString()))
                .body(result);
    }

    /**
     * GET  /contact-titles : get all the contact categories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contact categories in body
     */
    @GetMapping("/contact-titles")
    public List<ContactTitle> getAllContactCategories() {
        LOG.debug("REST request to get all Contact Categories");
        return contactTitleRepository.findAll();
    }

    /**
     * GET  /contact-titles/:id : get the "id" contactTitle.
     *
     * @param id the id of the contactTitle to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactTitle, or with status 404 (Not Found)
     */
    @GetMapping("/contact-titles/{id:\\d+}")
    public ResponseEntity<ContactTitle> getContactTitle(@PathVariable Long id) {
        LOG.debug("REST request to get ContactTitle : {}", id);
        Optional<ContactTitle> contactTitle = contactTitleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(contactTitle);
    }

    /**
     * GET  /contact-titles/:name : get the "name" contactTitle.
     *
     * @param name the name of the contactTitle to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactTitle, or with status 404 (Not Found)
     */
    @GetMapping("/contact-titles/{name:[^\\d].*}")
    public ResponseEntity<ContactTitle> getContactTitle(@PathVariable String name) {
        LOG.debug("REST request to get ContactTitle : {}", name);
        Optional<ContactTitle> contactTitle = contactTitleRepository.findByName(name);
        return ResponseUtil.wrapOrNotFound(contactTitle);
    }

    /**
     * DELETE  /contact-titles/:id : delete the "id" contactTitle.
     *
     * @param id the id of the contactTitle to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contact-titles/{id}")
    public ResponseEntity<Void> deleteContactTitle(@PathVariable Long id) {
        LOG.debug("REST request to delete ContactTitle : {}", id);
        contactTitleRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}


