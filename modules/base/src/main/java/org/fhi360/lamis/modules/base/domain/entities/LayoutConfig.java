package org.fhi360.lamis.modules.base.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
public class LayoutConfig extends BaseEntity implements Serializable, Persistable<String> {
    @NotNull
    @Type(type = "json-node")
    @Column(columnDefinition = "json")
    private JsonNode config;

    @OneToOne
    @NotNull
    private User user;

    @Override
    @JsonIgnore
    public boolean isNew() {
        return id == null;
    }
}
