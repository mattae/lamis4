package org.fhi360.lamis.modules.base.domain.enumeration;

public enum  MenuLevel {
    LEVEL_1, LEVEL_2, LEVEL_3
}
