package org.fhi360.lamis.modules.base.tenant.model.repository;

import org.fhi360.lamis.modules.base.tenant.model.entity.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantRepository extends JpaRepository<Tenant, Long> {
}
