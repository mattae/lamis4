package org.fhi360.lamis.modules.base.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.domain.entities.State;
import org.fhi360.lamis.modules.base.domain.repositories.StateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing State.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class StateResource {

    private final StateRepository stateRepository;

    public StateResource(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    /**
     * GET  /states : get all the states.
     *
     * @return the ResponseEntity with maritalStatus 200 (OK) and the list of states in body
     */
    @GetMapping("/states")
    @Timed
    public List<State> getAllStates() {
        LOG.debug("REST request to get all States");

        return stateRepository.findAll();
    }

    /**
     * GET  /states/:id : get the "id" state.
     *
     * @param id the id of the state to retrieve
     * @return the ResponseEntity with state 200 (OK) and with body the state, or with 404 (Not Found)
     */
    @GetMapping("/states/{id}")
    @Timed
    public ResponseEntity<State> getState(@PathVariable Long id) {
        LOG.debug("REST request to get State : {}", id);
        Optional<State> state = stateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(state);
    }
}
