/**
 * View Models used by Spring MVC REST controllers.
 */
package org.fhi360.lamis.modules.base.web.vm;
