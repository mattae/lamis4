package org.fhi360.lamis.modules.base.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.fhi360.lamis.modules.base.domain.entities.*;
import org.fhi360.lamis.modules.base.domain.enumeration.ModuleType;
import org.fhi360.lamis.modules.base.domain.repositories.ModuleRepository;
import org.fhi360.lamis.modules.base.util.UnsatisfiedDependencyException;
import org.fhi360.lamis.modules.base.yml.ModuleConfig;
import org.fhi360.lamis.modules.base.yml.WebModuleConfig;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ModuleConfigProcessor {

    private final ModuleRepository moduleRepository;

    public ModuleConfigProcessor(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    public void processModuleConfig(ModuleConfig moduleConfig, Module module) throws UnsatisfiedDependencyException {
        if (moduleConfig != null) {
            if (moduleConfig.getUmdLocation() != null) {
                module.setUmdLocation(String.format("across/resources/static/%s/js/%s",
                        StringUtils.isBlank(moduleConfig.getResourceKey()) ? moduleConfig.getName() :
                                moduleConfig.getResourceKey(), moduleConfig.getUmdLocation()));
                if (moduleConfig.getUmdLocation().startsWith("/")) {
                    module.setUmdLocation(moduleConfig.getUmdLocation());
                }
            }
            module.getBundledAuthorities().clear();
            Map<String, String> dependencies = moduleConfig.getDependencies();
            List<ModuleDependency> moduleDependencies = new ArrayList<>();
            dependencies.forEach((name, version) -> {
                if (!name.equals("BaseModule")) {
                    Optional<Module> m = moduleRepository.findByName(name);
                    if (!m.isPresent()) {
                        throw new UnsatisfiedDependencyException(
                                String.format("Unsatisfied module requirement: %s not installed.", name));
                    } else {
                        if (!versionSatisfied(m.get().getVersion(), version)) {
                            throw new UnsatisfiedDependencyException(
                                    String.format("Version requirements cannot be satisfied for module: %s required, %s installed. ",
                                            version, m.get().getVersion()));
                        }
                        ModuleDependency moduleDependency = new ModuleDependency();
                        moduleDependency.setDependency(module);
                        moduleDependency.setDependency(m.get());
                        moduleDependency.setVersion(version);
                        moduleDependencies.add(moduleDependency);
                    }
                }
            });
            module.getDependencies().addAll(moduleDependencies);
            Set<Authority> authorities = moduleConfig.getBundledAuthorities().stream()
                    .map(authority -> {
                        authority.setModule(module);
                        return authority;
                    })
                    .collect(Collectors.toSet());
            module.setBundledAuthorities(authorities);
            module.getMenus().clear();
            Set<Menu> menus;
            menus = moduleConfig.getMenus().stream()
                    .map(menuItem -> {
                        menuItem.setModule(module);
                        return menuItem;
                    })
                    .map(menu -> {
                        List<Menu> subs1 = menu.getSubs();
                        subs1 = subs1.stream()
                                .map(sub -> {
                                    sub.setModule(menu.getModule());
                                    sub.setParent(menu);
                                    return sub;
                                })
                                .map(sub -> {
                                    List<Menu> subs2 = sub.getSubs();
                                    subs2 = subs2.stream()
                                            .map(sub2 -> {
                                                sub2.setModule(menu.getModule());
                                                sub2.setParent(sub);
                                                return sub2;
                                            })
                                            .collect(Collectors.toList());
                                    sub.getSubs().clear();
                                    sub.getSubs().addAll(subs2);
                                    return sub;
                                })
                                .collect(Collectors.toList());
                        menu.getSubs().clear();
                        menu.getSubs().addAll(subs1);
                        return menu;
                    })
                    .collect(Collectors.toSet());
            module.setMenus(menus);

            List<WebModuleConfig> webModuleConfigs = moduleConfig.getWebModules();
            for (WebModuleConfig webModuleConfig : webModuleConfigs) {
                WebModule webModule = readWebModuleProperties(module, webModuleConfig);
                module.getWebModules().add(webModule);
            }
            moduleRepository.save(module);
        }
    }

    private WebModule readWebModuleProperties(Module module, WebModuleConfig config) {
        WebModule webModule = new WebModule();
        webModule.setModule(module);
        webModule.setName(config.getName());
        webModule.setTitle(config.getTitle());
        webModule.setBreadcrumb(config.getBreadcrumb());
        webModule.setPath(config.getPath());
        webModule.setPosition(config.getPosition());
        webModule.setType(ModuleType.WEB);
        return webModule;
    }

    private boolean versionSatisfied(String installedVersion, String requiredVersion) {
        String requiredMajor = requiredVersion.split("\\.")[0];
        String installedMajor = installedVersion.split("\\.")[0];

        return requiredMajor.equals(installedMajor);
    }
}
