package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.ContactTitle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactTitleRepository extends JpaRepository<ContactTitle, Long> {
    Optional<ContactTitle> findByName(String name);
}
