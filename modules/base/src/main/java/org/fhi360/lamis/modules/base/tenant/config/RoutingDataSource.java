package org.fhi360.lamis.modules.base.tenant.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class RoutingDataSource extends AbstractRoutingDataSource {

    public RoutingDataSource() {
        initDataSources();
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return "tenant_1";
    }

    private void initDataSources() {
        Map<Object, Object> dsMap = new HashMap<>();

        DataSource dataSource = DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url("jdbc:mysql://localhost:3306/tenant1?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false")
                .username("lamis")
                .password("password")
                .driverClassName("com.mysql.jdbc.Driver")
                .build();
        dsMap.put("tenant_1", dataSource);
        this.setTargetDataSources(dsMap);
    }

}
