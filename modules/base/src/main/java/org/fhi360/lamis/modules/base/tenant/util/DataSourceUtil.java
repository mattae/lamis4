package org.fhi360.lamis.modules.base.tenant.util;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.tenant.model.entity.Tenant;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;

@Slf4j
public final class DataSourceUtil {
    /**
     * Utility method to create and configure a data source
     *
     * @param tenant
     * @return DataSource
     */
    public static DataSource createAndConfigureDataSource(Tenant tenant) {
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url(tenant.getUrl())
                .username(tenant.getUsername())
                .password(tenant.getPassword())
                .driverClassName("com.mysql.jdbc.Driver")
                .build();
    }
}

