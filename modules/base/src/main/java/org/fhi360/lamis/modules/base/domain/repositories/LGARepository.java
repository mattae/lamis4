package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.LGA;
import org.fhi360.lamis.modules.base.domain.entities.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * Spring Data JPA repositories for the LGA entity.
 */
@SuppressWarnings("unused")
public interface LGARepository extends JpaRepository<LGA, Long> {
    List<LGA> findByStateOrderByName(State state);
}
