package org.fhi360.lamis.modules.base.domain.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fhi360.lamis.modules.base.domain.enumeration.Gender;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@javax.persistence.Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Contact extends BaseEntity {
    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Size(min = 2, max = 6)
    @Column(name = "lang_key", length = 6)
    private String langKey;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Embedded
    private Address address;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Email
    private String email;

    @URL
    private String website;

    private String phone;

    private String mobile;

    private Boolean active = true;

    private Long longitude;

    private Long latitude;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne
    @JoinColumn(name = "title_id")
    private ContactTitle title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private ContactCategory category;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contact parent;

    @OneToMany(mappedBy = "parent")
    private Set<Contact> children = new HashSet<>();
}
