package org.fhi360.lamis.modules.base.domain.repositories;

import org.fhi360.lamis.modules.base.domain.entities.ContactCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactCategoryRepository extends JpaRepository<ContactCategory, Long> {
    Optional<ContactCategory> findByName(String name);
}
