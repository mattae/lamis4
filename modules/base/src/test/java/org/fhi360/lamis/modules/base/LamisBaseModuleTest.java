package org.fhi360.lamis.modules.base;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.fhi360.lamis.modules.base.module.UMDModule;
import org.fhi360.lamis.modules.base.module.UMDModuleExtender;
import org.fhi360.lamis.modules.base.domain.entities.Authority;
import org.fhi360.lamis.modules.base.domain.entities.Menu;
import org.fhi360.lamis.modules.base.domain.enumeration.MenuType;
import org.fhi360.lamis.modules.base.yml.ModuleConfig;
import org.fhi360.lamis.modules.base.yml.WebModuleConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.FileSystem;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Unit test for simple BaseModule.
 */
@Slf4j
public class LamisBaseModuleTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    @Ignore
    public void shouldAnswerWithTrue() throws IOException {
        LOG.info("Value: {}", URLDecoder.decode("Futronic%20FS80H%20%231", "UTF-8"));
        LOG.info("Value 1: {}", System.getenv("ProgramFiles"));
        LOG.info("Value 2: {}", System.getenv("ProgramFiles(X86)"));
        for (int i = 0; i < 4; i++) {
            LOG.info("{}:\t{}", (i + 1), UUID.randomUUID().toString());
        }

        List<UMDModule> modules = new ArrayList<>();
        UMDModule module = new UMDModule("PharmacyModule", "/api/pharmacy", "");
        module.setMap("{\"moment\": \"http://unpkg.com/moment\"}");
        modules.add(module);

        module = new UMDModule("EACModule", "/api/eac", "");
        modules.add(module);
        String content = String.join("\n", Files.readAllLines(
                new File("C:\\Users\\User10\\Downloads\\angular-umd-dynamic-example-master\\src\\modules\\moduleb.umd.js").toPath()));
        UMDModule umdModule = new UMDModule("ModulebModule", "/", content);
        umdModule.setMap("{\"@clr/angular\": \"https://unpkg.com/@clr/angular\"}");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        umdModule = UMDModuleExtender.extendModule(umdModule, modules);
        stopWatch.stop();
        LOG.info("Elapse time 1: {}", stopWatch.getTime());
        stopWatch.reset();

        //LOG.info("Map 1: {}", umdModule.getMap());
        //LOG.info("Content 1: {}", umdModule.getContent());
        stopWatch.start();
        umdModule = UMDModuleExtender.extendModule(umdModule, modules);
        LOG.info("Elapse time 2: {}", stopWatch.getTime());
        stopWatch.reset();
        //LOG.info("Map 2: {}", umdModule.getMap());
        //LOG.info("Content 2: {}", umdModule.getContent());
        stopWatch.reset();
        stopWatch.start();
        umdModule = UMDModuleExtender.extendModule(umdModule, modules);
        LOG.info("Elapse time 3: {}", stopWatch.getTime());
        LOG.info("Content 2: {}", umdModule.getContent());
        stopWatch.reset();
        ModuleConfig config = new ModuleConfig();
        Authority a1 = new Authority();
        a1.setId("ROLE_TEST1");
        a1.setName("A test role 1");

        Authority a2 = new Authority();
        a2.setId("ROLE_TEST2");
        a2.setName("A test role 2");
        //config.setAuthorities(Arrays.asList(a1, a2));
        config.setBundledAuthorities(Arrays.asList(a1, a2));
        config.setBasePackage("org.fhi360.lamis.health");
        config.setName("LAMIS Health Module");
        config.setModuleMap("{'a': 'c'}");
        config.setUmdLocation("/api/resource");
        Map<String, String> deps = new HashMap<>();
        deps.put("BaseModule", "1.0.0");
        deps.put("PatientModule", "1.2.0");
        config.setDependencies(deps);

        WebModuleConfig webModuleConfig = new WebModuleConfig();
        webModuleConfig.setName("PatientModule");
        webModuleConfig.setPath("patients");
        webModuleConfig.setTitle("Patients");
        webModuleConfig.setBreadcrumb("PATIENTS");
        webModuleConfig.setAuthorities(Lists.newArrayList("ROLE_FACILITY", "ROLE_DEC"));
        config.getWebModules().add(webModuleConfig);
        webModuleConfig = new WebModuleConfig();
        webModuleConfig.setName("AppointmentModule");
        webModuleConfig.setPath("appointments");
        webModuleConfig.setTitle("Appointments");
        webModuleConfig.setBreadcrumb("APPOINTMENTS");
        webModuleConfig.setAuthorities(Lists.newArrayList("ROLE_FACILITY", "ROLE_DEC"));
        config.getWebModules().add(webModuleConfig);

        Menu menu = new Menu();
        menu.setType(MenuType.DROP_DOWN);
        menu.setState("patients");
        menu.setName("Patients");
        Menu sub = new Menu();
        sub.setName("New Patient");
        sub.setState("new");
        menu.getSubs().add(sub);

        config.getMenus().add(menu);
        Yaml yaml = new Yaml();
        String output = yaml.dump(config);
        LOG.info("Yaml output: \n{}", output);

        yaml = new Yaml(new Constructor(ModuleConfig.class));

        ModuleConfig moduleConfig = yaml.load(new FileInputStream("c:/users/user10/desktop/module.yml"));
        LOG.info("Module Config: {}", moduleConfig);

        Integer a = null;
        LOG.info("UUID: {}; {}", UUID.randomUUID(), UUID.randomUUID().version(), UUID.randomUUID().variant());
        byte[] bytes = Files.readAllBytes(Paths.get("c:/users/user10/downloads/keyfile.enc"));
        String base64 = Base64.getEncoder().encodeToString(bytes);
        LOG.info("Encoded: {}", base64);
        byte[] bytes1 = Base64.getDecoder().decode(base64);
        Files.write(Paths.get("c:/users/user10/downloads/keyfile1.enc"), bytes1);
    }

    @Test
    @Ignore
    public void split() {
        String inserts = "casemanager_transform,*,local_id,casemanager_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "casemanager_transform,*,casemanager_id,casemanager_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"casemanager\", transformedData.getSourceValues().get(\"casemanager_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,local_id,child_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,child_id,child_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"child\", transformedData.getSourceValues().get(\"child_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,anc_id,anc_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"anc\", transformedData.getSourceValues().get(\"anc_id\"),transformedData.getSourceValues().get(\"facility_id\");\",4,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,delivery_id,delivery_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"delivery\", transformedData.getSourceValues().get(\"delivery_id\"),transformedData.getSourceValues().get(\"facility_id\");\",5,current_timestamp,current_timestamp,Documentation\n" +
                "child_transform,*,mother_id,mother_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"motherinformation\", transformedData.getSourceValues().get(\"mother_id\"),transformedData.getSourceValues().get(\"facility_id\");\",6,current_timestamp,current_timestamp,Documentation\n" +
                "childfollowup_transform,*,local_id,childfollowup_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "childfollowup_transform,*,childfollowup_id,childfollowup_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"childfollowup\", transformedData.getSourceValues().get(\"childfollowup_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "childfollowup_transform,*,child_id,child_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"child\", transformedData.getSourceValues().get(\"child_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "chronic_care_transformation,*,local_id,chroniccare_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "chronic_care_transformation,*,chroniccare_id,chroniccare_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"chroniccare\", transformedData.getSourceValues().get(\"chroniccare_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "chronic_care_transformation,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "clinic_transformation,*,local_id,history_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "clinic_transformation,*,clinic_id,clinic_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"clinic\", transformedData.getSourceValues().get(\"history_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "clinic_transformation,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "clinic_transformation,,,,,,,,current_timestamp,current_timestamp,Documentation\n" +
                "delivery_transform,*,local_id,delivery_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "delivery_transform,*,clinic_id,clinic_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"delivery\", transformedData.getSourceValues().get(\"delivery_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "delivery_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "delivery_transform,*,anc_id,anc_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"anc\", transformedData.getSourceValues().get(\"anc_id\"),transformedData.getSourceValues().get(\"facility_id\");\",4,current_timestamp,current_timestamp,Documentation\n" +
                "devolve_transform,*,local_id,devolve_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "devolve_transform,*,devolve_id,devolve_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"devolve\", transformedData.getSourceValues().get(\"devolve_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "devolve_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "dm_screen_transform,*,local_id,history_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "dm_screen_transform,*,history_id,history_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"dmscreenhistory\", transformedData.getSourceValues().get(\"history_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "dm_screen_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "drug_therapy_transform,*,local_id,drugtherapy_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "drug_therapy_transform,*,drugtherpy_id,drugtherapy_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"drugtherapy\", transformedData.getSourceValues().get(\"drugtheray_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "drug_therapy_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "eac_transform,*,local_id,eac_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "eac_transform,*,eac_id,eac_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"eac\", transformedData.getSourceValues().get(\"eac_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "eac_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "eid_transform,*,local_id,eid_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "eid_transform,*,eid_id,eid_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"eid\", transformedData.getSourceValues().get(\"eid_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "eid_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "encounter_transform,*,local_id,encounter_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "encounter_transform,*,encounter_id,encounter_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"encounter\", transformedData.getSourceValues().get(\"encounter_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "encounter_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "exchange_transform,*,local_id,exchange_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "exchange_transform,*,exchange_id,exchange_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"exchange\", transformedData.getSourceValues().get(\"exchange_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "labno_transform,*,local_id,labno_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "labno_transform,*,labno_id,labno_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"labno\", transformedData.getSourceValues().get(\"labno_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "laboratory_transform,*,local_id,laboratory_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "laboratory_transform,*,laboratory_id,laboratory_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"laboratory\", transformedData.getSourceValues().get(\"laboratory_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "laboratory_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "mother_followup_transform,*,local_id,motherfollowup_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "mother_followup_transform,*,motherfollowup_id,motherfollowup_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"motherfollowup\", transformedData.getSourceValues().get(\"motherfollowup_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "mother_followup_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "mhtc_transform,*,local_id,mhtc_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "mhtc_transform,*,mhtc_id,mhtc_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"mhtc\", transformedData.getSourceValues().get(\"mhtc_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "mother_information_transform,*,local_id,motherinformation_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "mother_information_transform,*,motherinformation_id,motherinformation_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"motherinformation\", transformedData.getSourceValues().get(\"motherinformation_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "mother_information_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "nigqual_transform,*,local_id,nigqual_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "nigqual_transform,*,nigqual_id,nigqual_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"nigqual\", transformedData.getSourceValues().get(\"nigqual_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "nigqual_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "oi_history_transform,*,local_id,history_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "oi_history_transform,*,history_id,history_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"oihistory\", transformedData.getSourceValues().get(\"history_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "oi_history_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "partner_information_transform,*,local_id,partnerinformation_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "partner_information_transform,*,partnerinformation_id,partnerinformation_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"partnerinformation\", transformedData.getSourceValues().get(\"partnerinformation_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "partner_information_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "patient_case_manager_transform,*,local_id,patientcasemanager_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "patient_case_manager_transform,*,patientcasemanager_id,patientcasemanager_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"patientcasemanager\", transformedData.getSourceValues().get(\"patientcasemanager_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "patient_case_manager_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "pharmacy_transform,*,local_id,pharmacy_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "pharmacy_transform,*,pharmacy_id,pharmacy_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"pharmacy\", transformedData.getSourceValues().get(\"pharmacy_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "pharmacy_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "prescription_transform,*,local_id,prescription_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n" +
                "prescription_transform,*,prescription_id,prescription_id,1,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerId(\"prescription\", transformedData.getSourceValues().get(\"prescription_id\"),transformedData.getSourceValues().get(\"facility_id\"),transformedData.getSourceValues().get(\"id_uuid\"));\",2,current_timestamp,current_timestamp,Documentation\n" +
                "prescription_transform,*,patient_id,patient_id,0,bsh,\"org.fhi.lamis.application.ServerIDProvider.getServerFkFromLocalId(\"patient\", transformedData.getSourceValues().get(\"patient_id\"),transformedData.getSourceValues().get(\"facility_id\");\",3,current_timestamp,current_timestamp,Documentation\n" +
                "regimen_history_transform,*,local_id,history_id,0,copy,,1,current_timestamp,current_timestamp,Documentation\n";

        StringBuilder output = new StringBuilder();
        String[] lines = inserts.split("\n");
        for (String line : lines) {
            String[] tokens = line.split(",");
            output.append("(");
            for (String token : tokens) {
                if (!(StringUtils.length(token) > 0 && Character.isDigit(token.charAt(0))) || !StringUtils.isBlank(token)) {
                    output.append("'").append(token).append("'").append(",");
                } else {
                    output.append(token).append(",");
                }
            }
            output.append("),").append("\n");
        }

        String result = output.toString();
        for (Integer i = 0; i < 10; i++) {
            result = result.replaceAll("'" + i + "'", i.toString());
        }
        result = result.replaceAll("' transformedData", "transformedData");
        //result = result.replaceAll("\"',", "',");
        result = result.replaceAll("'current_timestamp'", "current_timestamp");
        result = result.replaceAll("'Documentation',", "'Documentation'");
        result = result.replaceAll("\"\\)','", "\"),");
        result = result.replaceAll("\"org.fhi", "org.fhi360");
        LOG.info("{}", result);
    }

    @Test
    @Ignore
    public void generateChannels() {
        StringBuilder builder = new StringBuilder();

        String[] channels = {"patient", "pharmacy", "clinic", "laboratory", "general"};
        for (int i = 1; i <= 5; i++) {
            //Generate institution channels
            builder.append("insert into sym_channel (channel_id, processing_order, max_batch_size, enabled, description) values\n");
            for (String channel : channels) {
                String query = String.format("('%s', 1, 100000, 1, '%s');",
                        channel + "_" + i, channel + " channel for institution " + i);
                if (!channel.equals("general")) {
                    builder.append(query).append(",\n");
                } else {
                    builder.append(query).append("\n");
                }
            }
        }

        for (int i = 1; i <= 5; i++) {
            //Generate institution triggers
            builder.append("insert into sym_trigger (trigger_id,source_table_name,channel_id,last_update_time,create_time) values\n");
            for (String channel : channels) {
                String query = String.format("\t('%s','%s','%s',current_timestamp,current_timestamp);",
                        channel + "_data_" + i, channel.equals("general") ? "*" : channel, channel + "_" + i);
                if (!channel.equals("general")) {
                    builder.append(query).append(",\n");
                } else {
                    builder.append(query).append("\n");
                }
            }
        }
        for (int i = 1; i <= 5; i++) {
            //Generate institution trigger routers
            int priority = 1;
            builder.append("insert into sym_trigger_router (trigger_id,router_id,initial_load_order,last_update_time,create_time) values\n");
            for (String channel : channels) {
                String query = String.format("\t('%s','facility_2_server', %s, current_timestamp, current_timestamp)",
                        channel + "_data_" + i, channel.equals("general") ? 100 : priority++);
                if (!channel.equals("general")) {
                    builder.append(query).append(",\n");
                } else {
                    builder.append(query).append("\n");
                }
            }
        }
        LOG.info("{}\n", builder.toString());
    }

    @Test
    @Ignore
    public void generateNode() {
        StringBuilder builder = new StringBuilder();
        DecimalFormat format = new DecimalFormat("0000");
        builder.append("insert into sym_node (node_id,node_group_id,external_id,sync_enabled,sync_url,schema_version,symmetric_version,database_type,database_version,batch_to_send_count,batch_in_error_count,created_at_node_id) values\n");
        for (int i = 1; i <= 5_000; i++) {
            String query = String.format("\t('%s','institution','%s',1,null,null,null,current_timestamp,null,0,0,'000')",
                    format.format(i), format.format(i));
            if (i != 5_000) {
                builder.append(query).append(",\n");
            } else {
                builder.append(query).append("\n");
            }
        }
        builder.append("\n").append("insert into sym_node_security (node_id,node_password,registration_enabled,registration_time,initial_load_enabled,initial_load_time,created_at_node_id) values\n");
        for (int i = 1; i <= 5_000; i++) {
            String query = String.format("\t('%s','xjRTiyjzIEBe0pHCrl0g9I9nY2qr5I',1,null,1,null,'000')",
                    format.format(i));
            if (i != 5_000) {
                builder.append(query).append(",\n");
            } else {
                builder.append(query).append("\n");
            }
        }
        LOG.info("\n{}", builder.toString());
    }

    public static String encodePath(String var0, boolean var1) {
        char[] var2 = new char[var0.length() * 2 + 16];
        int var3 = 0;
        char[] var4 = var0.toCharArray();
        int var5 = var0.length();

        for (int var6 = 0; var6 < var5; ++var6) {
            char var7 = var4[var6];
            if (!var1 && var7 == '/' || var1 && var7 == File.separatorChar) {
                var2[var3++] = '/';
            } else if (var7 > 127) {
                if (var7 > 2047) {
                    var3 = escape(var2, (char) (224 | var7 >> 12 & 15), var3);
                    var3 = escape(var2, (char) (128 | var7 >> 6 & 63), var3);
                    var3 = escape(var2, (char) (128 | var7 >> 0 & 63), var3);
                } else {
                    var3 = escape(var2, (char) (192 | var7 >> 6 & 31), var3);
                    var3 = escape(var2, (char) (128 | var7 >> 0 & 63), var3);
                }
            } else if (var7 >= 'a' && var7 <= 'z' || var7 >= 'A' && var7 <= 'Z' || var7 >= '0' && var7 <= '9') {
                var2[var3++] = var7;
            } else if (encodedInPath.get(var7)) {
                var3 = escape(var2, var7, var3);
            } else {
                var2[var3++] = var7;
            }

            if (var3 + 9 > var2.length) {
                int var8 = var2.length * 2 + 16;
                if (var8 < 0) {
                    var8 = 2147483647;
                }

                char[] var9 = new char[var8];
                System.arraycopy(var2, 0, var9, 0, var3);
                var2 = var9;
            }
        }

        return new String(var2, 0, var3);
    }

    private static BitSet encodedInPath = new BitSet(256);

    private static int escape(char[] var0, char var1, int var2) {
        var0[var2++] = '%';
        var0[var2++] = Character.forDigit(var1 >> 4 & 15, 16);
        var0[var2++] = Character.forDigit(var1 & 15, 16);
        return var2;
    }


    public List<URL> listResources(final URL jarPath) throws IOException {
        // TODO correct?
        final List<URL> resources = new ArrayList<>(1);
        Map<String, String> env = new HashMap<>();
        String absPath = jarPath.toString();
        URI uri = URI.create("jar:" + absPath);
        try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {
            Path path = zipfs.getPath("/");
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    resources.add(file.toUri().toURL());
                    return super.visitFile(file, attrs);
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    if (path.equals(dir)) {
                        resources.add(dir.toUri().toURL());
                    }
                    return super.preVisitDirectory(dir, attrs);
                }

            });
        }
        return resources;
    }

    @Test
    public void testThymeleaf() {
        List<Map<String, Object>> dataSource = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", "James Bassey");
        map.put("unique_id", "832741");
        map.put("gender", "Male");
        map.put("age", 45);
        map.put("address", "Oron rd, Uyo");
        map.put("phone", "93884921");
        map.put("current_status", "ART Restart");
        dataSource.add(map);

        map = new HashMap<>();
        map.put("name", "Jane Bassey");
        map.put("unique_id", "832742");
        map.put("gender", "Female");
        map.put("age", 37);
        map.put("address", "Oron rd, Uyo");
        map.put("phone", "938849213");
        map.put("current_status", "ART Restart");
        dataSource.add(map);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("facility", "ak Uyo Base");
        parameters.put("state", "Akwa Ibom");
        parameters.put("lga", "Uyo");
        parameters.put("title", "Biometric Enrollment Report");
        parameters.put("from", new Date());
        parameters.put("to", new Date());

        PDFUtil.generate("biometric_report", parameters, dataSource, true);
    }
}
