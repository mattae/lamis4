import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG, ServerApiUrlConfig } from '../../app.constants';

@Injectable({providedIn: 'root'})
export class AuthServerProvider {
    private $localStorage: LocalStorageService;
    private $sessionStorage: SessionStorageService;

    constructor(private http: HttpClient,
                private injector: Injector,
                @Inject(SERVER_API_URL_CONFIG) private serverUrl: ServerApiUrlConfig) {
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }

    getToken() {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    }

    login(credentials: any): Observable<any> {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        let _this = this;
        return this.http.post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data, {observe: 'response'}).pipe(map(authenticateSuccess.bind(this)));

        function authenticateSuccess(resp: any) {
            const bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                _this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }

    loginWithToken(jwt: any, rememberMe: any) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe);
            return Promise.resolve(jwt);
        } else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }

    storeAuthenticationToken(jwt: any, rememberMe: any) {
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        } else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    }

    logout(): Observable<any> {
        return new Observable(observer => {
            this.$localStorage.remove('authenticationToken');
            this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    }
}
