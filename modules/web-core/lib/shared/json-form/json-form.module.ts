import { NgModule } from '@angular/core';
import { JsonFormComponent } from './json-form.component';
import { CommonModule } from '@angular/common';
import { MatDateFormatModule } from '../util/date-format/mat-date-format.module';
import { DetailsComponent } from './component/details.component';
import { CoreModule } from '@alfresco/adf-core';
import { MatFormioModule } from 'angular-material-formio';

@NgModule({
    imports: [MatFormioModule, CommonModule, CoreModule, MatDateFormatModule],
    declarations: [JsonFormComponent, DetailsComponent],
    exports: [JsonFormComponent, MatFormioModule, DetailsComponent],
    entryComponents: [JsonFormComponent]
})
export class JsonFormModule {

}
