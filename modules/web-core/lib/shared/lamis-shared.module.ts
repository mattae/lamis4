import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedCommonModule } from './shared-common.module';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { SpeedDialFabComponent } from './util/speed-dial-fab.component';
import { CardViewModule } from './util/card-view/card-view.module';
import { DateRangePicker } from './util/date-range-picker/date-range-picker';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	MatButtonModule,
	MatCardModule,
	MatDatepickerModule,
	MatIconModule,
	MatProgressSpinnerModule,
	MatSelectModule,
	MatTooltipModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { DateRangePickerDirective } from './util/date-range-picker/date-range-picker.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppLoaderService } from './util/app-loader/app-loader.service';
import { AppConfirmComponent } from './util/app-confirm/app-confirm.component';
import { AppLoaderComponent } from './util/app-loader/app-loader.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatSelectModule,
        MatIconModule,
        MatTooltipModule,
        RouterModule,
        SharedCommonModule,
        CardViewModule,
        FlexLayoutModule,
        MatDatepickerModule,
        MatCardModule,
        MatProgressSpinnerModule
    ],
	declarations: [
		HasAnyAuthorityDirective,
		SpeedDialFabComponent,
		DateRangePicker,
		DateRangePickerDirective,
		AppConfirmComponent,
		AppLoaderComponent
	],
	entryComponents: [
		DateRangePicker,
		AppConfirmComponent,
		AppLoaderComponent
	],
	exports: [
		SharedCommonModule,
		HasAnyAuthorityDirective,
		SpeedDialFabComponent,
		CardViewModule,
		DateRangePicker,
		DateRangePickerDirective
	],
	providers: [
		AppLoaderService,
		AppConfirmComponent
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LamisSharedModule {
}
