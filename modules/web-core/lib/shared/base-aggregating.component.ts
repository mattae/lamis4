import { NotificationService, ObjectDataTableAdapter } from '@alfresco/adf-core';
import { OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { ActivatedRoute, Router } from '@angular/router';
import { ITEMS_PER_PAGE } from '../shared/constants/pagination.constants';
import { Aggregate } from './model/base-entity';


export abstract class BaseAggregatingComponent<T> implements OnInit {
	public entities: ObjectDataTableAdapter;
	public rawEntities: T[];
	protected previousPage: any;
	protected predicate: any;
	protected reverse: any;
	protected filter: any[] = [];
	totalItems = 0;
	queryCount: any;
	itemsPerPage = ITEMS_PER_PAGE;
	page = 1;
	aggregates: any;
	protected selectedAggregates: any[] = [];
	sortOrder = 'asc';
	sortBy: string = 'id';
	currentSearch: string;
	routeData: any;

	constructor(protected router: Router,
	            protected activatedRoute: ActivatedRoute,
	            protected notification: NotificationService,
	            protected media: MediaObserver) {
	}

	ngOnInit(): void {
		this.routeData = this.activatedRoute.data.subscribe((data) => {
			if (data['pagingParams']) {
				this.page = data['pagingParams'].page;
				this.previousPage = data['pagingParams'].page;
				this.currentSearch = data['pagingParams'].query;
				this.reverse = data['pagingParams'].ascending;
				this.predicate = data['pagingParams'].predicate;
				if (data['pagingParams'].filter) {
					this.filter = typeof data['pagingParams'].filter === 'string' ?
						[<string> data['pagingParams'].filter] : <any[]> data['pagingParams'].filter;
				}
			}
		});
		this.transition();
	}

	searchEntities(keyword: string) {
		this.selectedAggregates = [];
		this.currentSearch = keyword;
		this.transition();
	}

	entitySelected(event: any) {
		let row = event.obj;
		this.router.navigate(['..', this.getPath(), row.id, 'view'], {relativeTo: this.activatedRoute});
	}

	getSort() {
		const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
		if (this.predicate !== 'id') {
			result.push('id');
		}
		return result;
	}

	sort(sortEvent: any): void {
		this.sortBy = sortEvent.key;
		this.sortOrder = sortEvent.direction;
		this.predicate = this.sortBy;
		this.reverse = this.sortOrder === 'asc';
		this.transition();
	}

	changed() {
		this.previousPage = 1;
		this.page = 1;
		this.transition();
	}

	changeLinks(event: any): void {
		this.page = event;
		this.loadPage(event);
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.transition();
		}
	}

	transition() {
		let params: any = {};
		if ((this.page) || (this.currentSearch)) {
			params.page = this.page;
		}
		if (this.currentSearch) {
			params.query = this.currentSearch;
		}
		if (this.predicate && this.predicate !== 'id') {
			params.sort = this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
		}
		if (this.filter) {
			params.filter = this.filter;
		}
		this.router.navigate(['..', this.getPath()], {relativeTo: this.activatedRoute, queryParams: params});
		this.loadAll();
	}

	loadAll() {
		const aggs = [];
		if (this.filter) {
			if (this.selectedAggregates.length == 0) {
				this.filter.forEach((filter: string) => {
					let parts = filter.split(':');
					this.selectedAggregates.push({field: parts[0], key: parts[1]});
				})
			}
		}
		if (this.selectedAggregates) {
			this.selectedAggregates.forEach((agg) => {
				aggs.push(agg);
			});
		}
		this.getService().search({
			query: this.currentSearch,
			page: this.page - 1,
			size: this.itemsPerPage,
			sort: this.getSort(),
			aggs: this.selectedAggregates
		}).subscribe(
			(res: any) => this.onSuccess(res.body, res.headers),
			(res: any) => this.onError(res.statusText)
		);
	}

	protected onSuccess(data: any, headers: any) {
		this.totalItems = headers.get('X-Total-Count');
		this.queryCount = this.totalItems;
		this.rawEntities = data;
		this.entities = new ObjectDataTableAdapter(data);
		let result = this.buildMap(JSON.parse(headers.get('aggregates')));
		let map = new Map();
		result.forEach((entryVal, entryKey) => {
			let aggs: any[] = [];
			if (this.selectedAggregates && this.selectedAggregates.length !== 0) {
				entryVal.forEach((entry: any) => {
					let selectedAggregate: any = {};
					this.selectedAggregates.filter((e) => {
						if (e.key == entry.key) {
							selectedAggregate = e;
						}
					});
					if (entry.field == selectedAggregate.field && entry.key == selectedAggregate.key) {
						aggs.push({
							field: entry.field,
							key: entry.key,
							count: entry.count,
							selected: true
						});
					}
					else {
						aggs.push({
							field: entry.field,
							key: entry.key,
							count: entry.count,
							selected: false
						});
					}
				});
			}
			else {
				entryVal.forEach((entry: any) => {
					aggs.push({
						field: entry.field,
						key: entry.key,
						count: entry.count,
						selected: false
					})
				});
			}
			map.set(entryKey, aggs);
		});
		this.aggregates = map;
		this.querySuccess();
	}

	private onError(error: any) {
		this.notification.openSnackMessage(error.message);
	}

	addFilter(field: string, val: string) {
		this.aggregateSelected(field, val, true);
	}

	removeFilter(field: string, val: string) {
		this.aggregateSelected(field, val, false);
	}

	aggregateSelected(field: string, val: string, state: boolean) {
		const selection: Aggregate = {'field': field, 'key': val};
		if (state) {
			this.selectedAggregates.push(selection);
		}
		else {
			let found = false;
			this.selectedAggregates.filter(e => {
				if (e.field === selection.field && e.key === selection.key) {
					found = true;
				}
			});
			if (found) {
				this.selectedAggregates = this.selectedAggregates
					.filter(e => !(e.field === selection.field && e.key === selection.key));
			}
		}
		this.filter = [];
		this.selectedAggregates.forEach((agg) => {
			this.filter.push(agg.field + ':' + agg.key);
		});
		this.transition();
	}

	buildMap(obj: any) {
		let map = new Map();
		Object.keys(obj).forEach(key => {
			map.set(key, obj[key]);
		});
		return map;
	}

	abstract getService(): any;

	abstract getPath(): string;

	abstract querySuccess(): any;
}
