import { NotificationService } from '@alfresco/adf-core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { OnInit, ViewChild } from '@angular/core';
import { MatButton, MatProgressBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IBaseEntity } from '../shared/model/base-entity';


export abstract class BaseEntityEditComponent<T extends IBaseEntity> implements OnInit {
    @ViewChild(MatProgressBar, {static: true}) progressBar: MatProgressBar;
    @ViewChild(MatButton, {static: true}) submitButton: MatButton;
    entity: T;
    isSaving: boolean;
    error = false;

    constructor(protected notification: NotificationService,
                protected route: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.isSaving = false;
        this.route.data.subscribe(({entity}) => {
            this.entity = !!entity && entity.body ? entity.body : entity;
        });
        if (this.entity === undefined) {
            this.entity = this.createEntity();
        }
    }

    previousState() {
        window.history.back();
    }

    save(valid?: any) {
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        this.isSaving = true;
        if (this.entity.id !== undefined) {
            this.subscribeToSaveResponse(this.getService().update(this.entity));
        } else {
            this.subscribeToSaveResponse(this.getService().create(this.entity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (res: HttpResponse<any>) => this.onSaveSuccess(res.body),
            (res: HttpErrorResponse) => {
                this.onSaveError();
                this.onError(res.message)
            });
    }

    private onSaveSuccess(result: any) {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
        this.error = true;
        this.submitButton.disabled = true;
        this.progressBar.mode = 'determinate';
    }

    protected onError(errorMessage: string) {
        this.notification.openSnackMessage(errorMessage);
    }

    entityCompare(s1: any, s2: any): boolean {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    }

    abstract getService(): any;

    abstract createEntity(): T;
}
