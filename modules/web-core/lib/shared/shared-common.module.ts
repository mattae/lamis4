import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { CoreModule } from '@alfresco/adf-core';
import { CommonPipesModule } from './pipes/common/common-pipes.module';
import { NgJhipsterModule } from 'ng-jhipster';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { DetailsComponent } from './json-form/component/details.component';
import { MatCardModule } from '@angular/material';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		NgJhipsterModule,
		CommonPipesModule,
		MatCardModule
	],
	declarations: [
		AlertComponent,
		AlertErrorComponent
	],
	exports: [
		AlertComponent,
		AlertErrorComponent,
		CommonPipesModule
	]
})
export class SharedCommonModule {
}
