import { CardViewItemProperties } from '@alfresco/adf-core';
import { Phone } from '../../../model';

export interface CardViewPhoneItemProperties extends CardViewItemProperties {
    value: Phone;
}
