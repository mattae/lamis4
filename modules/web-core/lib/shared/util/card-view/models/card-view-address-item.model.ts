import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardItemAddressItemProperties } from './card-item-address-item.properties';

export class CardViewAddressItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string = 'address';

    constructor(obj: CardItemAddressItemProperties) {
        super(obj);
    }

    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        } else {
            return this.getValue();
        }
    }

    getValue() {
        return `${this.value.street1}${!!this.value.street2 ? ', ' + this.value.street2 : ''}
            ${!!this.value.city ? ', ' + this.value.city : ''}${!!this.value.lga ? ', ' + this.value.lga.name : ''}`
            .trim();
    }
}