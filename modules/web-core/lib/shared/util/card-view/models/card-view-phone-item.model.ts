import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardViewPhoneItemProperties } from './card-view-phone-item.properties';

export class CardViewPhoneItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string = 'phone';

    constructor(obj: CardViewPhoneItemProperties) {
        super(obj);
    }

    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        } else {
            return this.getValue();
        }
    }

    getValue() {
        return `${!!this.value.phone1 ? this.value.phone1: ''}
        ${!!this.value.phone2 ? ', ' + this.value.phone2: ''}`.trim();
    }
}