import { CardViewItemProperties } from '@alfresco/adf-core';
import { IAddress } from '../../../model';

export interface CardItemAddressItemProperties extends CardViewItemProperties {
    value: IAddress;
}
