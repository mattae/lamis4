import { CardViewItemProperties } from '@alfresco/adf-core';
import { PersonName } from '../../../model';

export interface CardViewNameItemProperties extends CardViewItemProperties {
    value: PersonName;
}
