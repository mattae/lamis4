import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardViewNameItemProperties } from './card-view-name-item.properties';

export class CardViewNameItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string = 'name';

    constructor(obj: CardViewNameItemProperties) {
        super(obj);
    }

    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        } else {
            return this.getValue();
        }
    }

    getValue() {
        return `${!!this.value.title ? this.value.title + ' ': ''}
        ${!!this.value.firstName ? this.value.firstName + ' ': ''}
        ${!!this.value.middleName ? this.value.middleName + ' ': ''}
        ${!!this.value.surname ? this.value.surname + ' ': ''}`.trim();
    }
}
