import {
    CardViewBaseItemModel,
    CardViewItem,
    CardViewTextItemProperties,
    DynamicComponentModel
} from '@alfresco/adf-core';

export class CardViewHtmlTextItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string = 'html-text';

    constructor(obj: CardViewTextItemProperties) {
        super(obj);
    }

    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        } else {
            return this.value;
        }
    }
}
