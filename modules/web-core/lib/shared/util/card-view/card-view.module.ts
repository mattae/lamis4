import { NgModule } from '@angular/core';
import { CardItemTypeService, CoreModule } from '@alfresco/adf-core';
import { CardViewHtmlTextItemComponent } from './components/card-view-html-text-item/card-view-html-text-item.component';
import { CardViewNameItemComponent } from './components/card-view-name-item/card-view-name-item.component';
import { CardViewAddressItemComponent } from './components/card-view-address-item/card-view-address-item.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CardViewFixedKeyvaluepairsitemComponent } from './components/card-view-fixed-keyvaluepairsitem/card-view-fixed-keyvaluepairsitem.component';
import { CardViewPhoneItemComponent } from './components/card-view-phone-item/card-view-phone-item.component';

export function components() {
    return [
        CardViewHtmlTextItemComponent,
        CardViewNameItemComponent,
        CardViewAddressItemComponent,
        CardViewFixedKeyvaluepairsitemComponent,
        CardViewPhoneItemComponent
    ]
}

@NgModule({
    imports: [
        CoreModule,
        CommonModule,
        FormsModule,
        FlexLayoutModule
    ],
    declarations: components(),
    exports: components(),
    entryComponents: components(),
    providers: [
        CardItemTypeService
    ]
})
export class CardViewModule {
    constructor(private cardItemTypeService: CardItemTypeService) {
        cardItemTypeService.setComponentTypeResolver('html-text', () => CardViewHtmlTextItemComponent);
        cardItemTypeService.setComponentTypeResolver('phone', () => CardViewPhoneItemComponent);
        cardItemTypeService.setComponentTypeResolver('name', () => CardViewNameItemComponent);
        cardItemTypeService.setComponentTypeResolver('address', () => CardViewAddressItemComponent);
        cardItemTypeService.setComponentTypeResolver('fixedkeyvaluepairs', () => CardViewFixedKeyvaluepairsitemComponent);
    }
}
