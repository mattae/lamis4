import { CardViewItemValidator } from '@alfresco/adf-core';

export class CardViewItemAddressValidator implements CardViewItemValidator {
    message = '';

    isValid(value: any): boolean {
        let streetError = false;
        let cityError = false;
        let lgaError = false;
        if (!value.street1 || value.street1.length < 3) {
            streetError = true;
        }
        if (!value.city || value.city.length < 3) {
            cityError = true;
        }
        if (!!value.lga) {
            lgaError = true;
        }
        if (streetError && cityError && lgaError) {
            this.message = 'Street, city and LGA are all required';
        } else if (streetError && cityError) {
            this.message = 'Street and city are required';
        } else if (streetError && lgaError) {
            this.message = 'Street and LGA are required';
        } else if (lgaError && cityError) {
            this.message = 'City and LGA are required';
        } else if (streetError) {
            this.message = 'Street is required';
        } else if (cityError) {
            this.message = 'City is required'
        } else if (lgaError) {
            this.message = 'LGA is required'
        }
        return !(streetError || cityError || lgaError);
    }

}