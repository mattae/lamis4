import { CardViewItemValidator } from '@alfresco/adf-core';

export class CardViewItemNameValidator implements CardViewItemValidator {
    message = '';

    isValid(value: any): boolean {
        let nameError = false;
        let surnameError = false;
        if (!value.firstName || value.firstName.length < 3) {
            nameError = true;
        }
        if (!value.surname || value.surname.length < 3) {
            surnameError = true;
        }
        if (nameError && surnameError) {
            this.message = 'Both first name and surname are required';
        } else if (nameError) {
            this.message = 'First name is required';
        } else if (surnameError) {
            this.message = 'Surname is required'
        }
        return !(nameError || surnameError);
    }

}