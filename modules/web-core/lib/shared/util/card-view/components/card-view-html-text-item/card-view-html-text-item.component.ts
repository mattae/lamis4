import { Component, Input, OnChanges } from '@angular/core';
import { CardViewHtmlTextItemModel } from '../../models/card-view-html-text-item.model';

@Component({
    selector: 'tradcard-view-html-text',
    templateUrl: './card-view-html-text-item.component.html',
    styleUrls: ['./card-view-html-text-item.component.scss']
})
export class CardViewHtmlTextItemComponent implements OnChanges {
    @Input()
    property: CardViewHtmlTextItemModel;

    constructor() {
    }

    ngOnChanges(): void {
    }

}
