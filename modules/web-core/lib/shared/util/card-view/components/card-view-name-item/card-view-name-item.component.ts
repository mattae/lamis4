import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewNameItemModel } from '../../models/card-view-name-item.model';
import { PersonName } from '../../../../model/address.model';

@Component({
    selector: 'card-view-name-item',
    templateUrl: './card-view-name-item.component.html'
})
export class CardViewNameItemComponent implements OnChanges {
    @Input()
    property: CardViewNameItemModel;

    @Input()
    editable: boolean = false;

    @Input()
    displayEmpty: boolean = true;

    @ViewChild('titleInput', {static: true})
    private titleInput: any;

    @ViewChild('firstNameInput', {static: true})
    private firstNameInput: any;

    @ViewChild('middleNameInput', {static: true})
    private middleNameInput: any;

    @ViewChild('titleInput', {static: true})
    private surnameInput: any;

    inEdit: boolean = false;
    editedTitle: string;
    editedSurname: string;
    editedFirstName: string;
    editedMiddleName: string;
    errorMessages: string[];

    constructor(private cardViewUpdateService: CardViewUpdateService) {
    }

    ngOnChanges(): void {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
    }

    showProperty(): boolean {
        return this.displayEmpty || !this.property.isEmpty();
    }

    isEditable(): boolean {
        return this.editable && this.property.editable;
    }

    isClickable(): boolean {
        return this.property.clickable;
    }

    hasIcon(): boolean {
        return !!this.property.icon;
    }

    hasErrors(): number {
        return this.errorMessages && this.errorMessages.length;
    }

    setEditMode(editStatus: boolean): void {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.titleInput) {
                this.titleInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.firstNameInput) {
                this.firstNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.middleNameInput) {
                this.middleNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.surnameInput) {
                this.surnameInput.nativeElement.click();
            }
        }, 0);
    }

    reset(): void {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
        this.setEditMode(false);
    }

    update(): void {
        console.log('Property', this.property);
        if (this.property.isValid(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname))) {
            this.cardViewUpdateService.update(this.property,
                new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
            this.property.value = new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname);
            this.setEditMode(false);
        } else {
            this.errorMessages = this.property.getValidationErrors(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
        }
    }

    get displayValue() {
        return this.property.displayValue;
    }

    clicked(): void {
        this.cardViewUpdateService.clicked(this.property);
    }
}
