import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewPhoneItemModel } from '../../models/card-view-phone-item.model';
import { Phone } from '../../../../model/address.model';

@Component({
    selector: 'card-view-phone-item',
    templateUrl: './card-view-phone-item.component.html',
})
export class CardViewPhoneItemComponent implements OnChanges, OnInit {
    phone: Phone;
    @Input()
    property: CardViewPhoneItemModel;

    @Input()
    editable: boolean = false;

    @Input()
    displayEmpty: boolean = true;

    @ViewChild('phone1Input', {static: true})
    private phone1Input: any;

    @ViewChild('phone2Input', {static: true})
    private phone2Input: any;

    inEdit: boolean = false;
    editedPhone1: string;
    editedPhone2: string;
    errorMessages: string[];

    constructor(private cardViewUpdateService: CardViewUpdateService) {
    }

    ngOnChanges(): void {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
    }

    ngOnInit() {
    }

    showProperty(): boolean {
        return this.displayEmpty || !this.property.isEmpty();
    }

    isEditable(): boolean {
        return this.editable && this.property.editable;
    }

    isClickable(): boolean {
        return this.property.clickable;
    }

    hasIcon(): boolean {
        return !!this.property.icon;
    }

    hasErrors(): number {
        return this.errorMessages && this.errorMessages.length;
    }

    setEditMode(editStatus: boolean): void {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.phone1Input) {
                this.phone1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.phone2Input) {
                this.phone2Input.nativeElement.click();
            }
        }, 0);
    }

    reset(): void {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
        this.setEditMode(false);
    }

    update(): void {
        if (this.property.isValid(new Phone(this.editedPhone1, this.editedPhone2))) {
            this.cardViewUpdateService.update(this.property,
                new Phone(this.editedPhone1, this.editedPhone2));
            this.property.value = new Phone(this.editedPhone1, this.editedPhone2);
            this.setEditMode(false);
        } else {
            this.errorMessages = this.property.getValidationErrors(new Phone(this.editedPhone1, this.editedPhone2));
        }
    }

    get displayValue() {
        return this.property.displayValue;
    }

    clicked(): void {
        this.cardViewUpdateService.clicked(this.property);
    }
}
