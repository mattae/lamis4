import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewAddressItemModel } from '../../models/card-view-address-item.model';
import { MatSelectChange } from '@angular/material';
import { Address } from '../../../../model/address.model';
import { LgaService } from '../../../../../services/lga.service';
import { StateService } from '../../../../../services/state.service';
import { ILGA } from '../../../../model/lga.model';
import { IState } from '../../../../model/state.model';

@Component({
    selector: 'card-view-address-item',
    templateUrl: './card-view-address-item.component.html',
})
export class CardViewAddressItemComponent implements OnChanges, OnInit {
    state: IState;
    @Input()
    property: CardViewAddressItemModel;

    @Input()
    editable: boolean = false;

    @Input()
    displayEmpty: boolean = true;

    @ViewChild('cityInput', {static: true})
    private cityInput: any;

    @ViewChild('street1Input', {static: true})
    private street1Input: any;

    @ViewChild('street2Input', {static: true})
    private street2Input: any;

    states: IState[];
    lgas: ILGA[];

    inEdit: boolean = false;
    editedStreet1: string;
    editedStreet2: string;
    editedCity: string;
    editedLga: ILGA;
    errorMessages: string[];

    constructor(private cardViewUpdateService: CardViewUpdateService,
                private stateService: StateService,
                private lgaService: LgaService) {
    }

    ngOnChanges(): void {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
    }

    ngOnInit() {
    }

    onChange(event: MatSelectChange): void {
        this.lgaService.findByState(event.value.id).subscribe(res => this.lgas = res.body);
    }

    showProperty(): boolean {
        return this.displayEmpty || !this.property.isEmpty();
    }

    isEditable(): boolean {
        return this.editable && this.property.editable;
    }

    isClickable(): boolean {
        return this.property.clickable;
    }

    hasIcon(): boolean {
        return !!this.property.icon;
    }

    hasErrors(): number {
        return this.errorMessages && this.errorMessages.length;
    }

    setEditMode(editStatus: boolean): void {
        if (editStatus) {
            this.stateService.getStates().subscribe(res => this.states = res.body);
        }
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.cityInput) {
                this.cityInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street1Input) {
                this.street1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street2Input) {
                this.street2Input.nativeElement.click();
            }
        }, 0);
    }

    reset(): void {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
        this.setEditMode(false);
    }

    update(): void {
        if (this.property.isValid(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga))) {
            this.cardViewUpdateService.update(this.property,
                new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
            this.property.value = new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga);
            this.setEditMode(false);
        } else {
            this.errorMessages = this.property.getValidationErrors(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
        }
    }

    get displayValue() {
        return this.property.displayValue;
    }

    clicked(): void {
        this.cardViewUpdateService.clicked(this.property);
    }

    entityCompare(s1: any, s2: any): boolean {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    }
}
