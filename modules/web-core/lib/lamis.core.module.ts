import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateTimeConfig, SERVER_API_URL_CONFIG, ServerApiUrlConfig } from './app.constants';
import { RxStompConfig } from './services/rx-stomp.config';
import { LamisSharedModule } from './shared/lamis-shared.module';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { LOCALE_CONFIG, LocaleConfig } from './shared/util/date-range-picker/date-range-picker.config';
import { LocaleService } from './shared/util/date-range-picker/locales.service';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { MomentDateFormat } from './moment-date-format';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        LamisSharedModule
    ],
    exports: [
        LamisSharedModule
    ],
    providers: []
})
export class LamisCoreModule {
    static forRoot(serverApiUrlConfig: ServerApiUrlConfig, dateTimeConfig?: DateTimeConfig, config?: LocaleConfig): ModuleWithProviders {
        //MomentDateFormat.DATE_FORMAT = dateTimeConfig.DATE_FORMAT;
        return {
            ngModule: LamisCoreModule,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                {provide: LOCALE_CONFIG, useValue: config || {}},
                {provide: LocaleService, useClass: LocaleService, deps: [LOCALE_CONFIG]},
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    }
}
