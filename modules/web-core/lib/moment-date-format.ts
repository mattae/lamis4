export class MomentDateFormat {
    static DATE_FORMAT: string = 'DD-MM-YYYY';
    static DATE_TIME_FORMAT: string = 'DD-MM-YYYY ';
}
