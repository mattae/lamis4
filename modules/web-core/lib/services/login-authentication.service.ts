import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';

@Injectable()
export class LoginAuthenticationService {

    constructor(private router: Router,
                private loginService: LoginService,
                private $localStorage: LocalStorageService,
                private $sessionStorage: SessionStorageService,
                private eventManager: JhiEventManager,
                private stateStorageService: StateStorageService,
                private injector: Injector) {
    }

    setRedirect(value: any){

    }

    isEcmLoggedIn() {
        return false;
    }

    isBpmLoggedIn() {
        return false;
    }

    isOauth() {
        return false;
    }

    getRedirect() {
        return null;
    }

    login(username: string, password: string, rememberMe: boolean = false): Observable<any> {
        this.loginService
            .login({
                username: username,
                password: password,
                rememberMe: rememberMe
            })
            .then((data) => {
                if (this.router.url === '/account/register' || (/^\/account\/activate\//.test(this.router.url)) ||
                    (/^\/account\/reset\//.test(this.router.url))) {
                    this.router.navigate(['']);
                }
                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });
                // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // // since login is successful, go to stored previousState and clear previousState
                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl('');
                    this.router.navigate([redirect]);
                } else {
                    this.router.navigate(['/dashboard']);
                }
            });
        return of(true);
    }
}
