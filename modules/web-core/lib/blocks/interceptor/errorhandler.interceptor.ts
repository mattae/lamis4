import { Injectable, Injector } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    private eventManager: JhiEventManager;

    constructor(private injector: Injector) {
        this.eventManager = injector.get(JhiEventManager);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                () => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (!(err.status === 401 && (err.message === '' || (err.url && err.url.includes('/api/account'))))) {
                            this.eventManager.broadcast({ name: 'app.httpError', content: err });
                        }
                    }
                }
            )
        );
    }
}
