package org.fhi360.lamis.modules.security;

import lombok.extern.slf4j.Slf4j;
import org.fhi360.lamis.modules.base.module.ModuleLifecycle;

@Slf4j
public class SecurityModuleInstaller implements ModuleLifecycle {
    @Override
    public void preInstall() {
        LOG.info("Running security module preinstaller");
    }
}
