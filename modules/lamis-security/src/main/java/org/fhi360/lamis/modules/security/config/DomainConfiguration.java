package org.fhi360.lamis.modules.security.config;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAcrossJpaRepositories(basePackages = "org.fhi360.lamis.modules.security.domain")
public class DomainConfiguration {
}
