package org.fhi360.lamis.modules.security;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;

@AcrossDepends(required = AcrossHibernateJpaModule.NAME)
public class LamisSecurityModule extends AcrossModule {
    public static final String NAME = "LAMISSecurityModule";

    public LamisSecurityModule() {
        super();
        addApplicationContextConfigurer(
                new ComponentScanConfigurer(getClass().getPackage().getName() + ".service",
                        getClass().getPackage().getName() + ".web", getClass().getPackage().getName() + ".jwt"));
    }

    @Override
    public String getName() {
        return NAME;
    }
}
